import sys, os, glob
sys.path.append(os.getcwd())

from yade import export
import numpy as np, warnings
import utl_aor

# "PP" for Potential Particle
# "CLP" for Clump
model = "PP" 

series = "main" # "param_study" for PP parametric study, "main" for main series
vtk_outer_surf = True # True to save a VTK file of the outer surface

# AOR measure parameters
x_min = 5e-3 # m, radius_part so particles stuck by the ledge are excluded
x_max = 1e-1 # m, horizontal length of the box
dx = 1e-2 # m, the diameter of 1 particle

# Void ratio measure parameters
e_method = "SUBV" # "TET" # "SUBV" for subvolume method, "TET" for tetrahedron method
C_subv = .8 # Homotethie's parameter for subvolume method
n_cores = 3 # Number of cores to use when computing void ratio (only for tetrahedron method for now)

# Other parameters
seed = 0

# Read parameters from table if running in batch
readParamsFromTable(seed=int(seed), model=model, Ks=240, density=1111, series=series)
from yade.params.table import *
np.random.seed(seed) # Fixing random seed

# Creating postprocessing directory if it doesn't already exist
save_dir = "ppc/"
if not os.path.isdir(save_dir) : os.mkdir(save_dir)

# Load sample
    ## Find the sample's symlink for the corresponding seed and model
if series=="main": sample = glob.glob("final_states/final_state_{:}_2150_parts_{:d}.yade*.xml.bz2".format(model, seed))[0] # Beware if you have several samples with the same parameters but different yade version
elif series=="param_study": sample = glob.glob("final_states/final_state_{:}_2150_parts_{:d}__density{:.0f}_Ks{:.0f}.yade{:}.xml.bz2".format(model, seed, density, Ks, version.split("-")[-1]))[0]
    
    ## Get sample's path
sample = os.path.realpath(sample).split("final_states/")[0] + os.path.realpath(sample).split("final_states/")[1] 

    ## Save empty simulation to import initial sample later
O.saveTmp()

    ## Load final state
O.load(sample)
print("Sample {:} loaded".format(sample))

# Import utility module for chosen the model
if model == "PP": import utl_pp as utl
elif model == "CLP": import utl_clp as utl

# Compute final stress tensor
    ## Get id of all walls
walls_id = [b.id for b in O.bodies if b.state.blockedDOFs=="xyzXYZ"]

    ## Get ids of all particles
if model=="PP": part_ids = np.array([b.id for b in O.bodies if b.id not in walls_id])
elif model=="CLP": part_ids = np.array([b.id for b in O.bodies if type(b.shape)==Clump])

    ## Compute stress
sig00, sig01, sig02, sig10, sig11, sig12, sig20, sig21, sig22 = utl_aor.sigma_heap(part_ids, walls_id).flatten()

# Keep only particles
for b in O.bodies:
    if b.state.blockedDOFs=="xyzXYZ": O.bodies.erase(b.id)

# Get centers of all particles
if model=="PP": centers = np.array([b.state.pos for b in O.bodies])
elif model=="CLP": centers = np.array([b.state.pos for b in O.bodies if type(b.shape)==Clump])

# Save outer surface
if series=="main": outer_surface_fn = "ppc/outer_surface_{:}_{:d}".format(model, seed)
elif series=="param_study": outer_surface_fn = "ppc/outer_surface_{:}_{:d}__density{:.0f}_Ks{:.0f}".format(model, seed, density, Ks)
outer_surface = utl_aor.get_outer_surface(centers, 2, dx)
with open(outer_surface_fn + ".csv", "w") as fil:
    fil.write("x\ty\tz\n")
    for point in outer_surface:
        data_line = "\t".join(["{:.8e}"]*3) + "\n"
        fil.write(data_line.format(*point))

    ## If user wants a VTK file of the outer surface
if vtk_outer_surf:
    if model == "CLP":
        vtk_exp = export.VTKExporter(outer_surface_fn)
        clp_ids, sph_ids = [b.id for b in O.bodies if b.state.pos in outer_surface], []
        for clp_id in clp_ids: sph_ids += list(O.bodies[clp_id].shape.members.keys())
        vtk_exp.exportSpheres(ids=sph_ids, what={'displacement': '0'}, numLabel=0)
    elif model == "PP": print("VTK saving of outer surface not yet available in these scripts")

# Compute AOR
aor = utl_aor.compute_aor(centers, x_min, x_max, dx)

# Compute void ratio
    ## Tetrahedron method
if e_method=="TET": e = utl_aor.compute_e_tet(part_ids, utl.is_in_particle, n_cores=3, n_mc_points=200, random_num_generator=np.random.default_rng(seed)) # Takes a long time
    ## Subvolume method
elif e_method=="SUBV": 
        ### Define subvolume
            #### Heap's bounding box
    [Xmin, Ymin, Zmin], [Xmax, Ymax, Zmax] = utl.custom_aabbExtrema()

            #### Compute subvolume's bounding box
    [xmin, ymin, zmin] = [(1-C_subv)*length + mininum for length, mininum in zip([Xmax-Xmin, Ymax-Ymin, Zmax-Zmin], [Xmin, Ymin, Zmin])]
    [xmax, ymax, zmax] = [C_subv*length + mininum for length, mininum in zip([Xmax-Xmin, Ymax-Ymin, Zmax-Zmin], [Xmin, Ymin, Zmin])]
            
            #### Compute subvolume's inclined face parameters
    face_slope = (zmax-zmin) / (xmax-xmin)
    face_interc = zmin - face_slope*xmin

            #### Compute subvolume's volume
    V_subv = (xmax-xmin)*(ymax-ymin)*(zmax-zmin)/2

            #### Define functions that returns True if a point is in the subvolume
    def is_in_subv(point):
        in_x = point[0] > xmin and point[0] < xmax
        in_y = point[1] > ymin and point[1] < xmax
        in_z = point[2] > zmin and point[2] < point[0]*face_slope + face_interc
        return  in_x and in_y and in_z

    e = utl_aor.compute_e_subv(part_ids, utl.is_in_particle, is_in_subv, V_subv, n_mc_points=1000, random_num_generator=np.random.default_rng(seed))

# Compute final number of erased particles
n_erased = 2150 - len(part_ids)

# Compute final average number of interactions
zc = utl.avg_coord_nmbr()

# Postprocess initial sample
    ## Load sample
if series=="main": sample = glob.glob("samples/{:}_2150_parts_{:d}.yade*.xml.bz2".format(model, seed))[0] # Beware if you have several samples with the same parameters but different yade version
elif series=="param_study": sample = glob.glob("samples/{:}_2150_parts_{:d}__density{:.0f}_Ks{:.0f}.yade*.xml.bz2".format(model, seed, density, Ks))[0]

O.loadTmp()
O.load(sample)

    ## Keep only particles
for b in O.bodies:
    if b.state.blockedDOFs=="xyzXYZ": O.bodies.erase(b.id)

    ## Reload utility module (to reinitialise variables)
import importlib
importlib.reload(utl)

    ## Compute void ratio
        ### Get ids of all particles
if model=="PP": part_ids = np.array([b.id for b in O.bodies])
elif model=="CLP": part_ids = np.array([b.id for b in O.bodies if type(b.shape)==Clump])

        ### Tetrahedron method
if e_method=="TET": e0 = utl_aor.compute_e_tet(part_ids, utl.is_in_particle, n_cores=3, n_mc_points=200, random_num_generator=np.random.default_rng(seed)) # Takes a long time
        ### Subvolume method
elif e_method=="SUBV": 
            #### Define subvolume
                ##### Heap's bounding box
    [Xmin, Ymin, Zmin], [Xmax, Ymax, Zmax] = utl.custom_aabbExtrema()

                ##### Compute subvolume's bounding box
    [xmin, ymin, zmin] = [(1-C_subv)*length + mininum for length, mininum in zip([Xmax-Xmin, Ymax-Ymin, Zmax-Zmin], [Xmin, Ymin, Zmin])]
    [xmax, ymax, zmax] = [C_subv*length + mininum for length, mininum in zip([Xmax-Xmin, Ymax-Ymin, Zmax-Zmin], [Xmin, Ymin, Zmin])]

                ##### Compute subvolume's volume
    V_subv = (xmax-xmin)*(ymax-ymin)*(zmax-zmin)

                ##### Define functions that returns True if a point is in the subvolume
    def is_in_subv(point):
        in_x = point[0] > xmin and point[0] < xmax
        in_y = point[1] > ymin and point[1] < xmax
        in_z = point[2] > zmin and point[2] < zmax
        return  in_x and in_y and in_z

    e0 = utl_aor.compute_e_subv(part_ids, utl.is_in_particle, is_in_subv, V_subv, n_mc_points=1000, random_num_generator=np.random.default_rng(seed))

    ## Compute average coordination number
zc0 = utl.avg_coord_nmbr()

# Saving results
    ## Data to save
data = [seed, aor, e, n_erased, zc, e0, zc0, sig00, sig01, sig02, sig10, sig11, sig12, sig20, sig21, sig22]

    ## Opening results' csv file
if series=="main": csv_path = save_dir + "{:}_model.csv".format(model)
elif series=="param_study": csv_path = save_dir + "{:}_model__density{:.0f}_Ks{:.0f}.csv".format(model, density, Ks)

        ### If the file exists and is not empty, display a warning
if os.path.exists(csv_path) and os.path.getsize(csv_path)>0: 
    warnings.warn("Postprocessing csv file is not empty")
    header = ""
else: header = "\t".join([[ i for i, a in globals().items() if id(a) == id(x)][0] for x in data]) + "\n"

        ### Open file and add a header
csv_f = open(csv_path, "a") # Before a new run, one should make sure this file is empty
csv_f.write(header)

    ## Add line in csv file
data_line = "\t".join(["{:.8e}"]*len(data)) + "\n"
csv_f.write(data_line.format(*data))
csv_f.close()




