import numpy as np, pprint as pp, os, matplotlib as plt
from datetime import datetime as dt, timedelta as td
from yade.export import text as txt, textClumps as txtclp
from yade._utils import *#unbalancedForce
import yade
import warnings
import sys
sys.path.append(os.getcwd())
warnings.filterwarnings("ignore")

class dataRecorder():
	
	def __init__(self, var2record, params2save=[], data_directory='', data_to_plot=[], var_dict={}):
		
		if data_directory[-1] != "/": data_directory += "/"
		if not os.path.isdir(data_directory) : os.mkdir(data_directory)
		self.var2record = var2record
		self.params2save = params2save
		self.data_to_plot = data_to_plot
		self.var_dict = var_dict
		# Creating an unique id for the run
		now = dt.now()
		test = True # Intialisation
		while test:
			self.run_id = ''
			for st in ["%y", "%m", "%d", "%H", "%M", "%S", "%f"]: self.run_id += now.strftime(st)
			self.data_directory = data_directory + 'run_' + self.run_id + '/'
			test = os.path.isdir(self.data_directory) 
			if test: now = now + td(seconds=1) # add one second to the run id
		# Get commit id
		if os.path.exists('.git'): 
			with open('.git/logs/HEAD', 'r') as git_head_file: self.commit_number = git_head_file.readlines()[-1].split(' ')[1] # l30
				
		# Get parameters of the run
		id_spheres = [b.id for b in O.bodies if type(b.shape) == yade.wrapper.Sphere]
		self.n_spheres = len(id_spheres)

		# Write the parameters in the parameters file
		self.write_params(header = 'Initial parameters')

		# Write the header of the data file
		head_data = ''
		for var_name in self.var2record:
			head_data += '{:18}\t'.format(var_name)
		head_data += '\n'
		self.data_file_path = self.data_directory+self.run_id+'_data'
		data_file = open(self.data_file_path,'w')
		data_file.write("Run id: "+self.run_id+" Separator: tabulation\n")
		data_file.write(head_data)
		data_file.close()		
		#self.data_file.write("{:10}\t{:10}\t{:10}\t{:10}\t{:10}\t{:10}\t{:10}\t{:10}\t{:10}\t{:10}\t{:10}\t{:10}\t{:10}\t{:10}\t{:10}\t{:10}\n".format('iter/100', 'Stress 11', 'Stress 22', 'Stress 33', 'Strain 11', 'Strain 22', 'Strain 33', 'Vol. strain', 'Strain rate 11', 'Strain rate 22', 'Strain rate 33', 'Porosity', 'Box Volume', 'Water volume', 'Unbal. Force', 'Ext. work'))
		
		# Initialiaze some attributes
		self.iter_begin_compression = 0

	def write_params(self, header=''):
		separator_line = '\n'
		engine_separator_line = '\n'

		for k in range(50) : 
			separator_line += '#'
			engine_separator_line += '_'
		separator_line += '\n'
		if not os.path.isdir(self.data_directory) : os.mkdir(self.data_directory)
		if not os.path.isdir(self.data_directory + 'Parameters_files/') : os.mkdir(self.data_directory + 'Parameters_files/')
		param_file = open(self.data_directory + 'Parameters_files/' + self.run_id + '_parameters_{:}_it_{:d}'.format(header, O.iter),'w')
		param_file.write(header+'\n\n')
		param_file.write("\tIMPORTANT PARAMETERS:\n")
		try : param_file.write("{:30}".format("Last commit id:") + ' ' + self.commit_number + '\n') # l70
		except : pass
		param_file.write("{:30}".format("Run id:") + ' ' + self.run_id + '\n')
		param_file.write("{:30} {:d}".format("Iteration:", O.iter)+'\n')		
		param_file.write("{:30} {:d}".format("Number of spheres:", self.n_spheres)+'\n')
		for param in self.params2save:
			param_file.write("{:30}".format(param) + ' ' + str(eval(param, self.var_dict)) + '\n')
			
		param_file.write(separator_line)
		param_file.write("\n\tMATERIALS :\n")
		for mat in O.materials: param_file.write('\n'+str(mat)[1:-23]+':\n\n'+pp.pformat(mat.dict())+'\n')

		param_file.write(separator_line)
		param_file.write("\n\tENGINES :\n")
		for engine in O.engines:
			param_file.write('\n'+str(type(engine))[21:-2]+':'+'\n\n')
			param_file.write(pp.pformat(engine.dict())+'\n')
			for attr in dir(engine):
				try:	
					ATTR = getattr(engine, attr)
					test = ATTR.dict()
				except: 
					continue
				param_file.write('\n\t'+str(ATTR)[1:-23]+':'+'\n\n')
				param_file.write(pp.pformat(ATTR.dict(), indent=8)+'\n')
				try:
					for funct in ATTR.functors:
						param_file.write('\n\t\t'+str(funct)[1:-23]+':'+'\n\n')
						param_file.write(pp.pformat(funct.dict(), indent=16)+'\n')
				except: 
					continue
			param_file.write(engine_separator_line+'\n')

	def write_data(self, var_dict={}):
		# Get current values
		data_line = ''
		for var_name in self.var2record:
			data_line += '{:.10e}\t'.format(eval(var_name, var_dict))
		data_line += '\n'
		data_file = open(self.data_file_path, 'a')
		data_file.write(data_line)
		data_file.close()
	
	def save_spheres(self, special_name=''):
		txt(self.data_directory+'Run_'+self.run_id+'_saved_spheres'+special_name)
	
	def save_clumps(self, special_name=''):
		txtclp(self.data_directory+'Run_'+self.run_id+'_saved_clumps'+special_name)
	
	def plot_data(self, data_to_plot=[], plot_lines=slice(0,-1), save=False, subfolder = ''):
		import matplotlib.pyplot as plt
		#['iter/100', 'Stress 11', 'Stress 22', 'Stress 33', 'Strain 11', 'Strain 22', 'Strain 33', 'Vol. strain', 'Strain rate 11', 'Strain rate 22', 'Strain rate 33', 'Porosity', 'Box Volume', 'Water volume', 'Unbal. Force', 'Ext. work']
		
		if data_to_plot != []: self.data_to_plot = data_to_plot 


		#[('iter/100', ('Stress 11', 'Stress 22', 'Stress 33'))]

		filename = self.data_directory + '/' + self.run_id + '_data'
		data = np.genfromtxt(filename, delimiter='\t', skip_header=2, invalid_raise=False)
		plt.close('all')	

		for i,fig in enumerate(data_to_plot):
			plt.figure(i)
			x = self.var2record.index(fig[0])
			title ='('
			for y_name in fig[1]:
				title += y_name + ' '
				y = self.var2record.index(y_name)
				plt.plot(data[plot_lines,x], data[plot_lines,y], label=y_name)
			title += ') vs ' + fig[0]

			plt.xlabel(fig[0])
			ylabel = ''
			for lab in fig[1]: ylabel += lab + '\n'
			plt.ylabel(ylabel)
			plt.legend(loc='best')		
			plt.suptitle(title)

		if not save: 
			plt.show(block=False)

		if save: 
			graph_dir = self.data_directory + 'graphs/'
			if not os.path.isdir(graph_dir) : os.mkdir(graph_dir)
			fig_dir = graph_dir + subfolder

			if not os.path.isdir(fig_dir) : os.mkdir(fig_dir)
			figs = list(map(plt.figure, plt.get_fignums()))
	
			if len(os.listdir(fig_dir))>0 : 
				for f in os.listdir(fig_dir):
					os.remove(fig_dir + f)
			for fig in figs: 
				fig.savefig(fig_dir + fig._suptitle.get_text() + '_' + self.run_id + '.jpg')

		


