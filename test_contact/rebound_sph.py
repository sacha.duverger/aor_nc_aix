import sys, os
sys.path.append(os.getcwd() + "/..")

import numpy as np, time as tm

# "PP" for Potential Particle
# "CLP" for Clump
model = "PP" 

# Material's parameters
material_params = {
    "Kn": 1.2e3,
    "Ks": 0.2*1.2e3,
    "nu": 0.37,
    "viscous_damping": 0.071,
    "en": 0.8, # Should correspond to viscous_damping (the information is redundant, en has to be specified for the clump model)
    "frictionAngle_part2part": 35.5*np.pi/180,
    "frictionAngle_part2wall": 27.2*np.pi/180,
    "density": 1111.
}

# Simulation's parameters
damping = 0
gravity = 0 # m/s**2
sphere_radius = 3.101e-3 # m
init_vels = [2.5e-1, 5.e-1] # m/s
init_rel_distance = 1e-1

# Other parameters
seed = 0
np.random.seed(seed) # Fixing random seed

# Import utility module for the selected model
if model == "PP": import utl_pp as utl
elif model == "CLP": import utl_clp as utl

# Create materials
material_ids = utl.create_materials(material_params)

# Set the model's engines
utl.set_engines(material_params, damping, gravity)

# Add particles
if model == "PP": # For this simulation to work with the PP model, make sure this line is commented in YADE's source : https://gitlab.com/yade-dev/trunk/-/blob/656906fab43bdd988dd01ce5ded5686eb385b3b8/pkg/potential/Ig2_PP_PP_ScGeom.cpp#L57
    Igeom = np.array((3.28600914143454e-12, 3.286145904978621e-12, 3.286197740532751e-12))
    Volume = sphere_radius**3 * 4/3. * np.pi
    k = 1

    aa=np.array([ 0.000000000000000,  0.8164965809277260,  0.0000000000000000, -0.8164965809277259])
    bb=np.array([ 0.000000000000000,  0.4714045207910317, -0.9428090415820634,  0.4714045207910317]) #-0.9428090415820634 instead of -1.0000000000000000
    cc=np.array([-1.000000000000000,  0.3333333333333334,  0.3333333333333334,  0.3333333333333334])
    dd=np.array([ 0.632993161855500,  0.6329931618555000,  0.6329931618555000,  0.6329931618555000])/(1e3) #FIXME: Check this. Do I need to subtract r?


    ## Body 0
    b0=Body()
    b0.aspherical=True
    b0.mask=1

    b0.shape=PotentialParticle( k=k, r=sphere_radius, R=sphere_radius, isBoundary=False, highlight=False, 
                                minAabb=1.2*np.array([sphere_radius]*3),			#1.2*Vector3(R,R,R),
                                maxAabb=1.2*np.array([sphere_radius]*3),			#1.2*Vector3(R,R,R),
                                minAabbRotated=1.2*np.array([sphere_radius]*3),		#1.2*Vector3(R,R,R), #FIXME: Not used with AabbMinMax=False
                                maxAabbRotated=1.2*np.array([sphere_radius]*3),	#1.2*Vector3(R,R,R), #FIXME: Not used with AabbMinMax=False
                                AabbMinMax=False, id=0)

    b0.shape.volume=Volume
    b0.shape.inertia=Igeom
    
    utils._commonBodySetup(b0, b0.shape.volume, b0.shape.inertia, material=material_ids[0], pos=(0,0,0), fixed=False)
    O.bodies.append(b0) 

    ## Body 1
    b1=Body()
    b1.aspherical=True
    b1.mask=1

    b1.shape=PotentialParticle( k=k, r=sphere_radius, R=sphere_radius, isBoundary=False, highlight=False, 
                                minAabb=1.2*np.array([sphere_radius]*3),			#1.2*Vector3(R,R,R),
                                maxAabb=1.2*np.array([sphere_radius]*3),			#1.2*Vector3(R,R,R),
                                minAabbRotated=1.2*np.array([sphere_radius]*3),		#1.2*Vector3(R,R,R), #FIXME: Not used with AabbMinMax=False
                                maxAabbRotated=1.2*np.array([sphere_radius]*3),	#1.2*Vector3(R,R,R), #FIXME: Not used with AabbMinMax=False
                                AabbMinMax=False, id=1)

    b1.shape.volume=Volume
    b1.shape.inertia=Igeom

    utils._commonBodySetup(b1, b1.shape.volume, b1.shape.inertia, material=material_ids[0], pos=(sphere_radius*(2+init_rel_distance),0,0), fixed=False)
    O.bodies.append(b1)

elif model=="CLP":
    b0 = O.bodies[O.bodies.append(sphere((0,0,0), sphere_radius, material=material_ids[0]))]
    b1 = O.bodies[O.bodies.append(sphere((sphere_radius*(2+init_rel_distance),0,0), sphere_radius, material=material_ids[0]))]

def save_data():
    global pen_depth_s, norm_f_s, ke_s
    if O.interactions.has(0,1,onlyReal=True):
        fn = O.interactions[0,1].phys.normalForce.norm()
        un = O.interactions[0,1].geom.penetrationDepth
    else:
        fn = nan
        un = nan
    ec = kineticEnergy()/ec0 # dimensionless kinetic energy of both particles

    pen_depth_s.append(un)
    norm_f_s.append(fn)
    ke_s.append(ec)

O.engines = O.engines + [PyRunner(iterPeriod=1, command="save_data()", dead=False)]

O.dt = 0.2*sqrt(0.5*O.bodies[0].state.mass/material_params["Kn"]) * 1e-1

O.saveTmp()

## Loop on velocities
for vel in init_vels:
    pen_depth_s, norm_f_s, ke_s = [], [], []

    O.loadTmp()

    b0 = O.bodies[b0.id] # Not sure why this is necessary
    b1 = O.bodies[b1.id]

    ### Setup simulation
    b1.state.vel = Vector3(-vel, 0, 0)
    ec0 = kineticEnergy()

    ### Run simulation
    tm_start = tm.time()
    O.run(int(1e5), wait=True)
    tm_end = tm.time()

    print("Contact test took {:f} seconds".format(tm_end-tm_start))

    ### Save data for this velocity
    with open("{:}_{:.3e}_contact.csv".format(model, vel), "w") as fil:
        fil.write("pen_depth\tnormal_force\tkinetic_energy\n")
        for a, b, c in zip(pen_depth_s, norm_f_s, ke_s): fil.write("{:.8e}\t{:.8e}\t{:.8e}\n".format(a, b, c))
