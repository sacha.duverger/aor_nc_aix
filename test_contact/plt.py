import sys, os, re, glob
from matplotlib import markers
sys.path.append(os.getcwd())

import numpy as np
import matplotlib as mpl, matplotlib.pyplot as plt
from matplotlib.lines import Line2D
mpl.rcParams['font.size'] = 25 # Setting the size of the fonts in the graphs

# Plot parameters
sphere_radius = 3.101e-3 # m
velocities = [2.5e-1, 5.e-1] # m/s
model_s = ["CLP", "PP"]

    ## Cosmetic parameters
color_s = ["blue", "green"]
label_s = ["Clumps", "Potential Particles"]
line_styles_s = ["solid", "dotted"] # For velocities

    ## Useful function
file_name_translate_dic = {' ': '_', '\\': '', '$': '', 'overline': 'mean_', '{': '', '}': '', '^': '_'}
def translate_str(text): return re.sub('({})'.format('|'.join(map(re.escape, file_name_translate_dic.keys()))), lambda m: file_name_translate_dic[m.group()], text)

# Creating save directory
    ## For graphs that plot all samples
save_dir = "graphs/"
if not os.path.isdir(save_dir) : os.mkdir(save_dir)
    
# Loading data
pen_dpt_sss, nf_sss, ke_sss, en_ss = [], [], [], []
for model in model_s:
    pen_dpt_ss, nf_ss, ke_ss, en_s = [], [], [], []
    for vel in velocities:
        with open("{:}_{:.3e}_contact.csv".format(model, vel), "r") as fil: lines = fil.readlines()[1:]
        pen_dpt_s, nf_s, ke_s = [], [], []
        for line in lines:
            values = [float(i) for i in line.split("\t")]
            pen_dpt_s.append(values[0])
            nf_s.append(values[1])
            ke_s.append(values[2])
        pen_dpt_ss.append(pen_dpt_s)
        nf_ss.append(nf_s)
        ke_ss.append(ke_s)
        en_s.append(ke_s[-1]/ke_s[0])
    pen_dpt_sss.append(pen_dpt_ss)
    nf_sss.append(nf_ss)
    ke_sss.append(ke_ss)
    en_ss.append(en_s)


#####################################################################################################################
# Plot data
fig1 = plt.figure(1, figsize=(15,7))

title1 = r"Normal force $F_n$ against penetration depth $D$"

    ## Loop on the models
for d_ss, nf_ss, color, label in zip(pen_dpt_sss, nf_sss, color_s, label_s):
    for d_s, nf_s, vel, linestyle in zip(d_ss, nf_ss, velocities, line_styles_s):
        nf_s = [nf*1e3 for nf in nf_s]
        # d_s = [d*1e2 for d in d_s]
        plt.plot(d_s, nf_s, color=color, label=label + ", V = {:.2e} m/s".format(vel), linestyle=linestyle)

        ## Plot arrows for direction
        # some_d_s_A, some_d_s_B = d_s[:int(len(d_s)*.3):int(len(d_s)/6)], d_s[int(len(d_s)*.8)::int(len(d_s)/6)]
        # some_nf_s_A, some_nf_s_B = nf_s[:int(len(nf_s)*.3):int(len(nf_s)/6)], nf_s[int(len(nf_s)*.8)::int(len(nf_s)/6)]
        # u_A, u_B = np.diff(some_d_s_A), np.diff(some_d_s_B)
        # v_A, v_B = np.diff(some_nf_s_A), np.diff(some_nf_s_B)
        # pos_x_A, pos_x_B = some_d_s_A[:-1] + u_A/2, some_d_s_B[:-1] + u_B/2
        # pos_y_A, pos_y_B = some_nf_s_A[:-1] + v_A/2, some_nf_s_B[:-1] + v_B/2
        # norm_A, norm_B = np.sqrt(u_A**2+v_A**2), np.sqrt(u_B**2+v_B**2)
        # plt.quiver(pos_x_A, pos_y_A, u_A/norm_A, v_A/norm_A, angles="xy", zorder=5, pivot="mid", color=color)
        # plt.quiver(pos_x_B, pos_y_B, u_B/norm_B, v_B/norm_B, angles="xy", zorder=5, pivot="mid", color=color)

# plt.title(title1)
plt.ylabel(r'$F_n$ ($mN$)')
plt.xlabel(r"$u_n$ ($m$)")
plt.legend(bbox_to_anchor=(1.01, .99), loc="upper left")
plt.ticklabel_format(style='sci', axis='x', scilimits=(-5,-5))


fig1.savefig(save_dir + translate_str(title1) + '.pdf', bbox_inches="tight", format='pdf')
