import numpy as np, time as tm

try: import plotext as plt
except ModuleNotFoundError: import matplotlib.pyplot as plt

# Simulation's parameters
sphere_radius = 3.101e-3 # m
init_vel = 2.5e-1
init_rel_distance = 1e-1

# Create materials
id_mat = O.materials.append(FrictMat(young=-1, poisson=-1, frictionAngle=35.5*np.pi/180, density=1111., label='granular'))

# Set the model's engines
O.engines = [
    ForceResetter(),
    InsertionSortCollider([PotentialParticle2AABB(aabbEnlargeFactor=1.1)], verletDist=0.0015, avoidSelfInteractionMask=2, label='collider'), #verletDist=0.01 or 0.00015
    InteractionLoop(
        [Ig2_PP_PP_ScGeom(twoDimension=False, unitWidth2D=1.0, calContactArea=False)], #FIXME: Check if we want constant stiffness or not
        [Ip2_FrictMat_FrictMat_KnKsPhys(kn_i=1.2e3, ks_i=0.2*1.2e3, Knormal=1.2e3, Kshear=0.2*1.2e3, useFaceProperties=False, viscousDamping=0.071)],
        [Law2_SCG_KnKsPhys_KnKsLaw(label='law', neverErase=False, allowViscousAttraction=False)],
        label="interloop"
    ),
    NewtonIntegrator(damping=0, exactAsphericalRot=True, gravity=[0,0,0], label='newton')
]

# Add particles
Igeom = np.array((3.28600914143454e-12, 3.286145904978621e-12, 3.286197740532751e-12))
Volume = sphere_radius**3 * 4/3. * np.pi
k = 1

## Body 0
b0=Body()
b0.aspherical=True
b0.mask=1

b0.shape=PotentialParticle( k=k, r=sphere_radius, R=sphere_radius, isBoundary=False, highlight=False, 
                            minAabb=1.2*np.array([sphere_radius]*3),			#1.2*Vector3(R,R,R),
                            maxAabb=1.2*np.array([sphere_radius]*3),			#1.2*Vector3(R,R,R),
                            minAabbRotated=1.2*np.array([sphere_radius]*3),		#1.2*Vector3(R,R,R), #FIXME: Not used with AabbMinMax=False
                            maxAabbRotated=1.2*np.array([sphere_radius]*3),	#1.2*Vector3(R,R,R), #FIXME: Not used with AabbMinMax=False
                            AabbMinMax=False, id=0)

b0.shape.volume=Volume
b0.shape.inertia=Igeom

utils._commonBodySetup(b0, b0.shape.volume, b0.shape.inertia, material=id_mat, pos=(0,0,0), fixed=False)
O.bodies.append(b0) 

## Body 1
b1=Body()
b1.aspherical=True
b1.mask=1

b1.shape=PotentialParticle( k=k, r=sphere_radius, R=sphere_radius, isBoundary=False, highlight=False, 
                            minAabb=1.2*np.array([sphere_radius]*3),			#1.2*Vector3(R,R,R),
                            maxAabb=1.2*np.array([sphere_radius]*3),			#1.2*Vector3(R,R,R),
                            minAabbRotated=1.2*np.array([sphere_radius]*3),		#1.2*Vector3(R,R,R), #FIXME: Not used with AabbMinMax=False
                            maxAabbRotated=1.2*np.array([sphere_radius]*3),	#1.2*Vector3(R,R,R), #FIXME: Not used with AabbMinMax=False
                            AabbMinMax=False, id=1)

b1.shape.volume=Volume
b1.shape.inertia=Igeom

utils._commonBodySetup(b1, b1.shape.volume, b1.shape.inertia, material=id_mat, pos=(sphere_radius*(2+init_rel_distance),0,0), fixed=False)
O.bodies.append(b1)


def save_data():
    global pen_depth_s, norm_f_s, ke_s
    if O.interactions.has(0,1,onlyReal=True):
        fn = O.interactions[0,1].phys.normalForce.norm()
        un = O.interactions[0,1].geom.penetrationDepth
    else: fn, un = nan, nan
    pen_depth_s.append(un)
    norm_f_s.append(fn)

O.engines +=[PyRunner(iterPeriod=1, command="save_data()", dead=False)]

O.dt = 0.2*sqrt(0.5*O.bodies[0].state.mass/1.2e3) * 1e-1

## Loop on velocities
pen_depth_s, norm_f_s = [], []

b0 = O.bodies[b0.id] # Not sure why this is necessary
b1 = O.bodies[b1.id]

### Setup simulation
b1.state.vel = Vector3(-init_vel, 0, 0)

### Run simulation
tm_start = tm.time()
O.run(int(1e5), wait=True)
tm_end = tm.time()

print("Contact test took {:f} seconds".format(tm_end-tm_start))

plt.plot(pen_depth_s, norm_f_s)
plt.show()