import sys, os
sys.path.append(os.getcwd() + "/..")

import numpy as np

# "PP" for Potential Particle
# "CLP" for Clump
model = "PP" 

# Material's parameters
material_params = {
    "Kn": 1.2e3,
    "Ks": 0.2*1.2e3,
    "nu": 0.37,
    "viscous_damping": 0.071,
    "en": 0.8, # Should correspond to viscous_damping (the information is redundant, en has to be specified for the clump model)
    "frictionAngle_part2part": 35.5*np.pi/180,
    "frictionAngle_part2wall": 27.2*np.pi/180,
    "density": 1111.
}

# Simulation's parameters
damping = 0
gravity = -9.81 # m/s**2
sphere_radius = 3.101e-3 # m
fall_rel_distance = 20 # relative to the radius

# Other parameters
seed = 0

# Read parameters from table if running in batch
readParamsFromTable(seed=int(seed), model=model)
from yade.params.table import *
np.random.seed(seed) # Fixing random seed

# Import utility module for the selected model
if model == "PP": import utl_pp as utl
elif model == "CLP": import utl_clp as utl

# Create materials
material_ids = utl.create_materials(material_params)

# Set the model's engines
utl.set_engines(material_params, damping, gravity)

# Add particles
if model == "PP": # For this simulation to work with the PP model, make sure this line is commented in YADE's source : https://gitlab.com/yade-dev/trunk/-/blob/656906fab43bdd988dd01ce5ded5686eb385b3b8/pkg/potential/Ig2_PP_PP_ScGeom.cpp#L57
    Igeom = np.array((3.28600914143454e-12, 3.286145904978621e-12, 3.286197740532751e-12))
    Volume = sphere_radius**3 * 4/3. * np.pi
    k = 1
    r = 5e-4

    aa=np.array([ 0.000000000000000,  0.8164965809277260,  0.0000000000000000, -0.8164965809277259])
    bb=np.array([ 0.000000000000000,  0.4714045207910317, -0.9428090415820634,  0.4714045207910317]) #-0.9428090415820634 instead of -1.0000000000000000
    cc=np.array([-1.000000000000000,  0.3333333333333334,  0.3333333333333334,  0.3333333333333334])
    dd=np.array([ 0.632993161855500,  0.6329931618555000,  0.6329931618555000,  0.6329931618555000])/(1e3) #FIXME: Check this. Do I need to subtract r?


    ## Body 0 (floor)

    dd=np.array([120e-3/2., 120e-3/2., 120e-3/2., 120e-3/2., 120e-3/2., 120e-3/2.]) - r

    b0=Body()
    b0.mask=3
    aabb=1.00*np.array((dd[0]+r,dd[2]+r,dd[4]+r))
    b0.shape=PotentialParticle(k=5e-2, r=r, R=2.3e-1, a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=0, isBoundary=True, AabbMinMax=True,
    maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=aabb, maxAabb=aabb, fixedNormal=False)
    utils._commonBodySetup(b0, 0, np.array((0,0,0)), material=material_ids[0], pos=[1e-1/2.,1e-1/2.,-1e-2/2.], fixed=True)
    O.bodies.append(b0)
    

    ## Body 1 (sphere)
    b1=Body()
    b1.aspherical=True
    b1.mask=1

    b1.shape=PotentialParticle( k=k, r=sphere_radius, R=sphere_radius, isBoundary=False, highlight=False, 
                                minAabb=1.2*np.array([sphere_radius]*3),			#1.2*Vector3(R,R,R),
                                maxAabb=1.2*np.array([sphere_radius]*3),			#1.2*Vector3(R,R,R),
                                minAabbRotated=1.2*np.array([sphere_radius]*3),		#1.2*Vector3(R,R,R), #FIXME: Not used with AabbMinMax=False
                                maxAabbRotated=1.2*np.array([sphere_radius]*3),	#1.2*Vector3(R,R,R), #FIXME: Not used with AabbMinMax=False
                                AabbMinMax=False, id=1)

    b1.shape.volume=Volume
    b1.shape.inertia=Igeom

    utils._commonBodySetup(b1, b1.shape.volume, b1.shape.inertia, material=material_ids[0], pos=(0,0,(1+fall_rel_distance)*sphere_radius), fixed=False)
    O.bodies.append(b1)

    n_iter = int(2e3)

elif model=="CLP": # Not working for now
    b0 = O.bodies[O.bodies.append(facet([[-10*sphere_radius, 0,0], [10*sphere_radius,-10*sphere_radius,0], [10*sphere_radius,10*sphere_radius,0]], fixed=True, material=material_ids[0]))]
    b1 = O.bodies[O.bodies.append(sphere((0,0,(1+fall_rel_distance)*sphere_radius), sphere_radius, material=material_ids[0]))]

    n_iter = int(4e3)

def save_data():
    global height_s, sim_time_s
    height_s.append(b1.state.pos[2])
    sim_time_s.append(O.time)

O.engines = O.engines + [PyRunner(iterPeriod=1, command="save_data()", dead=False)]

O.dt = 0.2*sqrt(0.5*O.bodies[1].state.mass/material_params["Kn"])

O.saveTmp()


height_s, sim_time_s = [], []

O.loadTmp()

b0 = O.bodies[b0.id] # Not sure why this is necessary
b1 = O.bodies[b1.id]

### Run simulation
O.run(n_iter, wait=True)

import plotext as plt
plt.plot(sim_time_s, height_s)
plt.show()

### Compute restitution coefficient
min_pos = min(height_s)

    #### According to the graph cutting height_s in two in enough (there is only "two maximums"). Beware if you change the number of iterations, or the time step, or gravity, or the fall height.
fst_reb, scd_reb = np.array_split(height_s, 2)

    #### Get maximums
hA = max(fst_reb) - min_pos
hB = max(scd_reb) - min_pos

print("Restitution coefficient: ", hB/hA)
