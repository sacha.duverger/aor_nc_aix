import numpy as np

# Simulation's parameters
Kn = 1.2e3
sphere_radius = 3.101e-3 # m
clump_diameter = 1e-2 # m
velocity = sphere_radius / 1e-3 # m/s

# Create material
mat_id = O.materials.append(FrictMat(young=-1, poisson=-1, frictionAngle=35.5*np.pi/180, density=1111.))

# Paricles' parameters
Igeom = np.array((3.28600914143454e-12, 3.286145904978621e-12, 3.286197740532751e-12))
Volume = 0.0000003924806765
k = 0.65

aa=np.array([ 0.000000000000000,  0.8164965809277260,  0.0000000000000000, -0.8164965809277259])
bb=np.array([ 0.000000000000000,  0.4714045207910317, -0.9428090415820634,  0.4714045207910317]) #-0.9428090415820634 instead of -1.0000000000000000
cc=np.array([-1.000000000000000,  0.3333333333333334,  0.3333333333333334,  0.3333333333333334])
dd=np.array([ 0.632993161855500,  0.6329931618555000,  0.6329931618555000,  0.6329931618555000])/(1e3) #FIXME: Check this. Do I need to subtract r?

# Particles' creation
    ## Body 0
b0=Body()
b0.aspherical=True

b0.shape=PotentialParticle( k=k, r=sphere_radius, R=np.sqrt(2)*sphere_radius, a=aa, b=bb, c=cc, d=dd, isBoundary=False, highlight=False, 
                            minAabb=1.2*np.array([sphere_radius]*3),			#1.2*Vector3(R,R,R),
                            maxAabb=1.2*np.array([sphere_radius]*3),			#1.2*Vector3(R,R,R),
                            minAabbRotated=1.2*np.array([sphere_radius]*3),		#1.2*Vector3(R,R,R), #FIXME: Not used with AabbMinMax=False
                            maxAabbRotated=1.2*np.array([sphere_radius]*3),	#1.2*Vector3(R,R,R), #FIXME: Not used with AabbMinMax=False
                            AabbMinMax=False, id=0)

b0.shape.volume=Volume
b0.shape.inertia=Igeom

utils._commonBodySetup(b0, b0.shape.volume, b0.shape.inertia, material=mat_id, pos=(0,0,0), fixed=True)
O.bodies.append(b0) 

    ## Body 1
b1=Body()
b1.aspherical=True

b1.shape=PotentialParticle( k=k, r=sphere_radius, R=sphere_radius, a=aa, b=bb, c=cc, d=dd, isBoundary=False, highlight=False, 
                            minAabb=1.2*np.array([sphere_radius]*3),			#1.2*Vector3(R,R,R),
                            maxAabb=1.2*np.array([sphere_radius]*3),			#1.2*Vector3(R,R,R),
                            minAabbRotated=1.2*np.array([sphere_radius]*3),		#1.2*Vector3(R,R,R), #FIXME: Not used with AabbMinMax=False
                            maxAabbRotated=1.2*np.array([sphere_radius]*3),	#1.2*Vector3(R,R,R), #FIXME: Not used with AabbMinMax=False
                            AabbMinMax=False, id=1)

b1.shape.volume=Volume
b1.shape.inertia=Igeom

utils._commonBodySetup(b1, b1.shape.volume, b1.shape.inertia, material=mat_id, pos=(clump_diameter,0,0), fixed=True)
O.bodies.append(b1)

# Setup engines
    ## Pyrunner
def printer(): print('\nIteration: ', O.iter, 
                     '\nRelative distance between centers:', abs(b1.state.pos[0]-b0.state.pos[0])/clump_diameter, ' %',
                     '\nNumber of interactions: ', len(O.interactions),
                     '\nNumber of real interactions: ', O.interactions.countReal())

    ## Engines
O.engines = [
    ForceResetter(),
    InsertionSortCollider([PotentialParticle2AABB(aabbEnlargeFactor=1.1)], verletDist=0.0015, avoidSelfInteractionMask=2, label='collider'), #verletDist=0.01 or 0.00015
    InteractionLoop(
        [Ig2_PP_PP_ScGeom(twoDimension=False, unitWidth2D=1.0, calContactArea=False)], #FIXME: Check if we want constant stiffness or not
        [Ip2_FrictMat_FrictMat_KnKsPhys(kn_i=Kn, ks_i=0.2*Kn, Knormal=Kn, Kshear=0.2*Kn, useFaceProperties=False, viscousDamping=0.071)],
        [Law2_SCG_KnKsPhys_KnKsLaw(label='law', neverErase=False, allowViscousAttraction=True)],
        label="interloop"
    ),
    NewtonIntegrator(damping=0, exactAsphericalRot=True, gravity=[0,0,0], label='newton'),
    PyRunner(iterPeriod=1, command="printer()", dead=False)
]

# Time step
O.dt = 0.2*sqrt(0.5*O.bodies[0].state.mass/Kn)

# Compute number of iterations to perform so particles end up completly overlaped
n_iter = int( abs(b1.state.pos[0]-b0.state.pos[0]) / velocity / O.dt)

# Set velocity and run
b1.state.vel = Vector3(-velocity, 0, 0)
O.run(n_iter)
