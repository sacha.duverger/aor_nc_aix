import sys, os, glob
sys.path.append(os.getcwd())

import numpy as np
from yade._packSpheres import SpherePack

# "PP" for Potential Particle
# "CLP" for Clump
model = "PP" 

series = "param_study" # "param_study" for PP parametric study, "main" for main series

# Material's parameters
material_params = {
    "Kn": 1.2e3,
    "Ks": 0.2*1.2e3,
    "nu": 0.37,
    "viscous_damping": 0.071,
    "en": 0.8, # Should correspond to viscous_damping (the information is redundant, en has to be specified for the clump model)
    "frictionAngle_part2part": 35.5*np.pi/180,
    "frictionAngle_part2wall": 27.2*np.pi/180,
    "density": 1111.
}

# Simulation's parameters
damping = .2
gravity = -9.81
uF_threshold = 1e-2


# Other parameters
seed = 0
sample_dir = "samples/"
if not os.path.isdir(sample_dir) : os.mkdir(sample_dir)

# Read parameters from table if running in batch
readParamsFromTable(seed=int(seed), model=model, Ks=material_params["Ks"], density=material_params["density"], series=series)
from yade.params.table import *
np.random.seed(seed) # Fixing random seed
material_params["Ks"] = Ks
material_params["density"] = density

# Import utility module for the selected model
import utl_pp as utl

# Load existing sample
sample = glob.glob("samples/CLP_2150_parts_{:d}.yade*.bz2".format(seed))[0] # Beware if you have several samples with the same parameters but different yade version
O.saveTmp()
O.load(sample)
print("Sample {:} loaded".format(sample))

# Get sphere pack from Clump centers, then reset simulation
sp = SpherePack([(b.state.pos, 1e-2) for b in O.bodies if type(b.shape)==Clump])
O.loadTmp()

# Create materials
material_ids = utl.create_materials(material_params)

# Set the model's engines
utl.set_engines(material_params, damping, gravity)

# Add particles
n_parts = utl.add_particles(material_ids[0], seed, sphere_pack=sp)

# Construct box
walls = utl.make_box(material_ids[1])
bbb, b0, bA, bB, bC, bD, bbA, bbB, bbC, bbD, bH = walls

# Add PyRunners
utl.set_pyruners(utl.gen_from_clp_pyrunners)

# Run simulation if it is part of batch
def run():
    ## Print all versions so saved file is easier to reopen
    printAllVersions()

    print("Running with: \n\tdt ~= {:.2e} s\n\tKs = {:.0f} N/m\n\tdensity = {:.0f} kg/m3".format(O.dt, Ks, density), flush=True)
    O.run(wait=True)

    if series=="main": sample_name = "PP_{:d}_parts_{:d}.yade{:}.xml.bz2".format(n_parts, seed, version.split("-")[-1])
    elif series=="param_study": sample_name = "PP_{:d}_parts_{:d}__density{:.0f}_Ks{:.0f}.yade{:}.xml.bz2".format(n_parts, seed, density, Ks, version.split("-")[-1])

    ## Save walls
        ### Create a dictionnary with all wall objects
    walls_names = [[ i for i, a in globals().items() if id(a) == id(x)][0] for x in walls]
    wall_dict = {key: val for key,val in zip(walls_names, walls)}

        ### Use YADE's saveVars
    saveVars('walls', **wall_dict)
    
    O.save(sample_dir + sample_name)

if runningInBatch(): run()




