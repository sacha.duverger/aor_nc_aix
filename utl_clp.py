import sys, os
sys.path.append(os.getcwd())

import numpy as np
from yade import utils, pack, geom
from yade.utils import *
from yade.pack import *

def create_materials(material_params):
    id_p2p = O.materials.append(ViscElMat(density=material_params["density"], 
                                          young=material_params["Kn"]/sphere_radius, 
                                          poisson=material_params["nu"],
                                          frictionAngle=material_params["frictionAngle_part2part"], 
                                          en=material_params["en"], 
                                          et=material_params["en"], 
                                          label="resin"))

    id_p2w = O.materials.append(ViscElMat(density=material_params["density"], 
                                          young=material_params["Kn"]/sphere_radius, 
                                          poisson=material_params["nu"],
                                          frictionAngle=material_params["frictionAngle_part2wall"], 
                                          en=material_params["en"], 
                                          et=material_params["en"], 
                                          label="acrylic"))

    return id_p2p, id_p2w

def set_engines(material_params, damping, gravity):
    # Create MatchMakers even though contact properties are not distributed (Ip2_ViscElMat_ViscElMat_ViscElPhys requires it)
    mm_phi = MatchMaker(algo='val', 
                        matches=np.array([[0, 0, material_params["frictionAngle_part2part"]],
                                          [0, 1, material_params["frictionAngle_part2wall"]]]), 
                        val=material_params["frictionAngle_part2part"])
    mm_en = MatchMaker(algo='val', 
                        matches=np.array([[0, 0, material_params["en"]],
                                          [0, 1, material_params["en"]]]), 
                        val=material_params["en"])

    O.engines = [
        ForceResetter(),
        InsertionSortCollider([Bo1_Sphere_Aabb(), Bo1_Facet_Aabb()],
                               label='collider'),
        InteractionLoop([Ig2_Sphere_Sphere_ScGeom(), Ig2_Facet_Sphere_ScGeom()],
                        [Ip2_ViscElMat_ViscElMat_ViscElPhys(frictAngle=mm_phi, en=mm_en)],
                        [Law2_ScGeom_ViscElPhys_Basic()],
                        label="interloop"),
        # GlobalStiffnessTimeStepper(timestepSafetyCoefficient=.8, viscEl=True), # Take off GSTS to correspond to PP model
        NewtonIntegrator(gravity=[0,0,gravity], damping=damping, label='newton')
	]


def add_particles(material_id, seed):
    # Get the centers of the 4 spheres
    ref_pos = np.array([0,0,0])
    e = (clump_radius-sphere_radius)*4/np.sqrt(6) # edge length
    h = np.sqrt(6)/3*e # heigth
    Rcirc = np.sqrt(6)/4*e # radius of the circumsphere
    proj_e2base = np.sqrt(Rcirc**2-(h-Rcirc)**2) # projection of an edge to the bottom base

    vs = []
    vs.append(ref_pos + np.array([0, Rcirc, 0])) # vertex 1 (the one on the top of the pyramid)
    vs.append(ref_pos + np.array([0, Rcirc-h, -proj_e2base])) # vertex 2 (the one behind the pyramid)
    a = np.sqrt(3*e**2/4)-proj_e2base
    vs.append(ref_pos + np.array([-e/2, Rcirc-h, a])) # vertex 3 (the one on the left)
    vs.append(ref_pos + np.array([e/2, Rcirc-h, a])) # vertex 4 (the one on the right)

    # Create the clump
    clp = SpherePack([(c, sphere_radius) for c in vs])
    
    # Make a cloud of the clump
    sp = SpherePack()
    sp.makeClumpCloud((0,0,0), (.1,.1,.19*5), [clp], num=2150, seed=seed) #makeCloud(minCorner=(0,0,0), maxCorner=(100,190,100), num=n_part)
    sp.toSimulation()
    print(int(len(sp)/4),' particles were added to the simulation')

        ## Assign material
    for b in O.bodies:
        if type(b.shape)==Sphere: b.material = O.materials[material_id]

        ## Assign mass and inertia
    for b in O.bodies:
        if type(b.shape)==Clump: 
            b.state.mass = clp_vol * O.materials[material_id].density
            b.state.inertia = clp_inertia

    # Set time step
    Kn = O.materials[material_id].young * sphere_radius
    clp_mass = clp_vol * O.materials[material_id].density
    O.dt = 0.2 * np.sqrt(0.5 * clp_mass / Kn)

    return int(len(sp)/4) # Numer of particles, not sphere

def make_box(material_id):
    hl = .1 # m, horizontal length
    vl = .19 * 5 # m, vertical length, it is increased so all particles can fit when they are added as a cloud
    ll = .02 # m, ledge length

    facets_pos = []

    # Define facets' vertices (each wall is made of 2 triangular facets)
    facets_pos.append(([0,0,0], [0,hl,0], [hl,hl,0]))# Bottom A
    facets_pos.append(([0,0,0], [hl,0,0], [hl,hl,0]))# Bottom B

    facets_pos.append(([0,0,0], [hl,0,0], [hl,0,vl]))# Left A
    facets_pos.append(([0,0,0], [0,0,vl], [hl,0,vl]))# Left B

    facets_pos.append(([0,hl,0], [hl,hl,0], [hl,hl,vl]))# Right A
    facets_pos.append(([0,hl,0], [0,hl,vl], [hl,hl,vl]))# Right B

    facets_pos.append(([0,0,0], [0,hl,0], [0,hl,ll]))# Back A (ledge)
    facets_pos.append(([0,0,0], [0,0,ll], [0,hl,ll]))# Back B (ledge)
    facets_pos.append(([0,0,0], [0,hl,0], [0,hl,vl]))# Back A (door)
    facets_pos.append(([0,0,0], [0,0,vl], [0,hl,vl])) # Back B (door)

    facets_pos.append(([hl,0,0], [hl,hl,0], [hl,hl,vl]))# Front A
    facets_pos.append(([hl,0,0], [hl,0,vl], [hl,hl,vl]))# Front B

    # Create a list of Facet body instances
    facets = []
    for f_pos in facets_pos: facets.append(facet(f_pos, fixed=True, material=O.materials[material_id]))

    # Add facets to simulation
    for f in facets: O.bodies.append(f)

    return facets

def set_pyruners(pyrunner_commands):
    for i, [label, value] in enumerate(pyrunner_commands):
        O.engines = O.engines + [PyRunner(iterPeriod=100,command=value,label=label, dead=False)]

#####################################################################################################################
# Pyrunner functions

def stability_checker_gen(uF_threshold):
    # The minimum number of iteration before considering the unbalenced force (1e4) was determined by running a simulation and monitoring the number of interactions
    if unbalancedForce()<uF_threshold and O.iter-init_iter > 1e4: 
        stability_checker_gen_pr.dead = True
        O.pause()
        print("Iteration {:d}: sample's generation is over".format(O.iter))

def stability_checker_sim(uF_threshold, door, h0):
    if unbalancedForce()<uF_threshold and door.state.pos[2]-h0 > .19: 
        stability_checker_sim_pr.dead = True
        O.pause()
        print("Iteration {:d}: sample is stable, simulation is over".format(O.iter))

def eraser():
	global n_erased

    # Loop on clumps
	for b in O.bodies:
		if type(b.shape)==Clump:

            ## Erase body if it fell lower than the box
			if b.state.pos[2]<-.01:
				O.bodies.erase(b.id, True)
				n_erased += 1

#####################################################################################################################
# Useful functions

def avg_coord_nmbr(): return yade.utils.avgNumInteractions(skipFree=True, considerClumps=True)

#####################################################################################################################
# Clump's parameters
    ## Shape parameters
sphere_radius = 3.101e-3 # m
clump_radius = 5e-3 # m
        ### For discretization = 1000
clp_vol = 3.329921274267349e-07
clp_inertia = (2.582826823842934883e-09,2.584117701565630446e-09,2.584208632821741795e-09)

    ## Other parameters
n_erased = 0 # number of particle erased
init_iter = O.iter # assigned when the module is loaded (for a generation init_iter = 0, for a simulation init_iter != 0)

    ## Pyrunners lists
gen_pyrunners = [
    ("stability_checker_gen_pr", "utl.stability_checker_gen(uF_threshold)"),
]

sim_pyrunners = [
    ("eraser", "utl.eraser()"),
    ("stability_checker_sim_pr", "utl.stability_checker_sim(uF_threshold, door, h0)")
]

#####################################################################################################################
# Postprocessing functions

## Define a function that checks if a point is in a particle
def _is_in_clump_member(point, b_id): 
    return np.linalg.norm(O.bodies[b_id].state.pos-point) < O.bodies[b_id].shape.radius

def is_in_particle(point, clp_id):
    sph_ids = list(O.bodies[clp_id].shape.members.keys())
    in_part = []
    for sph_id in sph_ids: in_part.append(_is_in_clump_member(point, sph_id))
    return np.array(in_part).any()

## Define sample's bouding box function (necesary because YADE's aabbExtrema doesn't work for potential particles)
def custom_aabbExtrema(): return aabbExtrema()


#####################################################################################################################
# Save / load functions to pass custom parameters from generation's to simulation's script

def save_walls(bodies):
    """
    This function saves bodies ids as a list of ints in a PyRunner set in its command. It will be used to save walls' ids.
    (This is not great, but I didn't found a better way to pass python variables through O.save / O.load)
    """
    O.engines += [PyRunner(command="_".join([str(b.id) for b in bodies]))]

def load_walls():
    ints_str = O.engines[-1].command
    O.engines = O.engines[:-1]
    walls = [O.bodies[int(i)] for i in ints_str.split("_")]
    return walls