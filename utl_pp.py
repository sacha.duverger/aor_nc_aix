import sys, os
sys.path.append(os.getcwd())

import numpy as np
from yade import utils, pack, geom
from yade.utils import *
from yade.pack import *

def set_engines(material_params, damping, gravity):
    """
    Vasileios' comments :
    # FIXME: Revisit the verlet distance!!!
    # Maybe assume either half the circumdiameter of the clump or half the radius of an individual sphere

    # FIXME: In the insertion collider, I have increased the Aabb by 10%. Can I avoid that?

    # FIXME: Restore:
    #	Kn=1e5
    #	verlet distance = 0.0015
    #	newton.damping=0.0 after we deposit to the lower box
    """
    O.engines = [
        ForceResetter(),
        InsertionSortCollider([PotentialParticle2AABB(aabbEnlargeFactor=1.1)], verletDist=0.0015, avoidSelfInteractionMask=2, label='collider'), #verletDist=0.01 or 0.00015
        InteractionLoop(
            [Ig2_PP_PP_ScGeom(twoDimension=False, unitWidth2D=1.0, calContactArea=False)], #FIXME: Check if we want constant stiffness or not
            [Ip2_FrictMat_FrictMat_KnKsPhys(kn_i=material_params["Kn"], ks_i=material_params["Ks"], Knormal=material_params["Kn"], Kshear=material_params["Ks"], useFaceProperties=False, viscousDamping=material_params["viscous_damping"])],
            [Law2_SCG_KnKsPhys_KnKsLaw(label='law', neverErase=False, allowViscousAttraction=False)],
            label="interloop"
        ),
        NewtonIntegrator(damping=damping, exactAsphericalRot=True, gravity=[0,0,gravity], label='newton')
    ]


def create_materials(material_params):
    id_p2p = O.materials.append(FrictMat(young=-1, poisson=-1, frictionAngle=material_params["frictionAngle_part2part"], density=material_params["density"], label='granular')) # Friction angle between clump-clump
    id_p2w = O.materials.append(FrictMat(young=-1, poisson=-1, frictionAngle=material_params["frictionAngle_part2wall"], density=0, label='box'))      # Friction angle between clump-box

    return id_p2p, id_p2w


def add_particles(material_id, seed, sphere_pack="auto"):
    packing='random'
    #mn=Vector3(Ds/2.,Ds/2.,t_190+Ds/2.)
    #mx=Vector3(t_100-Ds/2.,t_100-Ds/2.,4*t_190) #FIXME: This is the real one
    mn=np.array((Ds/2.,Ds/2.,t_190+Ds/2.))
    mx=np.array((t_100-Ds/2.,t_100-Ds/2.,2.9*t_190)) #FIXME: This is the real one

    if sphere_pack=="auto": sp=pack.randomDensePack(pack.inAlignedBox(mn,mx),radius=Ds/2.,returnSpherePack=True,spheresInCell=500,material=O.materials['granular'],seed=seed) #FIXME: 15 November 2020
    else: sp = sphere_pack
    print(len(sp))
    # -------------------------------------------------------------------- #
    # Sort packing in ascending Z coordinates and delete excess particles to achieve sample sizes of exactly 25k, 50k, 100k
    zValues=[]
    for s in sp: zValues.append(s[0][2])

    from operator import itemgetter
    indices, zValues_sorted = zip(*sorted(enumerate(zValues), key=itemgetter(1)))
    list(zValues)
    list(indices)

    sp_new=[]
    for i in range(0,len(sp)): sp_new.append(sp[indices[i]])

    sp_new=sp_new[0:2150] #FIXME 2150 # 5 November 2020
    sp=sp_new # Used for debugging purposes
    #O.bodies.append(sp_new)


    # -------------------------------------------------------------------- #
    # Count the number of spherical particles to verify sample size. We can comment this out later on.
    print('The total number of spheres is: ',len(sp))

    minAabbValue=1.2*np.array((R,R,R))
    maxAabbValue=1.2*np.array((R,R,R))

    for s in sp:
        #FIXME: Check this: We can subtract a percentage of "r" from all d[i] values and then adjust k
        # We can generate a number of particle shapes in matlab and find the best fit, by adjusting this percentage and the value of k.
        # dd=dd#-r/30. #FIXME: Check whether I need to change this (if I do, it will change the vertices of the inner, sharp tetrahedron)

        b=Body()
        b.aspherical=True
        b.mask=1
        wire=False
        color=np.array((random.random(),random.random(),random.random()))

        b.shape=PotentialParticle( k=k, r=r, R=R, a=aa, b=bb, c=cc, d=dd_part, isBoundary=False, color=color, wire=wire, highlight=False, 
        minAabb=minAabbValue,			#1.2*Vector3(R,R,R),
        maxAabb=maxAabbValue,			#1.2*Vector3(R,R,R),
        minAabbRotated=minAabbValue,		#1.2*Vector3(R,R,R), #FIXME: Not used with AabbMinMax=False
        maxAabbRotated=maxAabbValue,		#1.2*Vector3(R,R,R), #FIXME: Not used with AabbMinMax=False
        AabbMinMax=False, id=len(O.bodies) )

    	# V=4/3.*pi*R**3 #FIXME: I need to calculate the actual particle volume
    	# geomInert=2/5.*V*(R)**2 #FIXME: I need to calculate the actual particle inertias

        b.shape.volume=Volume	#V
        b.shape.inertia=Igeom	#Vector3(geomInert,geomInert,geomInert)
        
        if packing=='random':
            pos=s[0]
        elif packing=='custom':
            pos=s
        else:
            print('Unidentified packing regime')

        utils._commonBodySetup(b, b.shape.volume, b.shape.inertia, material=material_id, pos=pos, fixed=False) #s[0] stores center
    	# random.seed(5)
        rng = np.random.default_rng(seed)
        b.state.ori=Quaternion((rng.random(),rng.random(),rng.random()),rng.random()) # FIXME: Change the angle from [-2pi-2pi]
        O.bodies.append(b)
    

    Kn = interloop.physDispatcher.functors[0].Knormal
    O.dt = 0.2*sqrt(0.5*O.bodies[0].state.mass/Kn)

    return len(sp)


def make_box(material_id):
    #Faces of the box
    color=[0,0.5,1]
    r=0.05*t_10
    k=0.05
    R=1.0*t_230

    # ------------------------------------------------------------------------------------------------------------------------------------------
    # Box below
    # ------------------------------------------------------------------------------------------------------------------------------------------

    V=0
    geomInert=np.array((0,0,0))

    # Horizontal base: bbb
    dd=np.array([t_120/2., t_120/2., t_120/2., t_120/2., t_10/2., t_10/2.]) - r

    bbb=Body()
    bbb.mask=3
    aabb=1.00*np.array((dd[0]+r,dd[2]+r,dd[4]+r))
    bbb.shape=PotentialParticle(k=k, r=r, R=R,a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=True, color=color, AabbMinMax=True,
    maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=aabb, maxAabb=aabb, fixedNormal=False)
    utils._commonBodySetup(bbb, V, geomInert, material=material_id, pos=[t_100/2.,t_100/2.,-t_10/2.], fixed=True)
    O.bodies.append(bbb)

    # ------------------------------------------------------------------------------------------------------------------------------------------
    # Vertical Faces
    # Small front face b0
    dd=np.array([t_5/2., t_5/2., t_100/2., t_100/2., t_20/2., t_20/2.]) - r

    b0=Body()
    b0.mask=3
    aabb=1.00*np.array((dd[0]+r,dd[2]+r,dd[4]+r))
    b0.shape=PotentialParticle(k=k, r=r, R=R, a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=True, color=color, AabbMinMax=True,
    maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=np.array((0.8*dd[0]+r,dd[2]+r,dd[4]+r)), maxAabb=np.array((0.8*dd[0]+r,dd[2]+r,dd[4]+r)), fixedNormal=False) #minAabb=aabb, maxAabb=aabb,
    utils._commonBodySetup(b0, V, geomInert, material=material_id, pos=[-t_5/2.,t_100/2.,t_20/2.], fixed=True)
    O.bodies.append(b0)

    # ------------------------------------------------------------------------------------------------------------------------------------------
    dd=np.array([t_5/2., t_5/2., t_100/2., t_100/2., t_230/2., t_230/2.]) - r

    # COMMENT: This is the front movable plate: Have made it transparent manually
    bA=Body()
    bA.mask=3
    aabb=1.00*np.array((dd[0]+r,dd[2]+r,dd[4]+r))
    bA.shape=PotentialParticle(k=k, r=r, R=R, a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=True, color=[1.0,1.0,0], AabbMinMax=True, wire=True,
    maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=np.array((0.8*dd[0]+r,dd[2]+r,dd[4]+r)), maxAabb=np.array((0.8*dd[0]+r,dd[2]+r,dd[4]+r)), fixedNormal=False)
    utils._commonBodySetup(bA, V, geomInert, material=material_id, pos=[-t_5/2.,t_100/2.,t_20+t_230/2.], fixed=True)
    O.bodies.append(bA)

    # ------------------------------------------------------------------------------------------------------------------------------------------
    # Back face bB
    dd=np.array([t_10/2., t_10/2., t_100/2., t_100/2., t_190/2., t_190/2.]) - r

    bB=Body()
    bB.mask=3
    aabb=1.00*np.array((dd[0]+r,dd[2]+r,dd[4]+r))
    bB.shape=PotentialParticle(k=k, r=r, R=R, a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=True, color=color, AabbMinMax=True,
    maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=aabb, maxAabb=aabb, fixedNormal=False)
    utils._commonBodySetup(bB, V, geomInert, material=material_id, pos=[(t_100+t_10/2.),t_100/2.,t_190/2.], fixed=True)
    O.bodies.append(bB)

    # ------------------------------------------------------------------------------------------------------------------------------------------
    # Side face bC
    dd=np.array([ t_120/2., t_120/2., t_10/2., t_10/2., t_190/2., t_190/2.]) - r

    bC=Body()
    bC.mask=3
    aabb=1.00*np.array((dd[0]+r,dd[2]+r,dd[4]+r))
    bC.shape=PotentialParticle(k=k, r=r, R=R, a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=True, color=[1,0,0], AabbMinMax=True,
    maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=aabb, maxAabb=aabb, fixedNormal=False)
    utils._commonBodySetup(bC, V, geomInert, material=material_id, pos=[t_100/2.,t_100+t_10/2.,t_190/2.], fixed=True)
    O.bodies.append(bC)

    # ------------------------------------------------------------------------------------------------------------------------------------------
    # Side face bD
    dd=np.array([ t_120/2., t_120/2., t_10/2., t_10/2., t_190/2., t_190/2.]) - r

    bD=Body()
    bD.mask=3
    aabb=1.00*np.array((dd[0]+r,dd[2]+r,dd[4]+r))
    bD.shape=PotentialParticle(k=k, r=r, R=R, a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=True, color=[0,1,0], AabbMinMax=True, wire=False,
    maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=aabb, maxAabb=aabb, fixedNormal=False)
    utils._commonBodySetup(bD, V, geomInert, material=material_id, pos=[t_100/2.,-t_10/2.,t_190/2.], fixed=True)
    O.bodies.append(bD)


    # ------------------------------------------------------------------------------------------------------------------------------------------
    # Box above
    # ------------------------------------------------------------------------------------------------------------------------------------------

    # Vertical Faces
    dd=np.array([t_5/2., t_5/2., t_100/2., t_100/2., t_290/2., t_290/2.]) - r

    # COMMENT: This is the front plate of the box above: Have made it transparent manually
    bbA=Body()
    bbA.mask=3
    aabb=1.00*np.array((dd[0]+r,dd[2]+r,dd[4]+r))
    bbA.shape=PotentialParticle(k=k, r=r, R=R, a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=True, color=color, AabbMinMax=True, wire=True,
    maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=np.array((0.8*dd[0]+r,dd[2]+r,dd[4]+r)), maxAabb=np.array((0.8*dd[0]+r,dd[2]+r,dd[4]+r)), fixedNormal=False)
    utils._commonBodySetup(bbA, V, geomInert, material=material_id, pos=[-t_5/2.,t_100/2.,t_20+t_230+t_290/2.], fixed=True)
    O.bodies.append(bbA)

    # ------------------------------------------------------------------------------------------------------------------------------------------
    # Above back face bB
    dd=np.array([t_10/2., t_10/2., t_100/2., t_100/2., t_350/2., t_350/2.]) - r

    bbB=Body()
    bbB.mask=3
    aabb=1.00*np.array((dd[0]+r,dd[2]+r,dd[4]+r))
    bbB.shape=PotentialParticle(k=k, r=r, R=R, a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=True, color=color, AabbMinMax=True,
    maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=aabb, maxAabb=aabb, fixedNormal=False)
    utils._commonBodySetup(bbB, V, geomInert, material=material_id, pos=[(t_100+t_10/2.),t_100/2.,t_190+t_350/2.], fixed=True)
    O.bodies.append(bbB)

    # ------------------------------------------------------------------------------------------------------------------------------------------
    # Above side face bC
    dd=np.array([ t_120/2., t_120/2., t_10/2., t_10/2., t_350/2., t_350/2.]) - r

    bbC=Body()
    bbC.mask=3
    aabb=1.00*np.array((dd[0]+r,dd[2]+r,dd[4]+r))
    bbC.shape=PotentialParticle(k=k, r=r, R=R, a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=True, color=[1,0,0], AabbMinMax=True,
    maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=aabb, maxAabb=aabb, fixedNormal=False)
    utils._commonBodySetup(bbC, V, geomInert, material=material_id, pos=[t_100/2.,t_100+t_10/2.,t_190+t_350/2.], fixed=True)
    O.bodies.append(bbC)

    # ------------------------------------------------------------------------------------------------------------------------------------------
    # Above Side face bD
    dd=np.array([ t_120/2., t_120/2., t_10/2., t_10/2., t_350/2., t_350/2.]) - r

    bbD=Body()
    bbD.mask=3
    aabb=1.00*np.array((dd[0]+r,dd[2]+r,dd[4]+r))
    bbD.shape=PotentialParticle(k=k, r=r, R=R, a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=True, color=[0,1,0], AabbMinMax=True, wire=False,
    maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=aabb, maxAabb=aabb, fixedNormal=False)
    utils._commonBodySetup(bbD, V, geomInert, material=material_id, pos=[t_100/2.,-t_10/2.,t_190+t_350/2.], fixed=True)
    O.bodies.append(bbD)

    # ------------------------------------------------------------------------------------------------------------------------------------------
    # Horizontal movable plate at elevation +190mm
    dd=np.array([t_190/2., t_190/2., t_100/2., t_100/2., t_5/2., t_5/2.]) - r

    bH=Body()
    bH.mask=3
    aabb=1.00*np.array((dd[0]+r,dd[2]+r,dd[4]+r))
    bH.shape=PotentialParticle(k=k, r=r, R=R,a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=True, color=color, AabbMinMax=True, wire=False,
    maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=aabb, maxAabb=aabb, fixedNormal=False)
    utils._commonBodySetup(bH, V, geomInert, material=material_id, pos=[t_190/2.,t_100/2.,t_190+t_5/2.], fixed=True)
    O.bodies.append(bH)

    return bbb, b0, bA, bB, bC, bD, bbA, bbB, bbC, bbD, bH

def set_pyruners(pyrunner_commands):
    for i, [label, value] in enumerate(pyrunner_commands):
        O.engines = O.engines + [PyRunner(iterPeriod=100,command=value,label=label, dead=i>0)]

#####################################################################################################################
# Pyrunner functions

def controlStage(uF_threshold):
	# print( utils.unbalancedForce(), utils.kineticEnergy() )
	# print(str(O.iter)+" cS is running")
	maxPos=max(b.state.pos[2] for b in O.bodies if len(b.state.blockedDOFs)==0) + Ds/2. #or Ds/1. to be sure

	if maxPos<t_190+t_350: # sampleIsStable(uF_threshold) and maxPos<t_190+t_350:
		print("The sample is stable on the upper box!")
		newton.damping=0.2 #FIXME: Maybe I could still keep some newton damping here, like 20% and turn it to 0.0 before we start moving the vertical plate
		mH.dead=False
		# calm() # FIXME: Do I need to invoke calm() just in case?
		cS.dead=True
		print(str(O.iter)+" mH is running")

def moveHorizontalPlate(bH):
	bH = O.bodies[bH.id] # Necessary to update bH, not its copy (not sure how variable scopes works with YADE and custom modules)
	# print(str(O.iter)+" mH is running")
	if bH.state.vel.norm()==0:
		newton.damping=0.20 # FIXME: Maybe keep some local damping here if the sample cannot achieve equilibrium in the bottom box?
		bH.state.vel=[43/units*1e2,0,0] # 0.043 m/s = 43 mm/s
		print(str(O.iter)+" bH is moving")
	elif bH.state.pos[0]>=t_190+t_5/2.:
			print(str(O.iter)+" bH is deleted")
			O.bodies.erase(bH.id)
			dUB.dead=False
			mH.dead=True
			print(str(O.iter)+" dUB is running")

def deleteUpperBox(bbA, bbB, bbC, bbD, uF_threshold):
	bbA = O.bodies[bbA.id]
	bbB = O.bodies[bbB.id]
	bbC = O.bodies[bbC.id]
	bbD = O.bodies[bbD.id]
	# print(str(O.iter)+" dUB is running")
	maxPos=max(b.state.pos[2] for b in O.bodies if len(b.state.blockedDOFs)==0) + Ds/2. #or Ds/1. to be sure
	if maxPos<t_190 and unbalancedForce()<uF_threshold:
		O.bodies.erase(bbA.id)
		O.bodies.erase(bbB.id)
		O.bodies.erase(bbC.id)
		O.bodies.erase(bbD.id)
		dUB.dead=True
		O.pause()
		print(str(O.iter)+" sample's generation is over")

def moveVerticalPlate(bA, wall_velocity, damping, uF_threshold):
	global mVIter, vW
	bA = O.bodies[bA.id]
	# print(str(O.iter)+" mV is running")
	maxPos=max( b.state.pos[2] for b in O.bodies if len(b.state.blockedDOFs)==0 ) + Ds/2. #or Ds/1. to be sure
	# calm() #FIXME: To be removed
	if bA.state.vel.norm()==0 and bA.state.pos[2]-bA.state.refPos[2] < t_190:
		newton.damping = damping
		vW = [0,0,wall_velocity] #0.043m = 43mm/s
		bA.state.vel=vW #0.043m = 43mm/s
		print(str(O.iter)+" bA is moving")
		mVIter=O.iter
		# print('mVTime: ', mVTime)
	elif bA.state.pos[2]-bA.state.refPos[2] >= t_190: #  #FIXME: Check if I still need the second check
		print(str(O.iter)+" bA is deleted")
		O.bodies.erase(bA.id)
		mV.dead=True
		fS.dead=False
		print(str(O.iter)+" fS is running")
	erase_parts()

def finalStage(uF_threshold):
#	print(str(O.iter)+" fS is running")
    erase_parts()
    if unbalancedForce() < uF_threshold:
        # print('Simulation almost finished. Writing output file. Simulation continues.')
        # writeResults()
        # if sampleIsStable(0.05):
    #		if sampleIsStable(0.05):
        print('Simulation finished. Writing output file. Simulation paused.')
        O.engines=O.engines+[PotentialParticleVTKRecorder(fileName='./roundRobin_Device_1_Results/PP_VTK_Recorder_', twoDimension=False, iterPeriod=1, nDo=1, sampleX=30, sampleY=30, sampleZ=30, maxDimension=Ds/5.)]
#			writeResults()
#			O.pause()
        pS.dead=False

def erase_parts():
    global n_erased
    for b in O.bodies:
        if (b.state.vel.norm() > 0.0) and (b.state.pos[2] < -Ds-t_10) and (len(b.state.blockedDOFs)==0): #FIXME: Do I need the velocity check?
            O.bodies.erase(b.id,True)
            n_erased += 1


def pauseSimulation():
	O.pause()


#####################################################################################################################
# Useful functions

def sampleIsStable(ufMax):
	global uf
	global KE
	try:
		uf=utils.unbalancedForce()
		KE=utils.kineticEnergy()
#		if uf<ufMax and KE<0.0001:  		 #FIXME: Old value, used in Simulations 1-5
		if (uf<ufMax and KE<1e-8) or (KE<1e-12): # This is the one I copied from Device 2
#			calm() # FIXME: I could assign calm() here, to help the sample stabilise
			return True
		else:
			return False
	except:
		print('Cannot calculate either uf or KE')
		return False

def avg_coord_nmbr():
    n_inter = O.interactions.countReal()
    n_part, n_rattlers = 0, 0
    for b in O.bodies:
        if len(O.interactions.withBody(b.id)) <= 1: n_rattlers += 1
        else: n_part += 1
    return 2 * (n_inter-n_rattlers) / n_part

#####################################################################################################################
# Potential particles' parameters

units=1000 #1000 for meters, 1 for milimeters

t_230=230/units #mm
t_190=190/units #mm
t_160=160/units #mm
t_120=120/units #mm
t_100=100/units #mm

t_20 = 20/units #mm
t_10 = 10/units #mm
t_5  =  5/units #mm

t_350=350/units #mm
t_290=290/units #mm

    ## Coordinates of tetrahedron, where the spheres of the clump are located (from Excel file)
Ds=10/units # [10mm=0.01m] Circumdiameter of clump
rs=Ds/(2*(1+np.sqrt(6)/4)) # radius of each sphere


aa=np.array([ 0.000000000000000,  0.8164965809277260,  0.0000000000000000, -0.8164965809277259])
bb=np.array([ 0.000000000000000,  0.4714045207910317, -0.9428090415820634,  0.4714045207910317]) #-0.9428090415820634 instead of -1.0000000000000000
cc=np.array([-1.000000000000000,  0.3333333333333334,  0.3333333333333334,  0.3333333333333334])
dd_part=np.array([ 0.632993161855500,  0.6329931618555000,  0.6329931618555000,  0.6329931618555000])/(units) #FIXME: Check this. Do I need to subtract r?

    ## This combination works
k=0.65
r=rs/1.
R=np.sqrt(2)*rs

Igeom = np.array((3.28600914143454e-12, 3.286145904978621e-12, 3.286197740532751e-12))
Volume = 0.0000003924806765


    ## Pyrunners lists
gen_pyrunners = [
    ('cS', 'utl.controlStage(uF_threshold)'),
    ('mH', 'utl.moveHorizontalPlate(bH)'),
    ('dUB', 'utl.deleteUpperBox(bbA, bbB, bbC, bbD, uF_threshold)')
]

gen_from_clp_pyrunners = [
    ('dUB', 'utl.deleteUpperBox(bbA, bbB, bbC, bbD, uF_threshold)')
]

sim_pyrunners = [
    ('mV', 'utl.moveVerticalPlate(bA, wall_velocity, damping, uF_threshold)'),
    ('fS', 'utl.finalStage(uF_threshold)'),
    ('pS', 'utl.pauseSimulation()')
]

    ## Other parameters
n_erased = 0
init_iter = O.iter # assigned when the module is loaded (for a generation init_iter = 0, for a simulation init_iter != 0)

#####################################################################################################################
# Postprocessing functions

## Get particle's shape and state parameters
pp_params = {}
for pp in O.bodies:
    ### Potential particles' parameters
    pp_param = {}
    pp_param['r'], pp_param['R'], pp_param['k'] = pp.shape.r, pp.shape.R, pp.shape.k
    pp_param['a'], pp_param['b'], pp_param['c'], pp_param['d'] = np.array(pp.shape.a), np.array(pp.shape.b), np.array(pp.shape.c), np.array(pp.shape.d)        

    ### Parameters to move a point according to the particle's displacement and rotation
    pp_param['pp_displacement'] = pp.state.displ()
    pp_param['rot_angle'] = np.linalg.norm(pp.state.rot())
    if pp_param['rot_angle'] != 0: pp_param['rot_axis'] = np.array([i/pp_param['rot_angle'] for i in pp.state.rot()]) # else the body is a wall (walls have not yet been erased when loading this module). It is not 100% certain though, if a particle didn't rotate at all, an error should be raised when trying to access pp_params[pp_id]['rot_angle']
    pp_param['ref_pos'] = pp.state.refPos

    ### Add parameters to dictionnary
    pp_params[pp.id] = pp_param

## Define inside / oustide function
def _f(point, pp_id):
    ### Load particle's parameters
    pp_param = pp_params[pp_id]
    r, R, k = pp_param['r'], pp_param['R'], pp_param['k']
    a, b, c, d = pp_param['a'], pp_param['b'], pp_param['c'], pp_param['d']
    pp_displacement, rot_angle, rot_axis, ref_pos = pp_param['pp_displacement'], pp_param['rot_angle'], pp_param['rot_axis'], pp_param['ref_pos']

    ### Transform the point so it corresponds to the initial particle
    point = point - pp_displacement
    point = point - ref_pos # rotation seems to be with respect to reference position
    point = np.cos(-rot_angle)*point + np.sin(-rot_angle)*np.cross(rot_axis, point) + (1-np.cos(-rot_angle))*(np.dot(rot_axis, point))*rot_axis # Rodrigues' rotation formula

    ### Compute f's value
    sum = 0
    for a_i, b_i, c_i, d_i in zip(a, b, c, d):
        sum += max(0, a_i*point[0] + b_i*point[1] + c_i*point[2] - d_i)**2
    
    return (1-k) * (sum/r**2 - 1) + k * (np.linalg.norm(point)**2/R**2 - 1)

## Define is_in_particle function
def is_in_particle(point, pp_id): return _f(point, pp_id) < 0

## Define sample's bouding box function (necesary because YADE's aabbExtrema doesn't work for potential particles)
def custom_aabbExtrema(): 
    mins, maxs = np.array([np.inf]*3), np.array([-np.inf]*3)
    for b in O.bodies: 
        if np.isnan(b.bound.min[0]): O.step() # Compute bounding boxes if simulation has just been loaded
        
        for i, mini in enumerate(mins):
            if b.bound.min[i] < mini: mins[i] = b.bound.min[i]

        for i, maxi in enumerate(maxs):
            if b.bound.max[i] > maxi: maxs[i] = b.bound.max[i]

    return mins, maxs

#####################################################################################################################
# Save / load functions to pass custom parameters from generation's to simulation's script

def save_walls(bodies):
    """
    This function saves bodies ids as a list of ints in a PyRunner set in its command. It will be used to save walls' ids.
    (This is not great, but I didn't found a better way to pass python variables through O.save / O.load)
    """
    b_ids = []
    for b in bodies:
        try: b_ids.append(b.id)
        except AttributeError: print("Some bodies were erased, they will be skipped")
    O.engines += [PyRunner(command="_".join(b_ids))]

def load_walls():
    ints_str = O.engines[-1].command
    O.engines = O.engines[:-1]
    walls = [O.bodies[int(i)] for i in ints_str.split("_")]
    return walls
