#!/bin/bash
yadenc-batch -j44 --job-threads 1 batch_A.table ppc.py > ppc_batch_A.log
yadenc-batch -j18 --job-threads 1 batch_B.table ppc.py > ppc_batch_B.log
yadenc-batch -j10 --job-threads 1 batch_C.table ppc.py > ppc_batch_C.log
yadenc-batch -j10 --job-threads 1 batch_D.table ppc.py > ppc_batch_D.log
yadenc-batch -j9 --job-threads 1 batch_E.table ppc.py > ppc_batch_E.log
yadenc-batch -j5 --job-threads 1 batch_F.table ppc.py > ppc_batch_F.log
yadenc-batch -j5 --job-threads 1 batch_G.table ppc.py > ppc_batch_G.log
yadenc-batch -j5 --job-threads 1 batch_H.table ppc.py > ppc_batch_H.log
yadenc-batch -j5 --job-threads 1 batch_I.table ppc.py > ppc_batch_I.log
yadenc-batch -j5 --job-threads 1 batch_J.table ppc.py > ppc_batch_J.log
yadenc-batch -j4 --job-threads 1 batch_K.table ppc.py > ppc_batch_K.log
yadenc-batch -j1 --job-threads 1 batch_L.table ppc.py > ppc_batch_L.log