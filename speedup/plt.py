import sys, os, re
sys.path.append(os.getcwd())

import numpy as np
import matplotlib as mpl, matplotlib.pyplot as plt
mpl.rcParams['font.size'] = 25 # Setting the size of the fonts in the graphs

# Plot parameters
model = "CLP"

# Useful function
file_name_translate_dic = {' ': '_', '\\': '', '$': '', 'overline': 'mean_', '{': '', '}': '', '^': '_', "'": ""}
def translate_str(text): return re.sub('({})'.format('|'.join(map(re.escape, file_name_translate_dic.keys()))), lambda m: file_name_translate_dic[m.group()], text)

# Creating save directory
save_dir = "graphs/"
if not os.path.isdir(save_dir) : os.mkdir(save_dir)

# Loading data
    ## Get file's lines
with open("ppc/{:}_model.csv".format(model), 'r') as fil: lines = fil.readlines()[1:]

    ## Reading data
seed_tet_S, n_cores_S, e_tet_S, t_tet_S = [], [], [], []
for line in lines:
    values = [float(i) for i in line.split("\t")]

    seed_tet_S.append(values[0])
    n_cores_S.append(values[1])
    e_tet_S.append(values[2])
    t_tet_S.append(values[3])

# Compute mean value and standard deviation over all samples
    ## List of number of cores used
n_cores_s = list(set(n_cores_S))
n_cores_s.sort()

    ## List of measurements
e_mean_s, e_std_s = [], []
t_mean_s, t_std_s = [], []

        ### Looping on all sub-volume measurements
for n_cores in n_cores_s:
    e_t, t_subv_t = [], []
    for seed_tet, n_cores_bis, e_tet, t_tet in zip(seed_tet_S, n_cores_S, e_tet_S, t_tet_S):
        if n_cores != n_cores_bis: continue
        e_t.append(e_tet)
        t_subv_t.append(t_tet)

    e_mean_s.append(np.mean(e_t))
    e_std_s.append(np.std(e_t))
    
    t_mean_s.append(np.mean(t_subv_t))
    t_std_s.append(np.std(t_subv_t))

# Compute speed-up
su_s, su_std_s = [], []
t_mean_ref, t_std_ref = t_mean_s[0], t_std_s[0]
for n_cores, t_mean, t_std in zip(n_cores_s, t_mean_s, t_std_s):
    su_s.append(t_mean_ref/t_mean)
    su_std_s.append(su_s[-1] * (t_std/t_mean + t_std_ref/t_mean_ref))

#####################################################################################################################
# Plot data
fig1 = plt.figure(1, figsize=(15,10))

title1 = r"Speed-up $S$ for the tetrahedron method"

ax = plt.gca()

    ## Sub-volume method data
ax.errorbar(n_cores_s, su_s, yerr=su_std_s, fmt='o', capsize=10)	

xlims = ax.get_xlim()

plt.plot(xlims, xlims, color='red', label="Perfect speed-up\n" + r"($S=N_{cores}$)")

# plt.title(title1)
plt.xlabel(r"Number of CPU cores $N_{cores}$")
plt.ylabel(r"$S$")
plt.legend(loc='best', fontsize=20) #bbox_to_anchor=(1.01,1), loc="upper left"

fig1.savefig(save_dir + translate_str(title1) + '.pdf', bbox_inches="tight", format='pdf')

########################################################################################
