import sys, os, glob
sys.path.append(os.getcwd() + "/..")

import numpy as np, warnings, time as tm
import utl_aor

# "PP" for Potential Particle
# "CLP" for Clump
model = "CLP" 

# Void ratio measure parameters
e_method = "TET" # "TET" # "SUBV" for subvolume method, "TET" for tetrahedron method, "REF" for bounding box method
C_subv = .8 # Homotethie's parameter for subvolume method
n_mc_points = 100 # Number of Monte Carlo points
n_cores = 3 # Number of cores to use when computing void ratio (only for tetrahedron method for now)

# Other parameters
seed = 0

# Read parameters from table if running in batch
readParamsFromTable(seed=int(seed), model=model, n_cores=n_cores)
from yade.params.table import *
np.random.seed(seed) # Fixing random seed

# Creating postprocessing directory if it doesn't already exist
save_dir = "ppc/"
if not os.path.isdir(save_dir) : os.mkdir(save_dir)

# Load sample
    ## Get sample's path
sample = glob.glob("../samples/{:}_2150_parts_{:d}.yade*.xml.bz2".format(model, seed))[0] # Beware if you have several samples with the same parameters but different yade version

    ## Load sample
O.load(sample)
print("Sample {:} loaded".format(sample))

# Import utility module for chosen the model
if model == "PP": import utl_pp as utl
elif model == "CLP": import utl_clp as utl

# Keep only particles
for b in O.bodies:
    if b.state.blockedDOFs=="xyzXYZ": O.bodies.erase(b.id)

# Compute void ratio
    ## Get ids of all particles
if model=="PP": part_ids = np.array([b.id for b in O.bodies])
elif model=="CLP": part_ids = np.array([b.id for b in O.bodies if type(b.shape)==Clump])

        ### Tetrahedron method
if e_method=="TET": 
    t0 = tm.time()
    e = utl_aor.compute_e_tet(part_ids, utl.is_in_particle, n_cores=n_cores, n_mc_points=200, random_num_generator=np.random.default_rng(seed)) # Takes a long time
    t_measure = tm.time() - t0

        ### Subvolume method
elif e_method=="SUBV": 
            #### Define subvolume
                ##### Heap's bounding box
    [Xmin, Ymin, Zmin], [Xmax, Ymax, Zmax] = utl.custom_aabbExtrema()

                ##### Compute subvolume's bounding box
    [xmin, ymin, zmin] = [(1-C_subv)*length + mininum for length, mininum in zip([Xmax-Xmin, Ymax-Ymin, Zmax-Zmin], [Xmin, Ymin, Zmin])]
    [xmax, ymax, zmax] = [C_subv*length + mininum for length, mininum in zip([Xmax-Xmin, Ymax-Ymin, Zmax-Zmin], [Xmin, Ymin, Zmin])]
            

                ##### Compute subvolume's volume
    V_subv = (xmax-xmin)*(ymax-ymin)*(zmax-zmin)

                ##### Define functions that returns True if a point is in the subvolume
    def is_in_subv(point):
        in_x = point[0] > xmin and point[0] < xmax
        in_y = point[1] > ymin and point[1] < xmax
        in_z = point[2] > zmin and point[2] < zmax
        return  in_x and in_y and in_z

    t0 = tm.time()
    e = utl_aor.compute_e_subv(part_ids, utl.is_in_particle, is_in_subv, V_subv, n_mc_points=n_mc_points, random_num_generator=np.random.default_rng(seed))
    t_measure = tm.time() - t0

elif e_method=="REF": 
            #### Heap's bounding box
    [Xmin, Ymin, Zmin], [Xmax, Ymax, Zmax] = utl.custom_aabbExtrema()
    V_tot = (Xmax-Xmin)*(Ymax-Ymin)*(Zmax-Zmin)

            #### Volume of 1 particle
    if model=="PP": vol1p = 3.924806765e-7
    elif model=="CLP": vol1p = 3.329921274267349e-7

            #### Compute void ratio
    t0 = tm.time()
    e = (V_tot-len(part_ids)*vol1p) / (len(part_ids)*vol1p)
    t_measure = tm.time() - t0

# Saving results
    ## Data to save
data = [seed, n_cores, e, t_measure]
str_data = []

    ## Opening results' csv file
csv_path = save_dir + "{:}_model.csv".format(model)

        ### If the file exists and is not empty, display a warning
if os.path.exists(csv_path) and os.path.getsize(csv_path)>0: 
    warnings.warn("Postprocessing csv file is not empty")
    header = ""
else: header = "\t".join([[ i for i, a in globals().items() if id(a) == id(x)][0] for x in data + str_data]) + "\n"

        ### Open file and add a header
csv_f = open(csv_path, "a") # Before a new run, one should make sure this file is empty
csv_f.write(header)

    ## Add line in csv file
data_line = "\t".join(["{:.8e}"]*len(data) + ["{:}"]*len(str_data)) + "\n"
csv_f.write(data_line.format(*data, *str_data))
csv_f.close()




