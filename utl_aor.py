import sys, os
sys.path.append(os.getcwd())

import numpy as np, time as tm, warnings, shutil
from yade import utils, pack, geom
from yade.utils import *
from yade.pack import *

from scipy.spatial import Delaunay
from multiprocessing import Pool

#####################################################################################################################
# Angle of repose

def get_outer_surface(points, direction, discr_len):
    """
    This function detects in a cloud of points the points with the highest position in a specific direction. It divides the cloud in portions which length is `discr_len`.
    
    `points` should be a numpy array of size (n_points, 3)
    `direction` should one of the followig : {'0', '1', '2'}
    `discr_len` should be a float
    """
    # Determine the smallest box that bounds the points
    mins, maxs = points.min(0), points.max(0)
    maxs += (maxs - mins) * 1e-2 # Increase slightly maxima (1%) so no points are ignored
    lengths = maxs - mins    

    # Determine the two directions to discretise (e.g. if the function is used to find the highest points, the two directions are the horizontal ones)
    discr_dim_s = [0, 1, 2]
    discr_dim_s.remove(direction)

    # Dicretise using a discr_len of at least the one set by the user (the size of the portions is rounded up so none should be empty)
    n_inter_s = [int(lengths[i]/discr_len) for i in discr_dim_s]
    nodes_s = [np.linspace(mins[i], maxs[i], n_inter_s[i]+1) for i in discr_dim_s]

    # Detect outer surface
    outer_surface = []

        ## Loop on portions, the two discretised dimensions are called x and y for simplicity (but really depend on user's input)
    nodes_x, nodes_y = nodes_s
    for xmin, xmax in zip(nodes_x[:-1], nodes_x[1:]):
        for ymin, ymax in zip(nodes_y[:-1], nodes_y[1:]):

            ### Initalize variable holding maximum point
            maximum = np.array([-np.inf]*3)

            ### Loop on all points
            for p in points:
                x, y = [p[i] for i in discr_dim_s]
                z = p[direction] # for simplicity, direction of interest is called z

                #### If the point is not in the current portion, ignore it
                in_x = x >= xmin and x < xmax # This is why maxima had to be increased
                in_y = y >= ymin and y < ymax
                if not (in_x and in_y): continue

                #### Check if the point has a bigger position in the direction of interest
                if z > maximum[direction]: maximum = p
            
            ### Add the 'maximum' point in the current portion to the outer surface list, if any particle was detected
            if maximum[0]>-np.inf: outer_surface.append(maximum)
    
    return np.array(outer_surface)


def compute_aor(centers, x_min, x_max, dx):
    # Get outer surface of the heap (using the heights as direction)
    outer_surface = get_outer_surface(centers, 2, dx)

    # Project the outer surface in the 'depth' direction (simply drop the y component)
    proj_surf = outer_surface[:, ::2] # this slice simply jumps from column 0 to 2

    # Sort points according to the x direction
    proj_surf = proj_surf[np.argsort(proj_surf[:,0]),:]

    # Keep only points betwean x_min and x_max
    x_values = [p[0] for p in proj_surf if p[0]>x_min and p[0]<x_max]
    y_values = [p[1] for p in proj_surf if p[0]>x_min and p[0]<x_max] 

    # Perform a linear regression 
    lin_regr = np.polyfit(x_values, y_values, 1)

    # Compute angle of repose
    aor = np.arctan(lin_regr[0])*180/np.pi # degrees

    return aor

#####################################################################################################################
# Stress tensor

def sigma_heap(bodies_id, walls_id, volume="auto", gravity=[0,0,-9.81]):
    if volume=="auto": volume = _compute_heap_vol(bodies_id)

    ## Interaction contribution
    sigma_a = np.zeros((3,3))
    for inter in O.interactions:
        if inter.id1 in walls_id or inter.id2 in walls_id: # If one of the the bodies is a wall
            if inter.id1 in walls_id and inter.id2 not in walls_id: sgn = 1 # If only body 1 is a wall
            elif inter.id2 in walls_id and inter.id1 not in walls_id: sgn = -1 # If only body 2 is a wall
            
            f, x = sgn*(inter.phys.normalForce + inter.phys.shearForce), inter.geom.contactPoint
            for i, fi in enumerate(f):
                for j, xj in enumerate(x): sigma_a[i,j] += fi*xj/volume
            
    ## Gravity contribution
    sigma_b = np.zeros((3,3))
    for b_id in bodies_id:
        b = O.bodies[int(b_id)]
        m, x = b.state.mass, b.state.pos
        for i, gi in enumerate(gravity):
            for j, xj in enumerate(x): sigma_b[i,j] += m*gi*xj/volume

    return sigma_a + sigma_b

def _compute_heap_vol(bodies_id):
    ## Triangulate the sample
        ### Get centers of all particles
    centers = np.array([O.bodies[int(b_id)].state.pos for b_id in bodies_id])

        ### Triangulate using scipy's Delaunay triangulation 
    tri = Delaunay(centers)

    V_tot = 0
    for i_tet in range(tri.simplices.shape[0]):
        vertices = np.vstack([centers[i,:] for i in tri.simplices[i_tet,:]])
        V_tot += 1/6.*abs(np.linalg.det(np.vstack([vertices[i,:]-vertices[3,:] for i in range(3)])))
    
    return V_tot


#####################################################################################################################
# Void ratio

    ## Tetrahedron method
"""
This piece of code could be made into an object, but the computation speed when using parallelisation with the multiprocessing module would decrease a lot. What would be attributes (n_mc_pts, bds_id, is_in_part, centers) are defined inside the whole module, what would be methods (compute_e_tet, _process_tet, _draw_pts_in_tet, _bbox_overlap) are functions in the module.
"""

        ### Initialise parameters used by all functions (not necessary but more clear IMO) 
n_mc_pts = 0
bds_id = []
is_in_part = lambda : None
centers = np.array([])
rng = np.random.default_rng()
# tri = Delaunay(...) # Scipy's triangulation object

        ### Functions

def compute_e_tet(bodies_id, is_in_particle, n_cores=1, n_mc_points=1000, return_local_values=False, random_num_generator=np.random.default_rng()):
    """
    Compute the void ratio of a sample using the tetrahedron method described § 3.3.1 of Duverger et al. (2024) Granular Matter, 26:20.

    `bodies_id`: list of ints, the ids of all particles in the sample
    `is_in_particle`: function, it takes as parameters a point and a body id, in that order. It should return True if the point is inside the body whose id is `body_id`, False otherwise. Its signature should be like `is_in_particle(point, body_id)`. When this function is used, `point` will be a numpy array with shape (1, 3) and body_id will be an int.
    `n_cores`: int, number of cores to use for a parallel execution (where each tetrahedron can be processed independently)
    `n_mc_points`: int, number of points to draw when using the Monte Carlo method, for particle's volume computation 
    """
    #### Set some variables as global so they can be used by other functions
    global n_mc_pts, bds_id, is_in_part, tri, centers, rng
    n_mc_pts = n_mc_points
    bds_id = bodies_id
    is_in_part = is_in_particle
    rng = random_num_generator

    #### If simulation has just been loaded, bounding boxes are nan. Make a step to compute them (sample shouldn't change much and even be more stable)
    try: 
           if np.isnan(O.bodies[int(bodies_id[0])].bound.min[0]): O.step() 
    except AttributeError: pass # If clump (Clump bodies have no bound attribute)

    #### Triangulate the sample
        ##### Get centers of all particles
    centers = np.array([O.bodies[int(b_id)].state.pos for b_id in bodies_id])

        ##### Triangulate using scipy's Delaunay triangulation 
    tri = Delaunay(centers)

    #### Postprocess each tetrahedron using specified number of cores
    p = Pool(processes=n_cores)
    results = p.map(_process_tet, [i for i in range(tri.simplices.shape[0])])
    p.close()

    #### If user wishes to have local values, return void ratio and center of mass
    if return_local_values:
        e_s, tet_centers = [], []
        for V_tet, V_part, centroid in results:
            e_s.append((V_tet-V_part)/V_part)
            tet_centers.append(centroid)
        return e_s, tet_centers

    #### Compute total volume and particles volume for all the sample
    V_tot_tet, V_tot_part = 0, 0
    for V_tet, V_part, _ in results:
        V_tot_tet += V_tet
        V_tot_part += V_part
    
    #### Return void ratio
    return (V_tot_tet-V_tot_part)/V_tot_part


def _process_tet(i_tet):
    """
    This function applies the Monte Carlo method inside a tetrahedron to compute the proportion of void.
    Returns a tuple of 2 floats : the total volume of the tetrahedron and the volume of particle inside the tetrahedron, in that order.
    """

    #### Get tetrahedron's vertices
    vertices = np.vstack([centers[i,:] for i in tri.simplices[i_tet,:]])

    #### Determine the smallest axis aligned box that bounds the tetrahedron
    xmin, ymin, zmin = vertices.min(axis=0)
    xmax, ymax, zmax = vertices.max(axis=0)
    tet_bbox = ((xmin, ymin, zmin), (xmax, ymax, zmax))

    #### Get the ids of all particles to be checked with Monte Carlo method
    part2check_ids = []
    for b_id in bds_id:

        ##### Get particle's bounding box
        part_bbox = _get_part_bbox(b_id)

        ##### If the bounding boxes overlaps, add particle to the "to check" list
        if _bbox_overlap(tet_bbox, part_bbox): part2check_ids.append(int(b_id))

    #### Uniformly draw points inside the tetrahedron (for Monte Carlo method)
    mc_points = _draw_pts_in_tet(n_mc_pts, vertices)

    #### Begin Monte Carlo method
    n_in = 0
    for mc_point in mc_points:

        ##### Loop on particles that can be at least partly in the tetrahedron
        for b_id in part2check_ids:
            if is_in_part(mc_point, b_id): 
                n_in += 1 
                break # No need to check remaining particles
    
    #### Compute tetrahedron's volume
    V_tet = 1/6.*abs(np.linalg.det(np.vstack([vertices[i,:]-vertices[3,:] for i in range(3)])))

    #### Compute particle's volume
    V_part = n_in/n_mc_pts * V_tet
    
    return V_tet, V_part, vertices.mean(0)


def _bbox_overlap(bbox1, bbox2):
    #### Make sure bounding boxes are numpy arrays
    bbox1, bbox2 = np.array(bbox1), np.array(bbox2)

    #### Get bounding boxes centers
    c1, c2 = (bbox1[1]+bbox1[0])/2, (bbox2[1]+bbox2[0])/2

    #### Get bounding boxes lengths
    l1, l2 = bbox1[1]-bbox1[0], bbox2[1]-bbox2[0]

    #### Compare distance between centers to lengths
    overlaps = abs(c1-c2)*2 < l1 + l2

    return overlaps.all()

def _draw_pts_in_tet(n, vertices):
    """
    Uniformly draw `n` points inside the tetrahedron whose vertices are `vertices`. 

    For more details, see https://doi.org/10.1080/10867651.2000.10487528

    `n`: int
    `vertices`: numpy array of shape (4, 3)

    Returns a numpy array of shape (n, 3)
    """
    xs, ys, zs = [], [], []
    for s, t, u in rng.random((n, 3)):
        if s+t<=1: sp, tp, up = s, t, u
        else: sp, tp, up = 1-s, 1-t, u
        if sp+tp+up <= 1: sf, tf, uf = np.array([sp, tp, up])
        else:
            if tp+up>1: sf, tf, uf = np.array([sp, 1-up, 1-sp-tp])
            else: sf, tf, uf = np.array([1-tp-u, tp, sp+tp+up-1])
        a = 1 - sf - tf - uf 
        bar_coord = [a, sf, tf, uf]
        point = [bar_coord @ vertices[:,i] for i in range(3)] # The @ (dot) operator requires at least python3.5
        xs.append(point[0])
        ys.append(point[1])
        zs.append(point[2])
    return np.array([xs, ys, zs]).T

def _get_part_bbox(b_id):
    #### If simulation has just been loaded, bounding boxes are nan. Make a step to compute them (sample shouldn't change much and even be more stable)
    try: 
        if np.isnan(O.bodies[int(b_id)].bound.min[0]): O.step() 
    except AttributeError: pass # If clump (Clump bodies have no bound attribute)

    if type(O.bodies[int(b_id)].shape)==Clump: 
        _, clp_bbox = _get_clp_id_bb(list(O.bodies[int(b_id)].shape.members.keys())[0])
        return clp_bbox
        
    else: return np.array([O.bodies[int(b_id)].bound.min, O.bodies[int(b_id)].bound.max])

def _get_clp_id_bb(sph_id):
    """
    Returns the clump id of the sphere whose id is `sph_id`, and the clump's bounding box. For the tetrahedron method, only the second value is used
    """
    clp_id = O.bodies[int(sph_id)].clumpId
    sph_ids = [id for id in O.bodies[clp_id].shape.members.keys()]

    #### If simulation has just been loaded, bounding boxes are nan. Make a step to compute them (sample shouldn't change much and even be more stable)
    try: 
        if np.isnan(O.bodies[int(sph_id)].bound.min[0]): O.step() 
    except AttributeError: pass # If clump (Clump bodies have no bound attribute)

    bb_mins = np.array([O.bodies[int(id)].bound.min for id in sph_ids]).min(0)
    bb_maxs = np.array([O.bodies[int(id)].bound.max for id in sph_ids]).max(0)

    return clp_id, np.array((bb_mins, bb_maxs))

    ## Subvolume method

def compute_e_subv(bodies_id, is_in_particle, is_in_subv, subv_volume, n_mc_points=1000, random_num_generator=np.random.default_rng()):
    """
    Compute the void ratio of a sample using the sub-volume method described § 3.3.2 of Duverger et al. (2024) Granular Matter, 26:20.
    """
    ### Get the ids of all particles to be checked with Monte Carlo method
    cut_part_ids, in_part_ids = [], []
    for b_id in bodies_id: # NOTE: Can be parallelised with the multiprocessing module
        #### If simulation has just been loaded, bounding boxes are nan. Make a step to compute them (sample shouldn't change much and even be more stable)
        try: 
           if np.isnan(O.bodies[int(b_id)].bound.min[0]): O.step() 
        except AttributeError: pass # If clump (Clump bodies have no bound attribute)

        #### Get particle's bounding box
        part_bbox = _get_part_bbox(b_id)

        #### Put bounding box's vertices into an array (there must be better way to do this)
        vertices = np.array([ # shape (8, 3)
            [part_bbox[0,0], part_bbox[0,1], part_bbox[0,2]],
            [part_bbox[0,0], part_bbox[0,1], part_bbox[1,2]],
            [part_bbox[0,0], part_bbox[1,1], part_bbox[1,2]],
            [part_bbox[1,0], part_bbox[1,1], part_bbox[1,2]],
            [part_bbox[0,0], part_bbox[1,1], part_bbox[0,2]],
            [part_bbox[1,0], part_bbox[1,1], part_bbox[0,2]],
            [part_bbox[1,0], part_bbox[0,1], part_bbox[0,2]],
            [part_bbox[1,0], part_bbox[0,1], part_bbox[1,2]]
        ])

        #### Check how many vertices if the bounding box are in the subvolume
        nv_in_subv = sum(map(is_in_subv, vertices))

        #### Locate the particle with respect to the subvolume
        if nv_in_subv == 0: continue # Particle is completely outside the subvolume
        elif nv_in_subv == 8: in_part_ids.append(int(b_id)) # Particle is completely inside the subvolume
        else: cut_part_ids.append((int(b_id), part_bbox))  # Particle's bounding box is partly in the subvolume
        
    ### Compute the volume of particles inside the subvolume
    ### NOTE: Could have been merged with the last "for b_id in bds_id" loop, it was split because it is more clear IMO (and computation is not that long anyway)
    V_part = 0

        #### Particles completely inside the subvolume
    for b_id in in_part_ids: 
            ##### Get particle's density
        if type(O.bodies[b_id].shape) == Clump: density = O.bodies[[i for i in O.bodies[b_id].shape.members.keys()][0]].material.density
        else: density = O.bodies[b_id].material.density
        V_part += O.bodies[b_id].state.mass / density

        #### Particles cut by the faces of the subvolume
    for b_id, part_bbox in cut_part_ids: 

            ##### Uniformly draw points inside the particle's bounding box
        mc_points = random_num_generator.random((n_mc_points, 3)) * (part_bbox[1]-part_bbox[0]) + part_bbox[0]

            ##### Monte Carlo method to determine the proportion of the particle's volume inside the subvolume
        n_in = 0
        for mc_point in mc_points:
            if is_in_subv(mc_point) and is_in_particle(mc_point, b_id): n_in += 1
        
            ##### Compute the volume of the bounding box
        V_bbox = np.prod(part_bbox[1]-part_bbox[0])

            ##### Compute the particle's volume inside the subvolume
        V_part += V_bbox * n_in / n_mc_points
        
        #### Return void ratio
    return (subv_volume-V_part)/V_part






#####################################################################################################################
# Useful functions

    ## Terminal plot functions
        ### Try to import module to plot on terminal (can be installed with `python3 -m pip install plotext`)
try: import plotext as plt
except ModuleNotFoundError: warnings.warn("Module plotext not found, ploting functions won't be available. Consider installing it with `python3 -m pip install plotext`.")

        ### Get terminal size to set aspect ratio
width, height = shutil.get_terminal_size()
w_max, h_max = .1, .14
width = w_max * height/h_max

def plot_outer_surface(centers, dx, set_ar=False):
    """
    Plot the outer surface in the terminal. Very useful when running YADE remotely.
    """
    outer_surface = get_outer_surface(centers, 2, dx)

    if set_ar: plt.plot_size(width, height)
    plt.ylim(0, h_max) 
    plt.xlim(0, .1)

    plt.scatter(outer_surface[:,0], outer_surface[:,2])

    plt.title("Outer Surface")
    plt.show()

def live_plot_outer_surface(model, refresh=1e-2, set_ar=False):
    """
    Live-plot the outer surface. This functions clears the terminal. Use ^C to stop its execution.
    """
    dx = 1e-2
    plt.clc()
    while True:  
        if model=="PP": centers = np.array([b.state.pos for b in O.bodies])
        elif model=="CLP": centers = np.array([b.state.pos for b in O.bodies if type(b.shape)==Clump])
        plt.clt() 
        plt.cld()  
        plot_outer_surface(centers, dx, set_ar=set_ar) 
        tm.sleep(refresh)

def is_in_sphere(point, bid): 
    d = np.linalg.norm(point - np.array(O.bodies[bid].state.pos)) 
    return d < O.bodies[bid].shape.radius
