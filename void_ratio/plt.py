import sys, os, re
sys.path.append(os.getcwd())

import numpy as np
import matplotlib as mpl, matplotlib.pyplot as plt
mpl.rcParams['font.size'] = 25 # Setting the size of the fonts in the graphs

# Plot parameters
model = "PP"

# Useful function
file_name_translate_dic = {' ': '_', '\\': '', '$': '', 'overline': 'mean_', '{': '', '}': '', '^': '_', "'": ""}
def translate_str(text): return re.sub('({})'.format('|'.join(map(re.escape, file_name_translate_dic.keys()))), lambda m: file_name_translate_dic[m.group()], text)

# Creating save directory
save_dir = "graphs/"
if not os.path.isdir(save_dir) : os.mkdir(save_dir)

# Loading data
    ## Get file's lines
with open("ppc/{:}_model.csv".format(model), 'r') as fil: lines = fil.readlines()[1:]

    ## Reading data
seed_subv_S, C_subv_S, nmc_S, e_subv_S, t_subv_S = [], [], [], [], []
seed_tet_S, e_tet_S, t_tet_S = [], [], []
seed_ref_S, e_ref_S, t_ref_S = [], [], []
for line in lines:
    method = line.split("\t")[-1][:-1]
    values = [float(i) for i in line.split("\t")[:-1]]

    if method=="SUBV": 
        seed_subv_S.append(values[0])
        C_subv_S.append(values[1])
        nmc_S.append(values[2])
        e_subv_S.append(values[3])
        t_subv_S.append(values[4])
    
    elif method=="TET":
        seed_tet_S.append(values[0])
        e_tet_S.append(values[3])
        t_tet_S.append(values[4])

    elif method=="REF":
        seed_ref_S.append(values[0])
        e_ref_S.append(values[3])
        t_ref_S.append(values[4])

# Compute mean value and standard deviation over all samples
    ## For the sub-volume method
        ### Lists of method's parameters
nmc_s = list(set(nmc_S))
C_subv_s = list(set(C_subv_S))
nmc_s.sort()
C_subv_s.sort()

        ### Lists of lists of measurements (one dimension per method's parameter)
e_subv_mean_ss, e_subv_std_ss = [], []
t_subv_mean_ss, t_subv_std_ss = [], []

        ### Looping on all sub-volume measurements
for nmc in nmc_s:
    e_subv_ss, t_subv_ss = [[] for i in C_subv_s], [[] for i in C_subv_s]
    for C_subv, nmc_bis, e_subv, t_subv in zip(C_subv_S, nmc_S, e_subv_S, t_subv_S):
        if nmc != nmc_bis: continue
        
        ind_C = C_subv_s.index(C_subv)
        e_subv_ss[ind_C].append(e_subv)
        t_subv_ss[ind_C].append(t_subv)
    

    e_subv_mean_ss.append([np.mean(e_subv) for e_subv in e_subv_ss])
    e_subv_std_ss.append([np.std(e_subv) for e_subv in e_subv_ss])
    
    t_subv_mean_ss.append([np.mean(t_subv) for t_subv in t_subv_ss])
    t_subv_std_ss.append([np.std(t_subv) for t_subv in t_subv_ss])


    ## For the tetrahedron method
e_tet_mean, e_tet_std = np.mean(e_tet_S), np.std(e_tet_S)
t_tet_mean, t_tet_std = np.mean(t_tet_S), np.std(t_tet_S)

    ## For the bounding box method
e_ref_mean, e_ref_std = np.mean(e_ref_S), np.std(e_ref_S)
t_ref_mean, t_ref_std = np.mean(t_ref_S), np.std(t_ref_S)

#####################################################################################################################
# Plot data
fig1 = plt.figure(1, figsize=(15,10))

title1 = r"Void ratio $e$ against homothety's parameter $C$"

ax = plt.gca()

    ## Sub-volume method data
for nmc, e_subv_mean_s, e_subv_std_s in zip(nmc_s, e_subv_mean_ss, e_subv_std_ss):
	ax.errorbar(C_subv_s, e_subv_mean_s, yerr=e_subv_std_s, fmt='o', capsize=10, label=r'$e^{{SUB}}$ for $N_{{mc}}={:d}$'.format(int(nmc)))	

xlims = ax.get_xlim()

    ## Tetrahedron method data
plt.plot(xlims, [e_tet_mean]*2, color='mediumorchid', label=r'$e^{TET}$')
plt.plot(xlims, [e_tet_mean-e_tet_std]*2, color='mediumorchid', linestyle=':')
plt.plot(xlims, [e_tet_mean+e_tet_std]*2, color='mediumorchid', linestyle=':')

    ## Bounding box method data
plt.plot(xlims, [e_ref_mean]*2, color='red', label=r'$e^{REF}$')
plt.plot(xlims, [e_ref_mean-e_ref_std]*2, color='red', linestyle=':')
plt.plot(xlims, [e_ref_mean+e_ref_std]*2, color='red', linestyle=':')

# plt.title(title1)
plt.xlabel(r"$C$")
plt.ylabel(r"$e$")
plt.legend(loc='best', fontsize=20) #bbox_to_anchor=(1.01,1), loc="upper left"

fig1.savefig(save_dir + translate_str(title1) + '.pdf', bbox_inches="tight", format='pdf')

########################################################################################
fig2, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True, figsize=(15,10))

title2 = r"Time cost $T_c$ against homothety's parameter $C$"

fig2.subplots_adjust(hspace=0.05)

    ## Sub-volume method data
for nmc, t_subv_mean_s, t_subv_std_s in zip(nmc_s, t_subv_mean_ss, t_subv_std_ss):
	ax1.errorbar(C_subv_s, t_subv_mean_s, yerr=t_subv_std_s, fmt='o', capsize=10)	
	ax2.errorbar(C_subv_s, t_subv_mean_s, yerr=t_subv_std_s, fmt='o', capsize=10)	
	ax3.errorbar(C_subv_s, t_subv_mean_s, yerr=t_subv_std_s, fmt='o', capsize=10, label=r'$t^{{SUB}}$ for $N_{{mc}}={:d}$'.format(int(nmc)))

    ## Set broken axes limits manually
ax3.set_ylim(4e-6, 4e-5)
if model=="CLP": 
    ax2.set_ylim(5e-2, 250)
    ax1.set_ylim(5.2e3, 7e3) # The function that check if a point is in a particle takes longer with CLP model (it loops through clump members)
elif model=="PP": 
    ax2.set_ylim(5e-2, 150)
    ax1.set_ylim(2.1e3, 3.3e3)  


    ## Tweak ax3 labels
ax3.locator_params('y', tight=True, nbins=5)
ticks_ax3 = ax3.get_yticks()
ax3.set_yticklabels(['{:.1f}'.format(x*1e5) + r'$\times 10^{-5}$' for x in ticks_ax3])

    ## Tweak ax2 labels
ax2.locator_params('y', tight=True, nbins=5)

    ## Tweak ax1 labels
ax1.locator_params('y', tight=True, nbins=5)
ticks_ax1 = ax1.get_yticks()
ax1.set_yticklabels(['{:.1f}'.format(x*1e-3) + r'$\times 10^3$' for x in ticks_ax1])

    ## Hide the spines between ax1 and ax2
ax1.spines['bottom'].set_visible(False)
ax2.spines['bottom'].set_visible(False)
ax2.spines['top'].set_visible(False)
ax3.spines['top'].set_visible(False)
ax1.xaxis.tick_top()
#ax2.xaxis.tick_top()
ax2.axes.xaxis.set_visible(False)
ax1.tick_params(labeltop=False)  # don't put tick labels at the top
#ax2.tick_params(labeltop=False)
ax3.xaxis.tick_bottom()

    ## Slanted lines
d = .5  # proportion of vertical to horizontal extent of the slanted line
kwargs = dict(marker=[(-1, -d), (1, d)], markersize=12,
              linestyle="none", color='k', mec='red', mew=3, clip_on=False)
ax1.plot([0, 1], [0, 0], transform=ax1.transAxes, **kwargs)
ax2.plot([0, 1], [1, 1], transform=ax2.transAxes, **kwargs)
ax2.plot([0, 1], [0, 0], transform=ax2.transAxes, **kwargs)
ax3.plot([0, 1], [1, 1], transform=ax3.transAxes, **kwargs)

xlim = ax1.get_xlim()

    ## Tetrahedron method data
plt.plot(xlims, [t_tet_mean]*2, color='mediumorchid', label=r'$t^{TET}$')
ax1.plot(xlims, [t_tet_mean]*2, color='mediumorchid')
ax1.plot(xlims, [t_tet_mean-t_tet_std]*2, color='mediumorchid', linestyle=':')
ax1.plot(xlims, [t_tet_mean+t_tet_std]*2, color='mediumorchid', linestyle=':')

    ## Bounding box method data
plt.plot(xlims, [t_ref_mean]*2, color='red', label=r'$t^{REF}$')
ax3.plot(xlims, [t_ref_mean]*2, color='red')
ax3.plot(xlims, [t_ref_mean-t_ref_std]*2, color='red', linestyle=':')
ax3.plot(xlims, [t_ref_mean+t_ref_std]*2, color='red', linestyle=':')

#plt.title(title2)
# fig2.suptitle(title2)
plt.xlabel(r"$C$")
#for ax in [ax1, ax2, ax3]: ax.set_ylabel(r"$t$ $(s)$")
ax2.set_ylabel(r"$t$ $(s)$", labelpad=30)
plt.legend(bbox_to_anchor=(.1,1.3), loc="lower left", fontsize=23)

fig2.savefig(save_dir + translate_str(title2) + '.pdf', bbox_inches="tight", format='pdf')


########################################################################################
# fig3 = plt.figure(3, figsize=(15,10))

# title3 = r"Time cost $T_c$ against homothety's parameter $C$ (logscale)"

# fig2.subplots_adjust(hspace=0.05)

# #ax = brokenaxes(ylims=((0, 2e-2), (1e-1, 70))) # plt.gca() # 
# ax = plt.gca()

# for nmc, t_c, tstd_c in zip(val_nmc, t_c_nmc, tstd_c_nmc):
# 	plt.errorbar(val_C, t_c, yerr=tstd_c, fmt='o', capsize=10)	
# 	plt.errorbar(val_C, t_c, yerr=tstd_c, fmt='o', capsize=10)	
# 	plt.errorbar(val_C, t_c, yerr=tstd_c, fmt='o', capsize=10, label=r'$t_{{SUB}}$ for $N_{{mc}}={:d}$'.format(int(nmc)))

# xlim = ax.get_xlim()

# plt.plot(xlim, [t_aabb]*2, color='red', label=r'$t_{all}$')
# plt.plot(xlim, [t_aabb-tstd_aabb]*2, color='red', linestyle=':', label=r'$\mbox{StD}(t_{all})$')
# plt.plot(xlim, [t_aabb+tstd_aabb]*2, color='red', linestyle=':')

# ax1.plot(xlims, [t_tet]*2, color='mediumorchid', label=r'$t^{TET}$')
# ax1.plot(xlims, [t_tet-tstd_tet]*2, color='mediumorchid', linestyle=':', label=r'$\mbox{StD}(t^{TET})$')
# ax1.plot(xlims, [t_tet+tstd_tet]*2, color='mediumorchid', linestyle=':')

# plt.plot(xlims, [t_tet]*2, color='mediumorchid', label=r'$t^{TET}$')
# plt.plot(xlims, [t_tet-tstd_tet]*2, color='mediumorchid', linestyle=':', label=r'$\mbox{StD}(t^{TET})$')

# #plt.title(title2)
# # plt.title(title3)
# plt.xlabel(r"$C$")
# #for ax in [ax1, ax2, ax3]: ax.set_ylabel(r"$t$ $(s)$")
# plt.ylabel(r"$t$ $(s)$")
# plt.legend(bbox_to_anchor=(1.01, 1), loc="upper left")
# plt.yscale('log')

# fig3.savefig(graph_dir + title3.replace(" ", "_").replace("/", "_over_") + '.pdf', bbox_inches="tight", format='pdf')
