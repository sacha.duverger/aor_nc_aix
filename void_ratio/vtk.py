import sys, os, glob
sys.path.append(os.getcwd() + "/..")

import numpy as np
import utl_aor, utl_clp as utl

model = "CLP"
seed = 0
n_cores = 90

sample = glob.glob("../final_states/final_state_{:}_2150_parts_{:d}.yade*.xml.bz2".format(model, seed))[0] # Beware if you have several samples with the same parameters but different yade version
sample = os.path.realpath(sample).split("final_states/")[0] + os.path.realpath(sample).split("final_states/")[1] 

O.load(sample)
print("Sample {:} loaded".format(sample))

part_ids = np.array([b.id for b in O.bodies if type(b.shape)==Clump])

e_s, centers = utl_aor.compute_e_tet(part_ids, utl.is_in_particle, n_cores=n_cores, n_mc_points=100, return_local_values=True, random_num_generator=np.random.default_rng(seed)) # Takes a long time

with open("ppc/local_void_ratio.csv", 'w') as fil:
    fil.write("x coord, y coord, z coord, e\n")
    for e, center in zip(e_s, centers):
        fil.write(", ".join(["{:.5e}"]*4).format(*center, e) + "\n")
