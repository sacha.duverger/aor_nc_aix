This reposistory contains YADE scripts to simulate heaps of particles and measure their angle of repose (AOR), as described by [Duverger et al. (2024) Granular Matter, 26:20](https://doi.org/10.1007/s10035-023-01378-z) (see also an alternate fulltext access [here](https://hal.inrae.fr/hal-04222127) or [there](https://rdcu.be/dwthi)). Auxiliary functions for computing the void ratio of an arbitrary set of particles, as described § 3.3 of the previous publication, can in particular be found as `compute_e_tet` and `compute_e_subv` in `utl_aor.py`.

# Workflow

For running an actual AOR simulation, the user should use only the 4 scripts:
 1. `gen.py`, to **generate** a sample
 2. `sim.py`, to perform the **simulation**
 3. `ppc.py`, to **postprocess** the results
 4. `plt.py`, to **plot** the results

In each of these scripts, user has to specify the model (`"PP"` for potential particles or `"CLP"` for clumps). 

An utility module is imported depending on the selected model (`utl_pp.py` or `utl_clp.py`), these scripts contains all that is related to the particle model.
A measure module (`utl_aor.py`) is imported in `ppc.py`, it contains all that is related to the measure of the AOR.
Another module (`dataRecorder.py`) is imported in `sim.py` to record data in a csv file during the whole simulation.

## Sample's generation

For this step, user has to set the:

 - normal stiffness $K_n$
 - shear stiffness $K_t$
 - Poisson's ratio $\nu$
 - restitution coefficient $e_n$ (or the viscous damping coefficient for the PP model)
 - friction between two particles $\varphi_{p2p}$
 - friction between a particle and a wall $\varphi_{p2w}$
 - density of the material $\rho$
 - Cundall's damping $D_{gen}$, for generation only (it is reseted in `sim.py`)
 - stability threshold for the unbalanced force $U_f^{max}$
 - gravity $g$

After the generation is over, a YADE simulation file containing the generated sample will be saved in the model's sample directory (`samples_PP/` or `samples_CLP/`).

For instance, a batch of generation can be launched with:

```
nohup yadenc-batch -j30 --job-threads 1 batch.table gen.py >> gen_batch.log &
```

After the computation is over, many log files are now in the working directory. One can store them in the `logs/` directory:

```
mv *.log logs
```

## Simulation

For this step, user has to set:
 - Cundall's damping $D_{sim}$, for simulation only 
 - the wall's velocity $V_{wall}$
  
Lauching a simulation will create a directory in the model's simulation folder (`sims_PP/` or `sims_CLP/`) where data during the whole simulation is saved. The final state of the sample is saved in this directory as a YADE simulation file. It is also symlinked to be accesible from the `final_states/` directory.

For instance, a batch of simulations can be launched with:

```
nohup yadenc-batch -j30 --job-threads 1 batch.table sim.py >> sim_batch.log &
```

And after the simulations are over:

```
mv *.log logs
```

## Postprocessing

For this step, user has to set:
 - the minimum horizontal position where particles are considered in the linear regression $x_{min}$
 - the maximum horizontal position where particles are considered in the linear regression $x_{max}$
 - the discretisation step used to split the heap in portions, $\Delta x$ 

After postprocessing, a file containing the AOR measure is created in `ppc/`. If another postprocessing is performed when this file already exists, it will not be overwritten. Be carefull not to add the same measure several times.
Outer surfaces are also saved in `ppc/`

For instance, a batch of postprocessing can be launched with:

```
nohup yadenc-batch -j30 --job-threads 1 batch.table ppc.py >> ppc_batch.log &
```

And after the postprocessings are over:

```
mv *.log logs
```

## Plotting results

For this step, the user has to select the number of the simulation that will be fully plot. To run this script, Python is enough (YADE is not necessary since simulations already have been postprocessed into csv files).

Postprocess the results with:

```
python3 plt.py
```

This will create a directory `graphs/` containing a figure that present the results of the AOR for all samples `graphs/Angle_of_repose_alpha_for_different_samples.pdf`, and directories containing the graphs for some specific simulations.
All figures compare the PP model with the CLP model. 