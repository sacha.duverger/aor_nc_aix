import sys, os, re, glob
from matplotlib import markers
sys.path.append(os.getcwd())

import numpy as np
import matplotlib as mpl, matplotlib.pyplot as plt
from matplotlib.lines import Line2D
mpl.rcParams['font.size'] = 25 # Setting the size of the fonts in the graphs

# Plot parameters
plot_sim = -30 # Seed of the sample that will be fully plot, <0 for a plot with all simulations (as mean +/- std), with the absolute value being the number of simulations
model_s = ["PP", "CLP"]

    ## Cosmetic parameters
color_s = ["green", "blue"]
label_s = ["Potential Particles", "Clumps"]
n_part = 2150

    ## Useful function
file_name_translate_dic = {' ': '_', '\\': '', '$': '', 'overline': 'mean_', '{': '', '}': '', '^': '_'}
def translate_str(text): return re.sub('({})'.format('|'.join(map(re.escape, file_name_translate_dic.keys()))), lambda m: file_name_translate_dic[m.group()], text)

# Creating save directory
    ## For graphs that plot all samples
save_dir = "graphs/"
if not os.path.isdir(save_dir) : os.mkdir(save_dir)
    
    ## For graphs of a specific simulation
if plot_sim>=0: sim_save_dir = save_dir + "sample_{:d}/".format(plot_sim)
else: sim_save_dir = save_dir + "all_samples/"
if not os.path.isdir(sim_save_dir) : os.mkdir(sim_save_dir)

# Loading final state data
aor_ss, e_ss, seed_ss, n_erased_ss, zc_ss = [], [], [], [], []
e0_ss, zc0_ss = [], []
sig11_ss, sig22_ss, sig33_ss, sig12_ss, sig21_ss, sig23_ss, sig32_ss, sig13_ss, sig31_ss = [], [], [], [], [], [], [], [], []
for model in model_s: 
    csv_file_path = "ppc/{:}_model.csv".format(model)

    ## Read data
    aor_s, e_s, seed_s, n_erased_s, zc_s = [], [], [], [], []
    e0_s, zc0_s = [], []
    sig11_s, sig22_s, sig33_s, sig12_s, sig21_s, sig23_s, sig32_s, sig13_s, sig31_s = [], [], [], [], [], [], [], [], []
    with open(csv_file_path, 'r') as fil: lines = fil.readlines()[1:]
    for line in lines:
        values = [float(i) for i in line.split("\t")]
        seed_s.append(values[0])
        aor_s.append(values[1])
        e_s.append(values[2])
        n_erased_s.append(values[3])
        zc_s.append(values[4])
        e0_s.append(values[5])
        zc0_s.append(values[6])
        sig11_s.append(values[7]) 
        sig22_s.append(values[11])
        sig33_s.append(values[15])
        sig12_s.append(values[8]) 
        sig21_s.append(values[10])
        sig23_s.append(values[12])
        sig32_s.append(values[14])
        sig13_s.append(values[9])
        sig31_s.append(values[13]) 

    seed_ss.append(seed_s)
    aor_ss.append(aor_s)
    e_ss.append(e_s)
    n_erased_ss.append(n_erased_s)
    zc_ss.append(zc_s)
    e0_ss.append(e0_s)
    zc0_ss.append(zc0_s)
    sig11_ss.append(sig11_s) 
    sig22_ss.append(sig22_s)
    sig33_ss.append(sig33_s)
    sig12_ss.append(sig12_s) 
    sig21_ss.append(sig21_s)
    sig23_ss.append(sig23_s)
    sig32_ss.append(sig32_s)
    sig13_ss.append(sig13_s)
    sig31_ss.append(sig31_s) 

# Compute macroscopic friction angle (should move that to ppc.py)
phi_ss = []
for sig11_s, sig22_s, sig33_s, sig12_s, sig21_s, sig23_s, sig32_s, sig13_s, sig31_s in zip(sig11_ss, sig22_ss, sig33_ss, sig12_ss, sig21_ss, sig23_ss, sig32_ss, sig13_ss, sig31_ss):
    phi_s = []
    for sig11, sig22, sig33, sig12, sig21, sig23, sig32, sig13, sig31 in zip(sig11_s, sig22_s, sig33_s, sig12_s, sig21_s, sig23_s, sig32_s, sig13_s, sig31_s):
        sigma = np.array([[sig11, sig12, sig13], 
                          [sig21, sig22, sig23],
                          [sig31, sig32, sig33]])
        sigma_diag, _ = np.linalg.eig(sigma)
        s3, _, s1 = np.sort(sigma_diag)
        phi_s.append(np.arctan((s1-s3)/(2*np.sqrt(s1*s3)))*180/np.pi)
    phi_ss.append(phi_s)

# Loading simulation's data
iteration_ss, speed_ss, time_ss, uf_ss, ke_ss, door_heigth_ss, n_erased_sim_ss, ZC_ss = [], [], [], [], [], [], [], []
time_step_s = []
if plot_sim>=0:
    for model in model_s: 

        ## Get simulation's run id
        run_id = os.path.realpath(glob.glob("final_states/final_state_{:}_{:d}_parts_{:d}.yade*.bz2".format(model, n_part, plot_sim))[0]).split("run_")[-1].split("/")[0]

        ## Simulation's data file
        data_file_path = "sims_{:}/run_{:}/{:}_data".format(model, run_id, run_id)

        ## Read data
        iteration_s, speed_s, time_s, uf_s, ke_s, door_heigth_s, n_erased_sim_s, ZC_s = [], [], [], [], [], [], [], []
        with open(data_file_path, 'r') as fil: lines = fil.readlines()[2:]
        for line in lines:
            values = [float(i) for i in line.split("\t")[:-1]]

            iteration_s.append(int(values[0]))
            speed_s.append(values[3])
            time_s.append(values[2])
            uf_s.append(values[5])
            ke_s.append(values[6])
            door_heigth_s.append(values[8])
            n_erased_sim_s.append(int(values[9]))
            ZC_s.append(values[7])

            time_step = values[1] # Time step has the same value during all the simulation
        
        ## Set t0 = 0 s
        time_s = [t-time_s[0] for t in time_s]

        iteration_ss.append(iteration_s)
        speed_ss.append(speed_s)
        time_ss.append(time_s)
        uf_ss.append(uf_s)
        ke_ss.append(ke_s)
        door_heigth_ss.append(door_heigth_s)
        n_erased_sim_ss.append(n_erased_sim_s)
        ZC_ss.append(ZC_s)

        time_step_s.append(time_step)

else: # Import all simulations' data
    speed_std_ss, uf_std_ss, ke_std_ss, n_erased_sim_std_ss, iteration_std_ss, time_std_ss, ZC_std_ss = [], [], [], [], [], [], []
    iteration_SSS, speed_SSS, time_SSS, uf_SSS, ke_SSS, door_heigth_SSS, n_erased_sim_SSS, ZC_SSS = [], [], [], [], [], [], [], []
    for model in model_s:
        iteration_SS, speed_SS, time_SS, uf_SS, ke_SS, door_heigth_SS, n_erased_sim_SS, ZC_SS = [], [], [], [], [], [], [], []
        for seed in range(-plot_sim):

            ## Get simulation's run id
            run_id = os.path.realpath(glob.glob("final_states/final_state_{:}_2150_parts_{:d}.yade*.bz2".format(model, seed))[0]).split("run_")[-1].split("/")[0]

            ## Simulation's data file
            data_file_path = "sims_{:}/run_{:}/{:}_data".format(model, run_id, run_id)

            ## Read data
            iteration_S, speed_S, time_S, uf_S, ke_S, door_heigth_S, n_erased_sim_S, ZC_S = [], [], [], [], [], [], [], []
            with open(data_file_path, 'r') as fil: lines = fil.readlines()[2:]
            for line in lines:
                values = [float(i) for i in line.split("\t")[:-1]]

                iteration_S.append(int(values[0]))
                speed_S.append(values[3])
                time_S.append(values[2])
                uf_S.append(values[5])
                ke_S.append(values[6])
                door_heigth_S.append(values[8])
                n_erased_sim_S.append(int(values[9]))
                ZC_S.append(values[7])

                time_step = values[1] # Time step has the same value during all the simulation
            
            ## Set t0 = 0 s
            time_S = [t-time_S[0] for t in time_S]

            ## Fill list (seed) of list (data points)
            iteration_SS.append(iteration_S)
            speed_SS.append(speed_S)
            time_SS.append(time_S)
            uf_SS.append(uf_S)
            ke_SS.append(ke_S)
            door_heigth_SS.append(door_heigth_S)
            n_erased_sim_SS.append(n_erased_sim_S)
            ZC_SS.append(ZC_S)

            time_step_s.append(time_step)

        ## Fill list (model) of list (seed) of list (data points)
        iteration_SSS.append(iteration_SS)
        speed_SSS.append(speed_SS)
        time_SSS.append(time_SS)
        uf_SSS.append(uf_SS)
        ke_SSS.append(ke_SS)
        door_heigth_SSS.append(door_heigth_SS)
        n_erased_sim_SSS.append(n_erased_sim_SS)
        ZC_SSS.append(ZC_SS)

    ## Compute mean value and std over all seeds
    speed_std_s, uf_std_s, ke_std_s, n_erased_sim_std_s, ZC_std_s = [], [], [], [], []

    n_max_el_s = []
    for iteration_SS, speed_SS, time_SS, uf_SS, ke_SS, door_heigth_SS, n_erased_sim_SS, ZC_SS in zip(iteration_SSS, speed_SSS, time_SSS, uf_SSS, ke_SSS, door_heigth_SSS, n_erased_sim_SSS, ZC_SSS):
        ### Detect the maximum number of data points for all seeds for a model
        n_max_el = 0
        for iteration_S in iteration_SS:
            if len(iteration_S)>n_max_el: n_max_el = len(iteration_S)
        n_max_el_s.append(n_max_el)

        ### Fill list (data points) that don't reach the maximum length with NaN variable
        for iteration_S, speed_S, time_S, uf_S, ke_S, door_heigth_S, n_erased_sim_S, ZC_S in zip(iteration_SS, speed_SS, time_SS, uf_SS, ke_SS, door_heigth_SS, n_erased_sim_SS, ZC_SS):
            n_fill = n_max_el - len(iteration_S)
            for l in [iteration_S, speed_S, time_S, uf_S, ke_S, door_heigth_S, n_erased_sim_S, ZC_S]: l += [np.nan] * n_fill
        
        ### Fill list (model) of list (mean value and std of data points over all seeds)
        iteration_ss.append(np.nanmean(np.array(iteration_SS), axis=0))
        speed_ss.append(np.nanmean(np.array(speed_SS), axis=0))
        time_ss.append(np.nanmean(np.array(time_SS), axis=0))
        uf_ss.append(np.nanmean(np.array(uf_SS), axis=0))
        ke_ss.append(np.nanmean(np.array(ke_SS), axis=0))
        door_heigth_ss.append(np.nanmean(np.array(door_heigth_SS), axis=0))
        n_erased_sim_ss.append(np.nanmean(np.array(n_erased_sim_SS), axis=0))
        ZC_ss.append(np.nanmean(np.array(ZC_SS), axis=0))

        speed_std_ss.append(np.nanstd(np.array(speed_SS), axis=0))
        uf_std_ss.append(np.nanstd(np.array(uf_SS), axis=0))
        ke_std_ss.append(np.nanstd(np.array(ke_SS), axis=0))
        n_erased_sim_std_ss.append(np.nanstd(np.array(n_erased_sim_SS), axis=0))
        time_std_ss.append(np.nanstd(np.array(time_SS), axis=0))
        iteration_std_ss.append(np.nanstd(np.array(iteration_SS), axis=0))
        ZC_std_ss.append(np.nanstd(np.array(ZC_SS), axis=0))

    ## Get ordered list of final times, to know where the size of the sample stop being statistically representative
        ### Useful function to get last number in a list
    def last(l, i=-1):
        if np.isnan(l[i]): return last(l, i-1)
        else: return l[i]
    
        ### Get last times
    last_times_ss = [] # List (model) of ordered list (seed)
    for time_SS in time_SSS:
        last_times_s = [last(time_S) for time_S in time_SS]
        last_times_s.sort()
        last_times_ss.append(last_times_s)


# time_step_s = [[time_step_s[0][0]]*len(iteration_ss[0]), [time_step_s[1][0]]*len(iteration_ss[1])]


# Loading the outer surface
if plot_sim>=0:
    outer_surface_s = []
    for model in model_s:
        with open("ppc/outer_surface_{:}_{:d}.csv".format(model, plot_sim), "r") as fil: lines = fil.readlines()[1:]

        outer_surface = [[float(i) for i in line.split("\t")] for line in lines]
        outer_surface_s.append(np.array(outer_surface))

#####################################################################################################################
# Plot data
fig1 = plt.figure(1, figsize=(15,7))

title1 = r"Angle of repose $\alpha$ for different samples"

# Loop on the models
legend_elements, tweaked_labels = [], []
for aor_s, color, label in zip(aor_ss, color_s, label_s):

    ## Create histogram
    hist, bins = np.histogram(aor_s, 10)
    hatchs = [r'"\\\"', "///"] * int(len(hist)/2)

    ## Plot histogram
    for occ, i, h in zip(hist/len(aor_s), range(hist.shape[0]), hatchs):
        if occ!=0: plt.fill_between([bins[i], bins[i+1]], [0, 0], [occ, occ], facecolor="none", hatch=h, edgecolor=color, linewidth=1.0)

    ## Add a line to the legend
    legend_elements.append(Line2D([0], [0], color=color, lw=1))

    ## Add AOR measure to legend
    tweaked_labels.append(label + r", $\alpha={:.2f} ^{{\circ}} \pm {:.2f} ^{{\circ}}$".format(np.mean(aor_s), np.std(aor_s)))

ax = plt.gca()
ax.legend(legend_elements, tweaked_labels, fontsize=23)

# plt.title(title1)
plt.ylabel('Relative frequency')
plt.xlabel(r"$\alpha$ $(^{{\circ}})$")

fig1.savefig(save_dir + translate_str(title1) + '.pdf', bbox_inches="tight", format='pdf')


########################################################################################
fig2 = plt.figure(2, figsize=(15,10))

if plot_sim>=0: 
    suffix = "\nfor sample {:d}".format(plot_sim)
    s = 1
else: 
    suffix = "\nfor all sample"
    s = .4
title2 = r"Computation speed $S$ against simulation time $t$" # + suffix

if plot_sim<0:
    for speed_s, speed_std_s, time_s, color, label in zip(speed_ss, speed_std_ss, time_ss, color_s, label_s):
        speed_s = np.array(speed_s)
        speed_std_s = np.array(speed_std_s)
        ## Plot data
        plt.fill_between(time_s, speed_s-speed_std_s, speed_s+speed_std_s, color=color, alpha=0.5)
        # plt.gca().errorbar(time_s, speed_s, yerr=speed_std_s, fmt='o', markerfacecolor="red", capsize=10, color=color, label=label)

# Loop on the models
legend_elements = []
for speed_s, time_s, color, label in zip(speed_ss, time_ss, color_s, label_s):
    ## Plot data
    legend_elements.append(Line2D([0], [0], color='w', markerfacecolor=color, marker='.', markersize=20))
    plt.scatter(time_s, speed_s, color=color, s=s, label=label)

# plt.title(title2)
plt.ylabel(r"$S$ ($iteration/s$)")
plt.xlabel(r"Model time $t$ ($s$)")
plt.legend(legend_elements, label_s, loc="best")

# Set log scale on speed axis, necessary because PP model is a lot slower (~400x) than CLP model
plt.gca().set_yscale('log')

fig2.savefig(sim_save_dir + translate_str(title2) + '.pdf', bbox_inches="tight", format='pdf')

########################################################################################
fig2b = plt.figure(200, figsize=(15,10))

if plot_sim>=0: suffix, s = "\nfor sample {:d}".format(plot_sim), 1
else: suffix, s = "\nfor all sample", .4

title2b = r"Cundall's number $N_{C}$ against simulation time $t$" # + suffix

if plot_sim<0:
    for time_s, time_std_s, n_erased_sim_s, n_erased_sim_std_s, iteration_s, iteration_std_s, color, label in zip(time_ss, time_std_ss, n_erased_sim_ss, n_erased_sim_std_ss, iteration_ss, iteration_std_ss, color_s, label_s):
        t_s, nit_s, ne_s, t_std_s, nit_std_s, ne_std_s = [np.array(i) for i in [time_s, iteration_s, n_erased_sim_s, time_std_s, iteration_std_s, n_erased_sim_std_s]]
        np_s, np_std_s = n_part - ne_s, ne_std_s

        cundall_nbr_s = nit_s * np_s / t_s
        cundall_nbr_std_s = cundall_nbr_s * (t_std_s/t_s + nit_std_s/nit_s + np_std_s/np_s)
        ## Plot data
        plt.fill_between(time_s, cundall_nbr_s-cundall_nbr_std_s, cundall_nbr_s+cundall_nbr_std_s, color=color, alpha=0.5)
        # plt.gca().errorbar(time_s, speed_s, yerr=speed_std_s, fmt='o', markerfacecolor="red", capsize=10, color=color, label=label)

# Loop on the models
legend_elements = []
for speed_s, time_s, color, label in zip(speed_ss, time_ss, color_s, label_s):
    ## Plot data
    legend_elements.append(Line2D([0], [0], color='w', markerfacecolor=color, marker='.', markersize=20))
    # plt.scatter(time_s, speed_s, color=color, s=s, label=label)

# plt.title(title2)
plt.ylabel(r"$N_C$ (~$iteration/s$)")
plt.xlabel(r"Model time $t$ ($s$)")
plt.legend(legend_elements, label_s, loc="best")

# Set log scale on speed axis, necessary because PP model is a lot slower (~400x) than CLP model
plt.gca().set_yscale('log')

fig2b.savefig(sim_save_dir + translate_str(title2b) + '.pdf', bbox_inches="tight", format='pdf')


########################################################################################
fig3 = plt.figure(3, figsize=(15,10))

if plot_sim>=0: 
    suffix = "\nfor sample {:d}".format(plot_sim)
    s = 1
else: 
    suffix = "\nfor all sample"
    s = .4
title3 = r"Number of lost particles $N_{lost}$ against simulation time $t$" # + suffix


if plot_sim<0:
    # Loop on the models
    for n_erased_sim_s, n_erased_sim_std_s, time_s, color, label in zip(n_erased_sim_ss, n_erased_sim_std_ss, time_ss, color_s, label_s):
        n_erased_sim_s = np.array(n_erased_sim_s)
        n_erased_sim_std_s = np.array(n_erased_sim_std_s)

        ## Plot data
        plt.fill_between(time_s, n_erased_sim_s-n_erased_sim_std_s, n_erased_sim_s+n_erased_sim_std_s, color=color, alpha=0.5)


# Loop on the models
legend_elements = []
for n_erased_sim_s, time_s, color, label in zip(n_erased_sim_ss, time_ss, color_s, label_s):
    ## Plot data
    legend_elements.append(Line2D([0], [0], color='w', markerfacecolor=color, marker='.', markersize=20))
    plt.scatter(time_s, n_erased_sim_s, color=color, s=s, label=label)


# plt.title(title3)
plt.ylabel(r"$N_{lost}$")
plt.xlabel(r"$t$ ($s$)")
plt.legend(legend_elements, label_s, loc="best")

fig3.savefig(sim_save_dir + translate_str(title3) + '.pdf', bbox_inches="tight", format='pdf')

########################################################################################
fig4 = plt.figure(4, figsize=(15,10))

if plot_sim>=0: suffix = "\nfor sample {:d}".format(plot_sim)
else: suffix = "\nfor all sample"
title4 = r"Unbalanced force $U_f$ and kinetic energy $E_k$ against simulation time $t$" # + suffix

# Create twin axis
ax = plt.gca()
tax = ax.twinx()

if plot_sim<0:
    ## Loop on the models
    for uf_s, uf_std_s, ke_s, ke_std_s, time_s, color, label in zip(uf_ss, uf_std_ss, ke_ss, ke_std_ss, time_ss, color_s, label_s):
        ## Plot data
        uf_s = np.array(uf_s)
        uf_std_s = np.array(uf_std_s)
        ke_s = np.array(ke_s)
        ke_std_s = np.array(ke_std_s)

        plt.fill_between(time_s, uf_s-uf_std_s, uf_s+uf_std_s, color=color, alpha=0.5)
        plt.fill_between(time_s, ke_s-ke_std_s, ke_s+ke_std_s, color=color, alpha=0.5)

else: 
    ## Loop on the models
    for uf_s, ke_s, time_s, color, label in zip(uf_ss, ke_ss, time_ss, color_s, label_s):
        ## Plot data
        ax.plot(time_s, uf_s, color=color, label=r"$U_f$ " + label)
        tax.plot(time_s, ke_s, color=color, linestyle='dotted', label=r"$E_k$ " + label)

# plt.title(title4)
ax.set_ylabel(r"$U_f$")
tax.set_ylabel(r"$E_k$ ($J$)")
ax.set_xlabel(r"$t$ ($s$)")
fig4.legend(bbox_to_anchor=(.14,.12), loc="lower left")

# Set log scale on vertical axes
ax.set_yscale('log')
tax.set_yscale('log')

fig4.savefig(sim_save_dir + translate_str(title4) + '.pdf', bbox_inches="tight", format='pdf')

########################################################################################
if plot_sim>=0:
    fig5 = plt.figure(5, figsize=(15,10))

    title5 = r"Outer surface of the heap"  + " for sample {:d}".format(plot_sim)


    # Loop on the models
    for outer_surface, color, label in zip(outer_surface_s, color_s, label_s):
        ## Plot data
        plt.scatter(outer_surface[:,0], outer_surface[:,2], color=color, label=label)

    # plt.title(title5)
    plt.ylabel(r"$z$ ($m$)")
    plt.xlabel(r"$x$ ($m$)")
    plt.legend(loc="best")

    plt.gca().set_aspect(1)

    fig5.savefig(sim_save_dir + translate_str(title5) + '.pdf', bbox_inches="tight", format='pdf')

########################################################################################
fig6 = plt.figure(6, figsize=(15,7))

title6 = r"Final void ratio $e$ for different samples"

# Loop on the models
legend_elements, tweaked_labels = [], []
for e_s, color, label in zip(e_ss, color_s, label_s):

    ## Create histogram
    hist, bins = np.histogram(e_s, 10)
    hatchs = [r'"\\\"', "///"] * int(len(hist)/2)

    ## Plot histogram
    for occ, i, h in zip(hist/len(e_s), range(hist.shape[0]), hatchs):
        if occ!=0: plt.fill_between([bins[i], bins[i+1]], [0, 0], [occ, occ], facecolor="none", hatch=h, edgecolor=color, linewidth=1.0)

    ## Add a line to the legend
    legend_elements.append(Line2D([0], [0], color=color, lw=1))

    ## Add AOR measure to legend
    tweaked_labels.append(label + "," + "\n\t" + r" $e={:.3f} \pm {:.3f}$".format(np.mean(e_s), np.std(e_s)))

ax = plt.gca()
ax.legend(legend_elements, tweaked_labels, bbox_to_anchor=(.61, .99), loc="upper left", fontsize=20)

# plt.title(title6)
plt.ylabel('Relative frequency')
plt.xlabel(r"$e$")

fig6.savefig(save_dir + translate_str(title6) + '.pdf', bbox_inches="tight", format='pdf')

########################################################################################
fig7 = plt.figure(7, figsize=(15,7))

title7 = r"Number of lost particles $N_{lost}$ for different samples"

# Loop on the models
legend_elements, tweaked_labels = [], []
for n_erased_s, color, label in zip(n_erased_ss, color_s, label_s):

    ## Create histogram
    hist, bins = np.histogram(n_erased_s, 10)
    hatchs = [r'"\\\"', "///"] * int(len(hist)/2)

    ## Plot histogram
    for occ, i, h in zip(hist/len(n_erased_s), range(hist.shape[0]), hatchs):
        if occ!=0: plt.fill_between([bins[i], bins[i+1]], [0, 0], [occ, occ], facecolor="none", hatch=h, edgecolor=color, linewidth=1.0)

    ## Add a line to the legend
    legend_elements.append(Line2D([0], [0], color=color, lw=1))

    ## Add AOR measure to legend
    tweaked_labels.append(label + r", $N_{{lost}}={:d} \pm {:d}$".format(round(np.mean(n_erased_s)), round(np.std(n_erased_s))))

ax = plt.gca()
ax.legend(legend_elements, tweaked_labels, bbox_to_anchor=(.25, .99), loc="upper left")

# plt.title(title7)
plt.ylabel('Relative frequency')
plt.xlabel(r"$N_{lost}$")

fig7.savefig(save_dir + translate_str(title7) + '.pdf', bbox_inches="tight", format='pdf')

########################################################################################
fig8 = plt.figure(8, figsize=(15,7))

title8 = r"Average number of interactions $Z_c$ for different samples"

# Loop on the models
legend_elements, tweaked_labels = [], []
for zc_s, color, label in zip(zc_ss, color_s, label_s):

    ## Create histogram
    hist, bins = np.histogram(zc_s, 10)
    hatchs = [r'"\\\"', "///"] * int(len(hist)/2)

    ## Plot histogram
    for occ, i, h in zip(hist/len(zc_s), range(hist.shape[0]), hatchs):
        if occ!=0: plt.fill_between([bins[i], bins[i+1]], [0, 0], [occ, occ], facecolor="none", hatch=h, edgecolor=color, linewidth=1.0)

    ## Add a line to the legend
    legend_elements.append(Line2D([0], [0], color=color, lw=1))

    ## Add AOR measure to legend
    tweaked_labels.append(label + r", $Z_c={:.2f} \pm {:.2f}$".format(np.mean(zc_s), np.std(zc_s)))

ax = plt.gca()
ax.legend(legend_elements, tweaked_labels, bbox_to_anchor=(.15, .99), loc="upper left")

# plt.title(title8)
plt.ylabel('Relative frequency')
plt.xlabel(r"$Z_c$")

fig8.savefig(save_dir + translate_str(title8) + '.pdf', bbox_inches="tight", format='pdf')

########################################################################################
fig9 = plt.figure(9, figsize=(15,7))

title9 = r"Initial void ratio $e_0$ for different samples"

# Loop on the models
legend_elements, tweaked_labels = [], []
for e0_s, color, label in zip(e0_ss, color_s, label_s):

    ## Create histogram
    hist, bins = np.histogram(e0_s, 10)
    hatchs = [r'"\\\"', "///"] * int(len(hist)/2)

    ## Plot histogram
    for occ, i, h in zip(hist/len(e0_s), range(hist.shape[0]), hatchs):
        if occ!=0: plt.fill_between([bins[i], bins[i+1]], [0, 0], [occ, occ], facecolor="none", hatch=h, edgecolor=color, linewidth=1.0)

    ## Add a line to the legend
    legend_elements.append(Line2D([0], [0], color=color, lw=1))

    ## Add AOR measure to legend
    tweaked_labels.append(label + "," + "\n\t" + r" $e_0={:.3f} \pm {:.3f}$".format(np.mean(e0_s), np.std(e0_s)))

ax = plt.gca()
ax.legend(legend_elements, tweaked_labels, bbox_to_anchor=(.28, 1.01), loc="upper left", fontsize=20)

# plt.title(title9)
plt.ylabel('Relative frequency')
plt.xlabel(r"$e_0$")

fig9.savefig(save_dir + translate_str(title9) + '.pdf', bbox_inches="tight", format='pdf')

########################################################################################
fig10 = plt.figure(10, figsize=(15,7))

title10 = r"Initial average number of interactions $Z_{c0}$ for different samples"

# Loop on the models
legend_elements, tweaked_labels = [], []
for zc0_s, color, label in zip(zc0_ss, color_s, label_s):

    ## Create histogram
    hist, bins = np.histogram(zc0_s, 10)
    hatchs = [r'"\\\"', "///"] * int(len(hist)/2)

    ## Plot histogram
    for occ, i, h in zip(hist/len(zc0_s), range(hist.shape[0]), hatchs):
        if occ!=0: plt.fill_between([bins[i], bins[i+1]], [0, 0], [occ, occ], facecolor="none", hatch=h, edgecolor=color, linewidth=1.0)

    ## Add a line to the legend
    legend_elements.append(Line2D([0], [0], color=color, lw=1))

    ## Add AOR measure to legend
    tweaked_labels.append(label + r", $Z_{{c0}}={:.2f} \pm {:.2f}$".format(np.mean(zc0_s), np.std(zc0_s)))

ax = plt.gca()
ax.legend(legend_elements, tweaked_labels, bbox_to_anchor=(.2, .99), loc="upper left")

# plt.title(title10)
plt.ylabel('Relative frequency')
plt.xlabel(r"$Z_{c0}$")

fig10.savefig(save_dir + translate_str(title10) + '.pdf', bbox_inches="tight", format='pdf')

########################################################################################
plt.close('all')
########################################################################################
fig11 = plt.figure(11, figsize=(15,10))

if plot_sim>=0: 
    suffix = "\nfor sample {:d}".format(plot_sim)
    s = 1
else: 
    suffix = "\nfor all sample"
    s = .4
title11 = r"Average coordination number $Z_c$ against simulation time $t$" # + suffix


if plot_sim<0:
    # Loop on the models
    for ZC_s, ZC_std_s, time_s, color, label in zip(ZC_ss, ZC_std_ss, time_ss, color_s, label_s):
        ZC_s = np.array(ZC_s)
        ZC_std_s = np.array(ZC_std_s)

        ## Plot data
        plt.fill_between(time_s, ZC_s-ZC_std_s, ZC_s+ZC_std_s, color=color, alpha=0.5)


# Loop on the models
legend_elements = []
for ZC_s, time_s, color, label in zip(ZC_ss, time_ss, color_s, label_s):
    ## Plot data
    legend_elements.append(Line2D([0], [0], color='w', markerfacecolor=color, marker='.', markersize=20))
    plt.scatter(time_s, ZC_s, color=color, s=s, label=label)


# plt.title(title11)
plt.ylabel(r"$Z_c$")
plt.xlabel(r"$t$ ($s$)")
plt.legend(legend_elements, label_s, loc="best")

fig11.savefig(sim_save_dir + translate_str(title11) + '.pdf', bbox_inches="tight", format='pdf')

########################################################################################
fig12 = plt.figure(12, figsize=(15,10))

title12 = r"Angle of repose $\alpha$ against macroscopic friction angle $\phi$"

# Loop on the models
legend_elements, tweaked_labels = [], []
for aor_s, phi_s, color, label in zip(aor_ss, phi_ss, color_s, label_s):
    plt.scatter(phi_s, aor_s, color=color, label=label)

# plt.title(title1)
plt.ylabel(r'$\alpha$ $(^{{\circ}})$')
plt.xlabel(r"$\phi_{mob}$ $(^{{\circ}})$")

plt.legend(loc="best")

fig12.savefig(save_dir + translate_str(title12) + '.pdf', bbox_inches="tight", format='pdf')