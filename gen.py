import sys, os
sys.path.append(os.getcwd())

import numpy as np

# "PP" for Potential Particle
# "CLP" for Clump
model = "CLP" 

# Material's parameters
material_params = {
    "Kn": 1.2e3,
    "Ks": 0.2*1.2e3,
    "nu": 0.2,
    "viscous_damping": 0.071,
    "en": 0.8, # Should correspond to viscous_damping (the information is redundant, en has to be specified for the clump model)
    "frictionAngle_part2part": 35.5*np.pi/180,
    "frictionAngle_part2wall": 27.2*np.pi/180,
    "density": 1111.
}

# Simulation's parameters
damping = .2
gravity = -9.81
# if model == "PP": uF_threshold = .2 # Maximum value for the unbalanced force when checking stability
# elif model == "CLP": uF_threshold = 1e-2
uF_threshold = 1e-2


# Other parameters
seed = 0
sample_dir = "samples/"
if not os.path.isdir(sample_dir) : os.mkdir(sample_dir)

# Read parameters from table if running in batch
readParamsFromTable(seed=int(seed), model=model)
from yade.params.table import *
np.random.seed(seed) # Fixing random seed

# Import utility module for the selected model
if model == "PP": import utl_pp as utl
elif model == "CLP": import utl_clp as utl

# Create materials
material_ids = utl.create_materials(material_params)

# Set the model's engines
utl.set_engines(material_params, damping, gravity)

# Add particles
n_parts = utl.add_particles(material_ids[0], seed)

# Construct box
walls = utl.make_box(material_ids[1])
if model == "PP": bbb, b0, bA, bB, bC, bD, bbA, bbB, bbC, bbD, bH = walls
elif model == "CLP": boA, boB, lA, lB, rA, rB, baA_l, baB_l, baA_d, baB_d, fA, fB = walls

# Add PyRunners
utl.set_pyruners(utl.gen_pyrunners)

# Run simulation if it is part of batch
def run():
    ## Print all versions so saved file is easier to reopen
    printAllVersions()

    O.run(wait=True)
    sample_name = "{:}_{:d}_parts_{:d}.yade{:}.xml.bz2".format(model, n_parts, seed, version.split("-")[-1])

    ## Save walls
        ### Create a dictionnary with all wall objects
    walls_names = [[ i for i, a in globals().items() if id(a) == id(x)][0] for x in walls]
    wall_dict = {key: val for key,val in zip(walls_names, walls)}

        ### Use YADE's saveVars
    saveVars('walls', **wall_dict)
    
    O.save(sample_dir + sample_name)

if runningInBatch(): run()




