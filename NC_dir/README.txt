=========
 SCRIPTS
=========
roundRobin_PP_Device_1_batch.py		-	Run Device 1 models
roundRobin_PP_Device_2_batch.py		-	Run Device 2 models
roundRobin_writeVTK_dev01.py		-	Device 1 - Post-processing script to save equivalent clump particles (considering spheres at the vertices of the PP)
roundRobin_writeVTK_dev02.py		-	Device 2 - Same as above

The only parameter changing among different runs in controlled by the variable "case" within both models, which I control in batch mode or manually.

===================
 RUN IN BATCH MODE
===================
./yade-2020-03-09.git-04cf6a0-batch --job-threads 1 params.table roundRobin_PP_Device_1_batch.py
./yade-2020-03-09.git-04cf6a0-batch --job-threads 1 params.table roundRobin_PP_Device_2_batch.py
