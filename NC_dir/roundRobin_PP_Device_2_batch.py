#!/usr/local/bin/yade-trunk -x
# -*- encoding=utf-8 -*-
# 2020 (c) Vasileios Angelidakis

#COMMENT: In this second run, I choose:  Ks=Kn*nu, nu=0.37
#COMMENT: In a third run, I will choose: Ks=Kn/(2*(1+nu))
#COMMENT: In a fourth run, I will choose reduced: density=0.849*1111 and maybe the Kn of the third run

# FIXME: Seed values not used -> Update trunk

# This is a script to simulate the 2nd benchmark problem of the "Round robin test of angle of repose (AOR)".
# This second benchmark regards the axial-symmetric type experimental setup.

from yade import pack
import math

import yade.timing;
O.timingEnabled=False
if O.timingEnabled:
	yade.timing.reset() # not necessary if used for the first time, but it doesn't hurt having it here

import os
import errno
try:
   os.mkdir('./vtk/')
except OSError as exc:
   if exc.errno != errno.EEXIST:
      raise
   pass


# ENGINES
nu=0.37		# Poisson ratio
Kn=1.2e3	# FIXME: I have decreased the stiffness by 100!!

# run with: ./yade-2020-03-09.git-04cf6a0-batch --job-threads=2 params.txt roundRobin_PP_Device_2_batch.py
utils.readParamsFromTable(case=1)
from yade.params import table

case=table.case #case=1

if case==1:
	Ks=0.2*Kn
	density = 1.000*1111
	seedNum=5

elif case==2:
	Ks=0.2*Kn
	density = 0.849*1111
	seedNum=5

elif case==3:
	Ks=nu*Kn
	density = 0.849*1111
	seedNum=5

elif case==4:
	Ks=(1-nu)/(1-0.5*nu)*Kn
	density = 0.849*1111
	seedNum=5

elif case==5:
	Ks=(1-nu)/(1-0.5*nu)*Kn
	density = 0.849*1111
	seedNum=11

elif case>5:
	Ks=(1-nu)/(1-0.5*nu)*Kn
	density = 0.849*1111
	seedNum=case*2 # Seed just needs to be an integer

if case<10:
	subFix='00'+str(case)  #fileNameSubfix
else:
	subFix= '0'+str(case)

# FIXME: Revisit the verlet distance!!!
# Maybe assume either half the circumdiameter of the clump or half the radius of an individual sphere

# FIXME: In the insertion collider, I have increased the Aabb by 10%. Can I avoid that?

# FIXME: Restore:
#	Kn=1e5
#	verlet distance = 0.0015
#	newton.damping=0.0 after we deposit to the lower box
from numpy import array

def cuboid(material, edges=Vector3(1,1,1), k=0.0, r=0.0, R=0.0, center=[0,0,0], mask=1, isBoundary=False, fixed=False, color=[-1,-1,-1]):
	"""creates cuboid using the Potential Blocks

	:param Vector3 edges: edges of the cuboid
	:param Material material: material of new body (FrictMat)
	:param Vector3 center: center of the new body
	"""
	aa=[  1, -1,  0,  0,  0,  0 ]
	bb=[  0,  0,  1, -1,  0,  0 ]
	cc=[  0,  0,  0,  0,  1, -1 ]
	dd=[ edges[0]/2., edges[0]/2., edges[1]/2., edges[1]/2., edges[2]/2., edges[2]/2. ]

	aabbRotated	=	Vector3(dd[0], dd[2], dd[4])
	aabb		= 1.1 *	Vector3(dd[0], dd[2], dd[4])

	cuboid=Body()
	cuboid.shape = PotentialParticle(a=aa,b=bb,c=cc,d=array(dd)-r,k=k,r=r,R=R, isBoundary=isBoundary, color=color, AabbMinMax=True, maxAabbRotated=aabbRotated, minAabbRotated=aabbRotated, minAabb=aabb, maxAabb=aabb, fixedNormal=False, id=len(O.bodies))
	cuboid.mask=3
	utils._commonBodySetup(cuboid, 0, Vector3(0,0,0), material=material, pos=center, fixed=fixed)
	cuboid.state.pos = center
	return cuboid

def oblique_cuboid(material, edges=Vector3(1,1,1), k=0.0, r=0.0, R=0.0, center=[0,0,0], mask=1, isBoundary=False, fixed=False, color=[-1,-1,-1],showBodies=True):
	"""creates cuboid using the Potential Blocks

	:param Vector3 edges: edges of the cuboid
	:param Material material: material of new body (FrictMat)
	:param Vector3 center: center of the new body
	"""
	aa=[  1, -1,  0,  0,  1,  0 ]
	bb=[  0,  0,  1, -1,  0,  0 ]
	cc=[  0,  0,  0,  0,  1, -1 ]
	dd=[ edges[0]/2., edges[0]/2., edges[1]/2., edges[1]/2., edges[2]/2., edges[2]/2. ]

	aabbRotated	=	Vector3(dd[0], dd[2], dd[4])
	if showBodies==True:
		aabb		= 1.1 *	Vector3(dd[0], dd[2], dd[4])
	else:
		aabb		= Vector3.Zero
	cuboid=Body()
	cuboid.shape = PotentialParticle(a=aa,b=bb,c=cc,d=array(dd)-r,k=k,r=r,R=R, isBoundary=isBoundary, color=color, AabbMinMax=True, maxAabbRotated=aabbRotated, minAabbRotated=aabbRotated, minAabb=aabb, maxAabb=aabb, fixedNormal=False, id=len(O.bodies))
	cuboid.mask=3
	utils._commonBodySetup(cuboid, 0, Vector3(0,0,0), material=material, pos=center, fixed=fixed)
	cuboid.state.pos = center
	return cuboid


##**********************************************************************************
#creates regular prism with N faces

def prism(material, radius1=0.0, radius2=0.0, thickness=0.0, numFaces=3, k=0.0, r=0.0, R=0.0, center=[0,0,0], color=[1,0,0], mask=1, isBoundary=False, fixed=False):
	"""Return regular prism with numFaces

	:param Material material: material of new bodies (FrictMat)
	:param float radius1: inradius of the start cross-section of the prism
	:param float radius2: inradius of the end cross-section of the prism (equal to radius1 if unspecified)
	:param float thickness: thickness of the prism (equal to 1/10 of the inradius of the prism if not specified)
	:param int numFaces: number of cylinder faces (>3)
	:param float r: radius of inner Potential Particle (see PotentialBlock docs)
	:param float R: distance R of the Potential Blocks (see PotentialBlock docs)
	:param int mask: groupMask for the new bodies

	:returns: a list of PotentialBlock bodies enclosing the packing.
	"""
	aa=[]; bb=[]; cc=[]; dd=[];
	if not thickness: thickness=min(radius1,thickness/2.)/10.
	if not radius2: radius2=radius1
	angle=radians(360)/numFaces
	angle2=radians(0.01)
	for i in range(0,numFaces):
		aa.append(cos(i*angle))
		bb.append(sin(i*angle))
		cc.append(1-radius2/radius1) # assigning a value here can give us pyramides/cones or cylinder with non-parallel faces
		dd.append(radius1)

	aa.extend([0.0,  0.0])
	bb.extend([0.0,  0.0])
	cc.extend([1.0, -1.0])
	dd.extend([thickness, thickness])

	prism=Body()
	prism.mask=3
	prism.shape=PotentialParticle(a=aa,b=bb,c=cc,d=array(dd)-r,k=k,r=r,R=R,isBoundary=isBoundary, AabbMinMax=True, color=color,
		minAabb=1.0*Vector3(radius1,radius1,thickness),    maxAabb=1.0*Vector3(radius1,radius1,thickness),
		minAabbRotated=Vector3(radius1,radius1,thickness), maxAabbRotated=Vector3(radius1,radius1,thickness), id=len(O.bodies))
	if color[0] == -1:
		prism.shape.color = randomColor(seed=random.randint(0,1E6))
	else:
		prism.shape.color = color
	utils._commonBodySetup(prism,0,Vector3(0,0,0),material,pos=center,fixed=fixed)

	return prism


def cylindricalPlates(material, radius=0.0, height=0.0, thickness=0.0, numFaces=3, k=0.0, r=0.0, R=0.0, mask=1, isBoundary=False, fixed=True, wire=True, showBodies=True):
	"""Return numFaces cuboids that will wrap existing packing as walls from all sides. 			#FIXME: Correct this comment

	:param Material material: material of new bodies (FrictMat)
	:param float radius: radius of the cylinder
	:param float height: height of cylinder
	:param float thickness: thickness of cylinder faces (equal to 1/10 of the cylinder inradius if not specified)
	:param int numFaces: number of cylinder faces (>3)
	:param float r: radius of inner Potential Particle (see PotentialBlock docs)
	:param float R: distance R of the Potential Blocks (see PotentialBlock docs)
	:param int mask: groupMask for the new bodies

	:returns: a list of PotentialBlock bodies enclosing the packing.
	"""
	walls=[]
	if not thickness: thickness=min(radius,height/2.)/10.
	angle=radians(360)/numFaces
	axis=Vector3(0,0,1)	#FIXME: To make it work for any axis! I have to change: center, edges

	for i in range(0,numFaces):
		center=Vector3((radius+thickness/2.)*cos(i*angle), (radius+thickness/2.)*sin(i*angle), height/2.) #*(axis.asDiagonal())

		color=[abs(cos(i*angle)), abs(sin(i*angle)), 1]
		walls.append(oblique_cuboid(material=material, edges=Vector3(thickness,2*(radius)*tan(pi/numFaces), height), k=k, r=r, R=R, center=center, mask=mask, isBoundary=isBoundary, fixed=fixed, color=color,showBodies=showBodies))
		walls[-1].state.ori=Quaternion(axis,i*angle)
		walls[-1].shape.wire=wire
	return walls



O.engines=[
	ForceResetter(),
	InsertionSortCollider([PotentialParticle2AABB(aabbEnlargeFactor=1.1)],verletDist=0.0015, avoidSelfInteractionMask=2,label='collider'), #verletDist=0.01 or 0.00015
	InteractionLoop(
		[Ig2_PP_PP_ScGeom(twoDimension=False, unitWidth2D=1.0, calContactArea=False)], #FIXME: Check if we want constant stiffness or not
		[Ip2_FrictMat_FrictMat_KnKsPhys(kn_i=Kn, ks_i=Ks, Knormal = Kn, Kshear = Ks, useFaceProperties=False, viscousDamping=0.071)],
		[Law2_SCG_KnKsPhys_KnKsLaw(label='law',neverErase=False, allowViscousAttraction=True)]
	),
	NewtonIntegrator(damping=0.2,exactAsphericalRot=True,gravity=[0,0,-9.81], label='newton')
]

from numpy import array

O.materials.append(FrictMat(young=-1,poisson=-1,frictionAngle=radians(35.5),density=density,label='granular')) # Friction angle between clump-clump
O.materials.append(FrictMat(young=-1,poisson=-1,frictionAngle=radians(27.2),density=0,label='box'))            # Friction angle between clump-box

units=1000 #1000 for meters, 1 for milimeters

t_10 = 10/units #mm
t_25 = 25/units #mm
t_90 = 90/units #mm
t_100=100/units #mm
t_160=160/units #mm
t_340=340/units #mm

# Coordinates of tetrahedron, where the spheres of the clump are located (from Excel file)
Ds=10/units # [10mm=0.01m] Circumdiameter of clump
rs=Ds/(2*(1+sqrt(6)/4)) # radius of each sphere

ptA = Vector3( rs/2., rs*sqrt(3)/6., rs*sqrt(6)/3. )
ptB = Vector3( 0    , 0            , 0             )
ptC = Vector3( rs   , 0            , 0             )
ptD = Vector3( rs/2., rs*sqrt(3)/2., 0             )

# -------------------------------------------------------------------- #
## Tetrahedra

#from roundRobin_tetrahedral_PP import * # Import particle characteristics
# Approach 1: Tetrahedral Potential Particles
aa=array([ 0.000000000000000,  0.8164965809277260,  0.0000000000000000, -0.8164965809277259])
bb=array([ 0.000000000000000,  0.4714045207910317, -0.9428090415820634,  0.4714045207910317]) #-0.9428090415820634 instead of -1.0000000000000000
cc=array([-1.000000000000000,  0.3333333333333334,  0.3333333333333334,  0.3333333333333334])
dd=array([ 0.632993161855500,  0.6329931618555000,  0.6329931618555000,  0.6329931618555000])/(units)

k=0.65
r=rs/1.
R=sqrt(2)*rs

Igeom = Vector3(3.28600914143454e-12, 3.286145904978621e-12, 3.286197740532751e-12)
Volume = 0.0000003924806765

# -------------------------------------------------------------------- #
# Generate cloud of rounded tetrahedral particles
# -------------------------------------------------------------------- #
## Random cloud
#sp=pack.SpherePack()
#mn=Vector3(-(t_100+Ds)/2.,-(t_100-Ds)/2.,t_340+Ds/2.)
#mx=Vector3((t_100+Ds)/2.,(t_100-Ds)/2.,t_340*2.8)
#sp.makeCloud(mn,mx,R*1.0,0,4470,False,seed=seedNum) #4470 #FIXME: Here I set a fixed seed, to make the results reproducible! Should try different "seed" values!
#cyl = pack.inCylinder((0,0,t_340+Ds),(0,0,t_340*2.79),(t_100/2.-Ds/1.99))
#sp = pack.filterSpherePack(cyl,sp,True)

# -------------------------------------------------------------------- #
# Using randomDensePack
packing='random'
sp=pack.randomDensePack(pack.inCylinder((0,0,t_340+Ds/2.),(0,0,t_340*2.5),(t_100/2.-Ds/1.99)),radius=Ds/2., returnSpherePack=True, spheresInCell=500, material=O.materials['granular'], seed=seedNum) #FIXME: 15 November 2020
#print(len(sp))	# FIXME: How many particles do I need?

# -------------------------------------------------------------------- #
# Sort packing in ascending Z coordinates and delete excess particles to achieve sample size of 2468
zValues=[]
for s in sp:
	zValues.append(s[0][2])

from operator import itemgetter
indices, zValues_sorted = zip(*sorted(enumerate(zValues), key=itemgetter(1)))
list(zValues)
list(indices)

sp1=[]
for i in range(0,len(sp)):
	sp1.append(sp[indices[i]])

sp1=sp1[0:2468] #FIXME 2468 # 5 November 2020


# -------------------------------------------------------------------- #
# Count the number of spherical particles to verify sample size. We can comment this out later on.
print('The total number of spheres is: ',len(sp))



minAabbValue=1.2*Vector3(R,R,R)
maxAabbValue=1.2*Vector3(R,R,R)

for s in sp1: #sp
	dd=dd

	b=Body()
	b.aspherical=True
	b.mask=1
	wire=False
	color=Vector3(random.random(),random.random(),random.random())

	b.shape=PotentialParticle( k=k, r=r, R=R, a=aa, b=bb, c=cc, d=dd, isBoundary=False, color=color, wire=wire, highlight=False, 
	minAabb=minAabbValue,			#1.2*Vector3(R,R,R),
	maxAabb=maxAabbValue,			#1.2*Vector3(R,R,R),
	minAabbRotated=minAabbValue,		#1.2*Vector3(R,R,R), #FIXME: Not used with AabbMinMax=False
	maxAabbRotated=maxAabbValue,		#1.2*Vector3(R,R,R), #FIXME: Not used with AabbMinMax=False
	AabbMinMax=False, id=len(O.bodies) )

	b.shape.volume=Volume	#V
	b.shape.inertia=Igeom	#Vector3(geomInert,geomInert,geomInert)
	
	pos=s[0]

	utils._commonBodySetup(b, b.shape.volume, b.shape.inertia, material='granular', pos=pos, fixed=False) #s[0] stores center
	b.state.ori=Quaternion((random.random(),random.random(),random.random()),random.uniform(-2*pi,2*pi)) # angle from [-2pi-2pi]
	O.bodies.append(b)

Zmax=max(b.state.pos[2] for b in O.bodies if len(b.state.blockedDOFs)==0) + Ds/2.

# Plot number of dynamic bodies
countBodies=0
for b in O.bodies:
	if len(b.state.blockedDOFs)==0:
		countBodies=countBodies+1

print("Total number of dynamic bodies: ", countBodies)


from numpy import array

#Faces of the box
color=[0,0.5,1]
r=0.10*t_10
k=0.05
R=1.0*t_160 #FIXME: Choose a more appropriate value maybe?

# Cylinder up
cylUp=cylindricalPlates(material='box', radius=t_100/2., height=t_340*1.8+t_100, thickness=t_10, numFaces=20, k=0.2, r=r, R=R, mask=3, isBoundary=False, fixed=True, wire=False, showBodies=False)
O.bodies.append(cylUp)

basePt=Vector3(0,0,t_340-t_100)
for p in cylUp:
#	p.shape.color=[0,1,0]
	p.state.pos=p.state.pos+basePt

# Base up - Left
bL=cuboid(material='box', edges=Vector3(t_100,t_100,t_10), k=k, r=r, R=R, center=[-t_100/2.,0,t_340-t_10/2.], mask=3, isBoundary=False, fixed=True, color=[1,0,0])
O.bodies.append(bL)

# Base up - Right
bR=cuboid(material='box', edges=Vector3(t_100,t_100,t_10), k=k, r=r, R=R, center=[t_100/2.,0,t_340-t_10/2.], mask=3, isBoundary=False, fixed=True, color=[1,0,0])
O.bodies.append(bR)

# Cylinder down
cylDown=cylindricalPlates(material='box', radius=t_160/2., height=t_90, thickness=t_10, numFaces=20, k=0.2, r=r, R=R, mask=3, isBoundary=False, fixed=True, wire=False, showBodies=True)
O.bodies.append(cylDown)

#for p in cylDown:
#	p.shape.color=[0,1,0]

# Base down
baseDown=prism(material='box',radius1=t_160/2.,thickness=t_10,numFaces=20,k=k,r=r,R=R,center=[0,0,-t_10],color=color,mask=3,isBoundary=False,fixed=True)
O.bodies.append(baseDown)




# ------------------------------------------------------------------------------------------------------------------------------------------
# Box below
# ------------------------------------------------------------------------------------------------------------------------------------------

#V=0
#geomInert=Vector3(0,0,0)

# ------------------------------------------------------------------------------------------------------------------------------------------
# Steps we follow
# 1. we deposit the particles in the upper box and we leave them rest, under high local damping
# 2. we remove the local damping and leave active only the viscous damping
# 3. we move the horizontal plate bH, let the particles move to the bottom box and leave them rest
# 4. we delete the upper box and the horizontal plate bH
# 5. we move the movable plate bA upwards, and let the particles flow outside the box
# 6. we delete the particles that leave the box

# ------------------------------------------------------------------------------------------------------------------------------------------
# Additional engines to control the boundary plates & the simulation in general

# FIXME: Need to revise the iterPeriods below: When I use 2000 particles, is 1000 large/small?

O.engines=O.engines+[PyRunner(iterPeriod=100,command='controlStage()',label='cS', dead=False)] #cS=O.engines[4]
O.engines=O.engines+[PyRunner(iterPeriod=100,command='moveHorizontalPlate()',label='mH', dead=True)] #mH=O.engines[5]
O.engines=O.engines+[PyRunner(iterPeriod=100,command='deleteUpperBox()',label='dUB', dead=True)] #duB=O.engines[6]
O.engines=O.engines+[PyRunner(iterPeriod=100,command='moveVerticalPlate()',label='mV', dead=True)] #mV=O.engines[XXX]
O.engines=O.engines+[PyRunner(iterPeriod=100,command='finalStage()',label='fS', dead=True)] #fS=O.engines[XXX]

O.engines=O.engines+[PyRunner(iterPeriod= 20,command='pauseSimulation()',label='pS', dead=True)] #fS=O.engines[XXX]
#O.engines=O.engines+[PyRunner(iterPeriod= 1,command='print(O.iter)',label='pI', dead=False)] #fS=O.engines[XXX]

def sampleIsStable(ufMax, KEMax, KELim):
	global uf
	global KE
	try:
		uf=utils.unbalancedForce()
		KE=utils.kineticEnergy()
		print(uf, KE)
#		if (uf<ufMax and KE<1e-8) or (KE<1e-12):  #FIXME: Old value, used in Simulations 1-5
		if (uf<ufMax and KE<KEMax) or (KE<KELim):  #FIXME: New value
#			calm() # FIXME: I could assign calm() here
			return True
		else:
			return False
	except:
		print('Cannot calculate either uf or KE')
		return False

print(str(O.iter)+" cS is running")
def controlStage():
	maxPos=max(b.state.pos[2] for b in O.bodies if len(b.state.blockedDOFs)==0) + Ds/2. #or Ds/1. to be sure

	if sampleIsStable(ufMax=1.50, KEMax=1e-4, KELim=1e-7) and maxPos<t_340*1.8: #FIXME: Review this!!!
		print("The sample is stable on the upper cylinder!")
#		newton.damping=0.2 #FIXME: Maybe I could still keep some newton damping here, like 20% and turn it to 0.0 before we start moving the vertical plate
		mH.dead=False
#		calm() # FIXME: Do I need to invoke calm() just in case?
		cS.dead=True
		print(str(O.iter)+" mH is running")


def moveHorizontalPlate():
	if (bL.state.vel.norm()==0 and bR.state.vel.norm()==0):
		newton.damping=0.00 # FIXME: Maybe keep some local damping here if the sample cannot achieve equilibrium in the bottom box?
		bL.state.vel=[-40/units,0,0 ] # 0.040 m/s = 40 mm/s
		bR.state.vel=[ 40/units,0,0 ] # 0.040 m/s = 40 mm/s
		print(str(O.iter)+" bL, bR are moving")
	elif max(abs(bL.state.pos[0]),abs(bR.state.pos[0]))>=t_100+t_10/2.:
			print(str(O.iter)+" bL, bR are deleted")
			O.bodies.erase(bL.id)
			O.bodies.erase(bR.id)
			dUB.dead=False
			mH.dead=True
			print(str(O.iter)+" dUB is running")
	#i.e. if the plates are moving, but not deleted yet: we have to delete particles with negative coordinates
	eraseOutliers()

def deleteUpperBox():
	maxPos=max(b.state.pos[2] for b in O.bodies if len(b.state.blockedDOFs)==0) + Ds/2. #or Ds/1. to be sure
	if maxPos<t_340:
		for p in cylUp:
			O.bodies.erase(p.id)
		mV.dead=False
		dUB.dead=True
		print(str(O.iter)+" mV is running")
	eraseOutliers()

def moveVerticalPlate():
	global mVTime, mVIter, vW
	maxPos=max( b.state.pos[2] for b in O.bodies if len(b.state.blockedDOFs)==0 ) + Ds/2. #or Ds/1. to be sure

	eraseOutliers()

	if cylDown[0].state.vel.norm()==0 and sampleIsStable(ufMax=0.5, KEMax=1e-4, KELim=1e-8) and maxPos<t_90*2.0: #FIXME: *2.0 to be reviewed #FIXME: Return and see if I need "maxPos" check here!
		newton.damping=0.0
#		vW = [0,0, -40/60/units] #40 mm/min = 40/60 mm/sec
#		vW = [0,0,-120/60/units] # 120 mm/min = 2mm/sec
		vW = [0,0,-20/1/units] # 1200 mm/min = 20mm/sec 2020_05_20 11:55pm 
		for p in cylDown:
			p.state.vel=vW
#		bA.state.vel=vW #0.043m = 43mm/s
		print(str(O.iter)+" cylDown is moving")
		mVTime=time.time()
		mVIter=O.iter
#		print('mVTime: ', mVTime)
	elif cylDown[0].state.pos[2]<=-t_25+Ds:
		mV.iterPeriod=20 #Reduce iterPeriod to better approximate
		if cylDown[0].state.pos[2]<=-t_25:
			print(str(O.iter)+" cylDown has been stopped moving")
			for p in cylDown:
				p.state.vel=Vector3.Zero
			mV.dead=True
			fS.dead=False
			print(str(O.iter)+" fS is running")
	else: #	FIXME: to check if I really need this bit
		return

def finalStage():
	eraseOutliers()
	if sampleIsStable(ufMax=0.20, KEMax=1e-5, KELim=1e-8):
		print('Simulation almost finished. Writing output file. Simulation continues.')
		writeResults()
		if sampleIsStable(ufMax=0.15, KEMax=1e-5, KELim=1e-8):
			print('Simulation finished. Writing output file. Simulation paused.')
			O.engines=O.engines+[PotentialParticleVTKRecorder(fileName='./roundRobin_Device_2_Results/PP_VTK_Recorder_', twoDimension=False, iterPeriod=1, nDo=1, sampleX=30, sampleY=30, sampleZ=30, maxDimension=Ds/5.)]
			writeResults()
			O.pause()
			O.run(2)
			O.pause()
			pS.dead=False

def pauseSimulation():
	O.pause()

def eraseOutliers():
	for b in O.bodies:
		if (b.state.vel.norm() > 0.0) and (b.state.pos[2] < 0.0 or max(abs(b.state.pos[0]),abs(b.state.pos[1])) > t_160/2.) and (len(b.state.blockedDOFs)==0):
			O.bodies.erase(b.id,True)


#Save data details
directory='./roundRobin_Device_2_Results/'
if os.path.exists(directory)==False:
	os.mkdir(directory)
#else:
#	print('\n!! Save data: overwrite the contents of the folder roundRobin_results_run_2/ !!\n')


# ------------------------------------------------------------------------------------------------------------------------------------------

def writeResults():
	global endTime, endIter

	endTime=time.time()
	endIter=O.iter
	# Here I need to write the output positions, in the requested format
	f = open(directory+'dev02_run'+subFix+'.dat','w')
	for b in O.bodies:
		if len(b.state.blockedDOFs)==0 and b.state.pos[2]>0:
			#vertices = [b.state.pos + b.state.ori*v for v in b.shape.v] #TODO: I can vectorize this
			for v in b.shape.vertices:
				vRot=b.state.pos+b.state.ori*v
				f.write('%g %g %g\n'%(vRot[0],vRot[1],vRot[2]))
	f.close()

	# Here I output the elapsed time the simulation took to run
	f = open(directory+'dev02_run'+subFix+'_metadata.dat','w')

	f.write('rho  = %g kg/m^3\n'%(density))
	f.write('g    = %g m/sec^2\n'%(abs(newton.gravity[2])))
	f.write('dt   = %g sec\n'%(O.dt))
	f.write('D    = %g m\n'%(t_160/units))

	f.write('tr   = %g sec\n'%(endTime-mVTime))
	f.write('N    = %d steps\n'%(mVIter))
	f.write('T    = %g hours\n'%((endTime-startTime)/3600.))
	f.write('Zmax = %g m\n'%(Zmax))
	f.write('vW   = %g m/sec\n\n'%(vW[2]))

	f.write('(rho)  : Density\n')
	f.write('(g)    : Gravity\n')
	f.write('(dt)   : Time step\n')
	f.write('(D)    : Diameter of pedestal in Device II\n')
	f.write('(Zmax) : Initial height of the agglomerate\n')
	f.write('(vW)   : Elevating speed of the movable cylinder\n')

	f.write('(tr)   : Elapsed time at repose from starting to elevate of the movable cylinder\n')
	f.write('(N)    : Mean number of steps from starting to elevate of the movable cylinder\n')
	f.write('(T)    : Total execution time\n')

	f.close()

	print("Total execution time: ",str(endTime-startTime),"s")
	
	fileName="dev02_run"+subFix+"_savedScene.bz2"
	O.save(directory+fileName) #Save the scene to allow post-processing

#	if O.timingEnabled: #FIXME: Remember to reinstate this
#		yade.timing.stats()

#O.dt = 0.2*sqrt(0.3*O.bodies[0].state.mass/Kn)
O.dt = 0.2*sqrt(0.5*O.bodies[0].state.mass/Kn)

#if opts.nogui==False:
#	#Control the rendering quality
#	quality=24 #40
#	Gl1_PotentialParticle.aabbEnlargeFactor=1.2
#	Gl1_PotentialParticle.sizeX=quality
#	Gl1_PotentialParticle.sizeY=quality
#	Gl1_PotentialParticle.sizeZ=quality

#	from yade import qt
#	qt.Controller()
#	v=qt.View()

#	v.ortho=True
#	#v.sceneRadius=200
#	v.eyePosition=Vector3(-0.002604612769259929,-0.9417727709104703,0.3238574306142245)
#	v.upVector=Vector3(2.503708927703407e-33,-2.220446049250313e-16,1)
#	v.viewDir=Vector3(-2.8447434139544e-18,1,2.220446049250313e-16)
#	v.showEntireScene()

#	rndr=yade.qt.Renderer()
##	rndr.shape=False
##	rndr.bound=True # visualise Aabb
###	rndr.intrAllWire=True # visualise contact forces network
#	rndr.wire=False

#O.saveTmp()


import time
startTime=time.time()
startIter=O.iter

##O.run(5) # FIXME: 15 November 2020
O.run()
waitIfBatch()

