#!/usr/local/bin/yade-trunk -x
# -*- encoding=utf-8 -*-
# 2019 (c) Vasileios Angelidakis

#COMMENT: In this second run, I choose:  Ks=Kn*nu, nu=0.37
#COMMENT: In a third run, I will choose: Ks=Kn/(2*(1+nu))
#COMMENT: In a fourth run, I will choose reduced: density=0.849*1111 and maybe the Kn of the third run

# FIXME: Seed values not used -> Update trunk

# This is a script to simulate the 1st benchmark problem of the "Round robin test of angle of repose (AOR)".
# This first benchmark regards the plain-strain type experimental setup.

from yade import pack
import math

import yade.timing;

O.timingEnabled=False
if O.timingEnabled:
	yade.timing.reset() # not necessary if used for the first time, but it doesn't hurt having it here

import os
import errno
#try:
#   os.mkdir('./vtk/')
#except OSError as exc:
#   if exc.errno != errno.EEXIST:
#      raise
#   pass

# ENGINES
nu=0.37		# Poisson ratio
Kn=1.2e3	# FIXME: I have decreased the stiffness by 100!!


# run with: ./yade-2020-03-09.git-04cf6a0-batch --job-threads=2 params.txt roundRobin_PP_Device_1_batch.py
utils.readParamsFromTable(case=1)
from yade.params import table

case=table.case #case=1
if case==1:
	Ks=0.2*Kn
	density = 1.000*1111
	seedNum=5

elif case==2:
	Ks=0.2*Kn
	density = 0.849*1111
	seedNum=5

elif case==3:
	Ks=nu*Kn
	density = 0.849*1111
	seedNum=5

elif case==4:
	Ks=(1-nu)/(1-0.5*nu)*Kn
	density = 0.849*1111
	seedNum=5

elif case==5:
	Ks=(1-nu)/(1-0.5*nu)*Kn
	density = 0.849*1111
	seedNum=11

elif case>5:
	Ks=(1-nu)/(1-0.5*nu)*Kn
	density = 0.849*1111
#	seedNum=random.randint()
	seedNum=case*2

if case<10:
	subFix='00'+str(case)  #fileNameSubfix
else:
	subFix= '0'+str(case)

# FIXME: Revisit the verlet distance!!!
# Maybe assume either half the circumdiameter of the clump or half the radius of an individual sphere

# FIXME: In the insertion collider, I have increased the Aabb by 10%. Can I avoid that?

# FIXME: Restore:
#	Kn=1e5
#	verlet distance = 0.0015
#	newton.damping=0.0 after we deposit to the lower box


O.engines=[
	ForceResetter(),
	InsertionSortCollider([PotentialParticle2AABB(aabbEnlargeFactor=1.1)],verletDist=0.0015, avoidSelfInteractionMask=2,label='collider'), #verletDist=0.01 or 0.00015
	InteractionLoop(
		[Ig2_PP_PP_ScGeom(twoDimension=False, unitWidth2D=1.0, calContactArea=False)], #FIXME: Check if we want constant stiffness or not
		[Ip2_FrictMat_FrictMat_KnKsPhys(kn_i=Kn, ks_i=Ks, Knormal = Kn, Kshear = Ks, useFaceProperties=False, viscousDamping=0.071)],
		[Law2_SCG_KnKsPhys_KnKsLaw(label='law',neverErase=False, allowViscousAttraction=True)]
	),
	NewtonIntegrator(damping=0.2,exactAsphericalRot=True,gravity=[0,0,-9.81], label='newton')
]

from numpy import array

#density = 0.849*1111 #kg/m3 #FIXME: I can reduce it to achieve equal mass using: 0.849*1111


O.materials.append(FrictMat(young=-1,poisson=-1,frictionAngle=radians(35.5),density=density,label='granular')) # Friction angle between clump-clump
O.materials.append(FrictMat(young=-1,poisson=-1,frictionAngle=radians(27.2),density=0,label='box'))      # Friction angle between clump-box

units=1000 #1000 for meters, 1 for milimeters

t_230=230/units #mm
t_190=190/units #mm
t_160=160/units #mm
t_120=120/units #mm
t_100=100/units #mm

t_20 = 20/units #mm
t_10 = 10/units #mm
t_5  =  5/units #mm

t_350=350/units #mm
t_290=290/units #mm

# Coordinates of tetrahedron, where the spheres of the clump are located (from Excel file)
Ds=10/units # [10mm=0.01m] Circumdiameter of clump
rs=Ds/(2*(1+sqrt(6)/4)) # radius of each sphere

ptA = Vector3( rs/2., rs*sqrt(3)/6., rs*sqrt(6)/3. )
ptB = Vector3( 0    , 0            , 0             )
ptC = Vector3( rs   , 0            , 0             )
ptD = Vector3( rs/2., rs*sqrt(3)/2., 0             )

# -------------------------------------------------------------------- #
## Tetrahedra			#FIXME: The aa,bb,cc,dd values were calculated manually. They do NOT correspond to the Principal Axes: To be corrected!
#from roundRobin_tetrahedral_PP import * # Import particle characteristics
#aa=[-0.8669644300000000, 0.0540542600000000, 0.0540542600000000, 0.7588559000000000]
#bb=[-0.0000000000000000, -0.8164965800000000, 0.8164965800000000, -0.0000000000000000]
#cc=[0.4983700200000000, -0.5748142900000000, -0.5748142900000000, 0.6512585600000000]
#dd=[0.0006329900000000, 0.0006329900000000, 0.0006329900000000, 0.0006329900000000]
#Igeom = Vector3(3.28600914143454e-12, 3.286145904978621e-12, 3.286197740532751e-12)
#Volume = 0.0000003924806765
#minAabbValue = Vector3(0.0045204145251551, 0.0046310935638444, 0.0045849090077778)
#maxAabbValue = Vector3(0.0047653279218820, 0.0046310935637068, 0.0045662823887129)
#D_bound = 0.0101841905752756


# Approach 1: Tetrahedral Potential Particles
aa=array([ 0.000000000000000,  0.8164965809277260,  0.0000000000000000, -0.8164965809277259])
bb=array([ 0.000000000000000,  0.4714045207910317, -0.9428090415820634,  0.4714045207910317]) #-0.9428090415820634 instead of -1.0000000000000000
cc=array([-1.000000000000000,  0.3333333333333334,  0.3333333333333334,  0.3333333333333334])
dd=array([ 0.632993161855500,  0.6329931618555000,  0.6329931618555000,  0.6329931618555000])/(units) #FIXME: Check this. Do I need to subtract r?

#print(ptA)
#print(ptB)
#print(ptC)
#print(ptD)
# This combination works
k=0.65
r=rs/1.
R=sqrt(2)*rs

Igeom = Vector3(3.28600914143454e-12, 3.286145904978621e-12, 3.286197740532751e-12)
Volume = 0.0000003924806765

# This combination works too
#k=0.85
#r=0.65*rs
#R=Ds/2.

# Old attempts
#k=0.7
#r=rs/1.0 #rs/0.6
#R=Ds/2.0

# -------------------------------------------------------------------- #
# Generate cloud of rounded tetrahedral particles
# -------------------------------------------------------------------- #
# Random cloud
#packing='random'
#sp=pack.SpherePack()
#mn=Vector3(Ds/2.,Ds/2.,t_190+Ds/2.)
#mx=Vector3(t_100-Ds/2.,t_100-Ds/2.,4*t_190) #FIXME: This is the real one
##mx=Vector3(t_100-Ds,t_100-Ds,t_190+3*Ds) #FIXME: This is just a temp one
#sp.makeCloud(mn,mx,R*1.1,0,2150,False,seed=seedNum) #FIXME: Here I set a fixed seed, to make the results reproducible! Should try different "seed" values!
##2150
## FIXME: Check whether I can generate the packing using R and not 1.1*R, to make it tighter and thus reduce the height and thus the initial kinetic energy, to make it settle faster

#random.seed=seedNum
# Using randomDensePack
packing='random'
#mn=Vector3(Ds/2.,Ds/2.,t_190+Ds/2.)
#mx=Vector3(t_100-Ds/2.,t_100-Ds/2.,4*t_190) #FIXME: This is the real one
mn=Vector3(Ds/2.,Ds/2.,t_190+Ds/2.)
mx=Vector3(t_100-Ds/2.,t_100-Ds/2.,2.9*t_190) #FIXME: This is the real one

sp=pack.randomDensePack(pack.inAlignedBox(mn,mx),radius=Ds/2.,returnSpherePack=True,spheresInCell=500,material=O.materials['granular'],seed=seedNum) #FIXME: 15 November 2020
print(len(sp))
# -------------------------------------------------------------------- #
# Sort packing in ascending Z coordinates and delete excess particles to achieve sample sizes of exactly 25k, 50k, 100k
zValues=[]
for s in sp:
	zValues.append(s[0][2])

from operator import itemgetter
indices, zValues_sorted = zip(*sorted(enumerate(zValues), key=itemgetter(1)))
list(zValues)
list(indices)

sp_new=[]
for i in range(0,len(sp)):
	sp_new.append(sp[indices[i]])

sp_new=sp_new[0:2150] #FIXME 2150 # 5 November 2020
sp=sp_new # Used for debugging purposes
#O.bodies.append(sp_new)


# -------------------------------------------------------------------- #
# Count the number of spherical particles to verify sample size. We can comment this out later on.
print('The total number of spheres is: ',len(sp))


# -------------------------------------------------------------------- #
# Using regularOrtho
#mn=Vector3(    0+Ds/5.,     0+Ds/5., t_190)
#mx=Vector3(t_100-Ds/5., t_100-Ds/5., t_190*2.5) #FIXME: This is the real one
#sp=pack.regularOrtho(pack.inAlignedBox(mn,mx),radius=R*1.1,gap=R*1.1/10.,color=(1,0,0))
#sp=sp[0:2150]

# -------------------------------------------------------------------- #
## In-house code with positions
#packing='custom'
#aabbDist=Ds*collider.boundDispatcher.functors[0].aabbEnlargeFactor
#Nx=floor(t_100/aabbDist)
#Ny=Nx
#Nz=ceil(2150/((Nx-2)*(Ny-1)))

##dX=t_100-(Nx-1)*aabbDist/Nx/t_100
#gap=(t_100-(Nx-1)*aabbDist)/(Nx)

#sp=[]
#count=0
#for z in range(1,Nz):
#	for x in range(1,Nx):
#		for y in range(1,Ny):
#			sp.append(Vector3(x*gap+(2*x-1)*aabbDist/2., y*gap+(2*y-1)*aabbDist/2., z*aabbDist) + Vector3(0,0,t_190))
##			sp.append(Vector3(x*aabbDist, y*aabbDist, z*aabbDist) + Vector3(0,0,t_190))
# #instead of starting from 0,0,190 I can start from the middle
#			count=count+1
##print(Nx, Ny, Nz)
#sp=sp[0:2150] #FIXME: 2150
# -------------------------------------------------------------------- #


minAabbValue=1.2*Vector3(R,R,R)
maxAabbValue=1.2*Vector3(R,R,R)

for s in sp:
	#FIXME: Check this: We can subtract a percentage of "r" from all d[i] values and then adjust k
	# We can generate a number of particle shapes in matlab and find the best fit, by adjusting this percentage and the value of k.
	dd=dd#-r/30. #FIXME: Check whether I need to change this (if I do, it will change the vertices of the inner, sharp tetrahedron)

	b=Body()
	b.aspherical=True
	b.mask=1
	wire=False
	color=Vector3(random.random(),random.random(),random.random())

	b.shape=PotentialParticle( k=k, r=r, R=R, a=aa, b=bb, c=cc, d=dd, isBoundary=False, color=color, wire=wire, highlight=False, 
	minAabb=minAabbValue,			#1.2*Vector3(R,R,R),
	maxAabb=maxAabbValue,			#1.2*Vector3(R,R,R),
	minAabbRotated=minAabbValue,		#1.2*Vector3(R,R,R), #FIXME: Not used with AabbMinMax=False
	maxAabbRotated=maxAabbValue,		#1.2*Vector3(R,R,R), #FIXME: Not used with AabbMinMax=False
	AabbMinMax=False, id=len(O.bodies) )

#	V=4/3.*pi*R**3 #FIXME: I need to calculate the actual particle volume
#	geomInert=2/5.*V*(R)**2 #FIXME: I need to calculate the actual particle inertias

	b.shape.volume=Volume	#V
	b.shape.inertia=Igeom	#Vector3(geomInert,geomInert,geomInert)
	
	if packing=='random':
		pos=s[0]
	elif packing=='custom':
		pos=s
	else:
		print('Unidentified packing regime')

	utils._commonBodySetup(b, b.shape.volume, b.shape.inertia, material='granular', pos=pos, fixed=False) #s[0] stores center
#	random.seed(5)
	b.state.ori=Quaternion((random.random(),random.random(),random.random()),random.random()) # FIXME: Change the angle from [-2pi-2pi]
	O.bodies.append(b)

Zmax=max(b.state.pos[2] for b in O.bodies if len(b.state.blockedDOFs)==0) + Ds/2.

# Done: For now I dodged this FIXME: I set isBoundary=False, until I fix the shear forces there
# Done: I instead assigned isBoundary=False FIXME: I changed to isBoundary=True, and I need to fix the shear forces issue ASAP
# Done: Temporary solution FIXME: isBoundary=True: I need to either assign shearForces to the boundary bodies or set isBoundary=False and make sure I calculate the viscousNormal correctly

# FIXME: I need to define the particle to its principal system and centred to its centroid!!
# FIXME: I need to write the vertices of the inner "pilot" particle
# FIXME: I need to calculate the volume/inertia tensor/orientation of the particle to its principal system

# FIXME: I need to adjust the friction angle from the experiments: XXX: I have different friction among spheres and between spheres-boundaries
# FIXME: I need to adjust the viscous damping from the experiments

# FIXME: I need to control the parameters in batch-mode: Parameters to control: Kn, Ks, frictionAngle, verletDistance, viscousDamping

# FIXME: I need to write all the required information in .txt files: position of vertices, time elapsed for the whole analysis, xxx


from numpy import array

#Faces of the box
color=[0,0.5,1]
r=0.05*t_10
k=0.05
R=1.0*t_230

# ------------------------------------------------------------------------------------------------------------------------------------------
# Box below
# ------------------------------------------------------------------------------------------------------------------------------------------

V=0
geomInert=Vector3(0,0,0)

# Horizontal base: bbb
dd=array([t_120/2., t_120/2., t_120/2., t_120/2., t_10/2., t_10/2.]) - r

bbb=Body()
bbb.mask=3
aabb=1.00*Vector3(dd[0]+r,dd[2]+r,dd[4]+r)
bbb.shape=PotentialParticle(k=k, r=r, R=R,a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=False, color=color, AabbMinMax=True,
maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=aabb, maxAabb=aabb, fixedNormal=False)
utils._commonBodySetup(bbb, V, geomInert, material='box', pos=[t_100/2.,t_100/2.,-t_10/2.], fixed=True)
O.bodies.append(bbb)

# ------------------------------------------------------------------------------------------------------------------------------------------
# Vertical Faces
# Small front face b0
dd=array([t_5/2., t_5/2., t_100/2., t_100/2., t_20/2., t_20/2.]) - r

b0=Body()
b0.mask=3
aabb=1.00*Vector3(dd[0]+r,dd[2]+r,dd[4]+r)
b0.shape=PotentialParticle(k=k, r=r, R=R, a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=False, color=color, AabbMinMax=True,
maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=Vector3(0.8*dd[0]+r,dd[2]+r,dd[4]+r), maxAabb=Vector3(0.8*dd[0]+r,dd[2]+r,dd[4]+r), fixedNormal=False) #minAabb=aabb, maxAabb=aabb,
utils._commonBodySetup(b0, V, geomInert, material='box', pos=[-t_5/2.,t_100/2.,t_20/2.], fixed=True)
O.bodies.append(b0)

# ------------------------------------------------------------------------------------------------------------------------------------------
dd=array([t_5/2., t_5/2., t_100/2., t_100/2., t_230/2., t_230/2.]) - r

# COMMENT: This is the front movable plate: Have made it transparent manually
bA=Body()
bA.mask=3
aabb=1.00*Vector3(dd[0]+r,dd[2]+r,dd[4]+r)
bA.shape=PotentialParticle(k=k, r=r, R=R, a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=False, color=[1.0,1.0,0], AabbMinMax=True, wire=True,
maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=Vector3(0.8*dd[0]+r,dd[2]+r,dd[4]+r), maxAabb=Vector3(0.8*dd[0]+r,dd[2]+r,dd[4]+r), fixedNormal=False)
utils._commonBodySetup(bA, V, geomInert, material='box', pos=[-t_5/2.,t_100/2.,t_20+t_230/2.], fixed=True)
O.bodies.append(bA)

# ------------------------------------------------------------------------------------------------------------------------------------------
# Back face bB
dd=array([t_10/2., t_10/2., t_100/2., t_100/2., t_190/2., t_190/2.]) - r

bB=Body()
bB.mask=3
aabb=1.00*Vector3(dd[0]+r,dd[2]+r,dd[4]+r)
bB.shape=PotentialParticle(k=k, r=r, R=R, a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=False, color=color, AabbMinMax=True,
maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=aabb, maxAabb=aabb, fixedNormal=False)
utils._commonBodySetup(bB, V, geomInert, material='box', pos=[(t_100+t_10/2.),t_100/2.,t_190/2.], fixed=True)
O.bodies.append(bB)

# ------------------------------------------------------------------------------------------------------------------------------------------
# Side face bC
dd=array([ t_120/2., t_120/2., t_10/2., t_10/2., t_190/2., t_190/2.]) - r

bC=Body()
bC.mask=3
aabb=1.00*Vector3(dd[0]+r,dd[2]+r,dd[4]+r)
bC.shape=PotentialParticle(k=k, r=r, R=R, a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=False, color=[1,0,0], AabbMinMax=True,
maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=aabb, maxAabb=aabb, fixedNormal=False)
utils._commonBodySetup(bC, V, geomInert, material='box', pos=[t_100/2.,t_100+t_10/2.,t_190/2.], fixed=True)
O.bodies.append(bC)

# ------------------------------------------------------------------------------------------------------------------------------------------
# Side face bD
dd=array([ t_120/2., t_120/2., t_10/2., t_10/2., t_190/2., t_190/2.]) - r

bD=Body()
bD.mask=3
aabb=1.00*Vector3(dd[0]+r,dd[2]+r,dd[4]+r)
bD.shape=PotentialParticle(k=k, r=r, R=R, a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=False, color=[0,1,0], AabbMinMax=True, wire=False,
maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=aabb, maxAabb=aabb, fixedNormal=False)
utils._commonBodySetup(bD, V, geomInert, material='box', pos=[t_100/2.,-t_10/2.,t_190/2.], fixed=True)
O.bodies.append(bD)


# ------------------------------------------------------------------------------------------------------------------------------------------
# Box above
# ------------------------------------------------------------------------------------------------------------------------------------------

# Vertical Faces
dd=array([t_5/2., t_5/2., t_100/2., t_100/2., t_290/2., t_290/2.]) - r

# COMMENT: This is the front plate of the box above: Have made it transparent manually
bbA=Body()
bbA.mask=3
aabb=1.00*Vector3(dd[0]+r,dd[2]+r,dd[4]+r)
bbA.shape=PotentialParticle(k=k, r=r, R=R, a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=False, color=color, AabbMinMax=True, wire=True,
maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=Vector3(0.8*dd[0]+r,dd[2]+r,dd[4]+r), maxAabb=Vector3(0.8*dd[0]+r,dd[2]+r,dd[4]+r), fixedNormal=False)
utils._commonBodySetup(bbA, V, geomInert, material='box', pos=[-t_5/2.,t_100/2.,t_20+t_230+t_290/2.], fixed=True)
O.bodies.append(bbA)

# ------------------------------------------------------------------------------------------------------------------------------------------
# Above back face bB
dd=array([t_10/2., t_10/2., t_100/2., t_100/2., t_350/2., t_350/2.]) - r

bbB=Body()
bbB.mask=3
aabb=1.00*Vector3(dd[0]+r,dd[2]+r,dd[4]+r)
bbB.shape=PotentialParticle(k=k, r=r, R=R, a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=False, color=color, AabbMinMax=True,
maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=aabb, maxAabb=aabb, fixedNormal=False)
utils._commonBodySetup(bbB, V, geomInert, material='box', pos=[(t_100+t_10/2.),t_100/2.,t_190+t_350/2.], fixed=True)
O.bodies.append(bbB)

# ------------------------------------------------------------------------------------------------------------------------------------------
# Above side face bC
dd=array([ t_120/2., t_120/2., t_10/2., t_10/2., t_350/2., t_350/2.]) - r

bbC=Body()
bbC.mask=3
aabb=1.00*Vector3(dd[0]+r,dd[2]+r,dd[4]+r)
bbC.shape=PotentialParticle(k=k, r=r, R=R, a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=False, color=[1,0,0], AabbMinMax=True,
maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=aabb, maxAabb=aabb, fixedNormal=False)
utils._commonBodySetup(bbC, V, geomInert, material='box', pos=[t_100/2.,t_100+t_10/2.,t_190+t_350/2.], fixed=True)
O.bodies.append(bbC)

# ------------------------------------------------------------------------------------------------------------------------------------------
# Above Side face bD
dd=array([ t_120/2., t_120/2., t_10/2., t_10/2., t_350/2., t_350/2.]) - r

bbD=Body()
bbD.mask=3
aabb=1.00*Vector3(dd[0]+r,dd[2]+r,dd[4]+r)
bbD.shape=PotentialParticle(k=k, r=r, R=R, a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=False, color=[0,1,0], AabbMinMax=True, wire=False,
maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=aabb, maxAabb=aabb, fixedNormal=False)
utils._commonBodySetup(bbD, V, geomInert, material='box', pos=[t_100/2.,-t_10/2.,t_190+t_350/2.], fixed=True)
O.bodies.append(bbD)

# ------------------------------------------------------------------------------------------------------------------------------------------
# Horizontal movable plate at elevation +190mm
dd=array([t_190/2., t_190/2., t_100/2., t_100/2., t_5/2., t_5/2.]) - r

bH=Body()
bH.mask=3
aabb=1.00*Vector3(dd[0]+r,dd[2]+r,dd[4]+r)
bH.shape=PotentialParticle(k=k, r=r, R=R,a=[1,-1,0,0,0,0], b=[0,0,1,-1,0,0], c=[0,0,0,0,1,-1], d=dd, id=len(O.bodies), isBoundary=False, color=color, AabbMinMax=True, wire=False,
maxAabbRotated=aabb, minAabbRotated=aabb, minAabb=aabb, maxAabb=aabb, fixedNormal=False)
utils._commonBodySetup(bH, V, geomInert, material='box', pos=[t_190/2.,t_100/2.,t_190+t_5/2.], fixed=True)
bbbH=O.bodies.append(bH)

# ------------------------------------------------------------------------------------------------------------------------------------------
# Steps we follow
# 1. we deposit the particles in the upper box and we leave them rest, under high local damping
# 2. we remove the local damping and leave active only the viscous damping
# 3. we move the horizontal plate bH, let the particles move to the bottom box and leave them rest
# 4. we delete the upper box and the horizontal plate bH
# 5. we move the movable plate bA upwards, and let the particles flow outside the box
# 6. we delete the particles that leave the box

# ------------------------------------------------------------------------------------------------------------------------------------------
# Additional engines to control the boundary plates & the simulation in general

# FIXME: Need to revise the iterPeriods below: When I use 2000 particles, is 1000 large/small?

O.engines=O.engines+[PyRunner(iterPeriod=100,command='controlStage()',label='cS', dead=False)] #cS=O.engines[4]
O.engines=O.engines+[PyRunner(iterPeriod=100,command='moveHorizontalPlate()',label='mH', dead=True)] #mH=O.engines[5]
O.engines=O.engines+[PyRunner(iterPeriod=100,command='deleteUpperBox()',label='dUB', dead=True)] #duB=O.engines[6]
O.engines=O.engines+[PyRunner(iterPeriod=100,command='moveVerticalPlate()',label='mV', dead=True)] #mV=O.engines[XXX]
O.engines=O.engines+[PyRunner(iterPeriod=100,command='finalStage()',label='fS', dead=True)] #fS=O.engines[XXX]

O.engines=O.engines+[PyRunner(iterPeriod= 20,command='pauseSimulation()',label='pS', dead=True)] #fS=O.engines[XXX]
#O.engines=O.engines+[PyRunner(iterPeriod= 1,command='print(O.iter)',label='pI', dead=False)] #fS=O.engines[XXX]

def sampleIsStable(ufMax):
	global uf
	global KE
	try:
		uf=utils.unbalancedForce()
		KE=utils.kineticEnergy()
#		if uf<ufMax and KE<0.0001:  		 #FIXME: Old value, used in Simulations 1-5
		if (uf<ufMax and KE<1e-8) or (KE<1e-12): # This is the one I copied from Device 2
#			calm() # FIXME: I could assign calm() here, to help the sample stabilise
			return True
		else:
			return False
	except:
		print('Cannot calculate either uf or KE')
		return False

print(str(O.iter)+" cS is running")
def controlStage():
#	print( utils.unbalancedForce(), utils.kineticEnergy() )
#	print(str(O.iter)+" cS is running")
	maxPos=max(b.state.pos[2] for b in O.bodies if len(b.state.blockedDOFs)==0) + Ds/2. #or Ds/1. to be sure

	if sampleIsStable(0.20) and maxPos<t_190+t_350:
		print("The sample is stable on the upper box!")
		newton.damping=0.2 #FIXME: Maybe I could still keep some newton damping here, like 20% and turn it to 0.0 before we start moving the vertical plate
		mH.dead=False
#		calm() # FIXME: Do I need to invoke calm() just in case?
		cS.dead=True
		print(str(O.iter)+" mH is running")


def moveHorizontalPlate():
#	print(str(O.iter)+" mH is running")
	if bH.state.vel.norm()==0:
		newton.damping=0.20 # FIXME: Maybe keep some local damping here if the sample cannot achieve equilibrium in the bottom box?
		bH.state.vel=[43/units,0,0] # 0.043 m/s = 43 mm/s
		print(str(O.iter)+" bH is moving")
	elif bH.state.pos[0]>=t_190+t_5/2.:
			print(str(O.iter)+" bH is deleted")
			O.bodies.erase(bH.id)
			dUB.dead=False
			mH.dead=True
			print(str(O.iter)+" dUB is running")

def deleteUpperBox():
#	print(str(O.iter)+" dUB is running")
	maxPos=max(b.state.pos[2] for b in O.bodies if len(b.state.blockedDOFs)==0) + Ds/2. #or Ds/1. to be sure
	if maxPos<t_190:
		O.bodies.erase(bbA.id)
		O.bodies.erase(bbB.id)
		O.bodies.erase(bbC.id)
		O.bodies.erase(bbD.id)
		mV.dead=False
		dUB.dead=True
		print(str(O.iter)+" mV is running")

def moveVerticalPlate():
	global mVTime, mVIter, vW
#	print(str(O.iter)+" mV is running")
	maxPos=max( b.state.pos[2] for b in O.bodies if len(b.state.blockedDOFs)==0 ) + Ds/2. #or Ds/1. to be sure
#	calm() #FIXME: To be removed
	if bA.state.vel.norm()==0 and sampleIsStable(0.15) and maxPos<t_190: #FIXME: Return and see if I need the "maxPos" check here!
		newton.damping=0.0
		vW = [0,0,43/units] #0.043m = 43mm/s
		bA.state.vel=vW #0.043m = 43mm/s
		print(str(O.iter)+" bA is moving")
		mVTime=time.time()
		mVIter=O.iter
#		print('mVTime: ', mVTime)
	elif bA.state.pos[2]>=t_190+t_230/2.+t_5/2. or (bA.state.pos[2]>=t_190 and len(O.interactions.withBody(bA.id))==0): #  #FIXME: Check if I still need the second check
		print(str(O.iter)+" bA is deleted")
		O.bodies.erase(bA.id)
		mV.dead=True
		fS.dead=False
		print(str(O.iter)+" fS is running")
	else: #	FIXME: to check if I really need this bit
		return

def finalStage():
#	print(str(O.iter)+" fS is running")
	for b in O.bodies:
		if (b.state.vel.norm() > 0.0) and (b.state.pos[2] < -Ds-t_10) and (len(b.state.blockedDOFs)==0): #FIXME: Do I need the velocity check?
			O.bodies.erase(b.id,True)
	if sampleIsStable(0.15):
		print('Simulation almost finished. Writing output file. Simulation continues.')
		writeResults()
		if sampleIsStable(0.05):
#		if sampleIsStable(0.05):
			print('Simulation finished. Writing output file. Simulation paused.')
			O.engines=O.engines+[PotentialParticleVTKRecorder(fileName='./roundRobin_Device_1_Results/PP_VTK_Recorder_', twoDimension=False, iterPeriod=1, nDo=1, sampleX=30, sampleY=30, sampleZ=30, maxDimension=Ds/5.)]
#			writeResults()
#			O.pause()
			pS.dead=False

def pauseSimulation():
	O.pause()


#Save data details
directory='./roundRobin_Device_1_Results/'
#scriptPath = os.path.abspath(os.path.dirname(sys.argv[-1])) #Path where the script is stored
if os.path.exists(directory)==False:
	os.mkdir(directory)
#else:
#	print('\n!! Save data: overwrite the contents of the folder roundRobin_results_run_2/ !!\n')


# ------------------------------------------------------------------------------------------------------------------------------------------

mVTime=0 # FIXME: To be deleted
mVIter=200 # FIXME: To be deleted
vW=[0,0,0.043]

def writeResults():
	global endTime, endIter

	endTime=time.time()
	endIter=O.iter
	# Here I need to write the output positions, in the requested format
	f = open(directory+'dev01_run'+subFix+'.dat','w')
	for b in O.bodies:
		if len(b.state.blockedDOFs)==0 and b.state.pos[2]>0:
			#vertices = [b.state.pos + b.state.ori*v for v in b.shape.v] #TODO: I can vectorize this
			for v in b.shape.vertices:
				vRot=b.state.pos+b.state.ori*v
				f.write('%g %g %g\n'%(vRot[0],vRot[1],vRot[2]))
	f.close()

	# Here I output the elapsed time the simulation took to run
	f = open(directory+'dev01_run'+subFix+'_metadata.dat','w')

	f.write('rho  = %g kg/m^3\n'%(density))
	f.write('g    = %g m/sec^2\n'%(abs(newton.gravity[2])))
	f.write('dt   = %g sec\n'%(O.dt))
	f.write('L    = %g m\n'%(t_100/units))

	f.write('tr   = %g sec\n'%(endTime-mVTime))
	f.write('N    = %d steps\n'%(mVIter))
	f.write('T    = %g hours\n'%((endTime-startTime)/3600.))
	f.write('Zmax = %g m\n'%(Zmax))
	f.write('vW   = %g m/sec\n\n'%(vW[2]))

	f.write('(rho)  : Density\n')
	f.write('(g)    : Gravity\n')
	f.write('(dt)   : Time step\n')
	f.write('(L)    : Length of the bottom plate of the box in Device I\n')
	f.write('(Zmax) : Initial height of the agglomerate\n')
	f.write('(vW)   : Elevating speed of the movable front vertical face\n')

	f.write('(tr)   : Elapsed time at repose from starting to elevate of the front vertical face\n')
	f.write('(N)    : Mean number of steps from starting to elevate of the front vertical face\n')
	f.write('(T)    : Total execution time\n')


	f.close()

	print("Total execution time: ",str(endTime-startTime),"s")

	fileName="dev01_run"+subFix+"_savedScene.bz2"
	O.save(directory+fileName) #Save the scene to allow post-processing

#	print('endTime: ', endTime)
#	if O.timingEnabled: #FIXME: Remember to reinstate this
#		yade.timing.stats()

	# FIXME: Write also the "mVTime" and other parameters!


# FIXME: Also need to write another file, keeping:
#	- Elapsed time
#	- XXX other features of the simulation they require in the Excel files


#O.dt = 0.2*sqrt(0.3*O.bodies[0].state.mass/Kn)
O.dt = 0.2*sqrt(0.5*O.bodies[0].state.mass/Kn)

#if opts.nogui==False:
#	#Control the rendering quality
#	Gl1_PotentialParticle.aabbEnlargeFactor=1.3
#	Gl1_PotentialParticle.sizeX=18
#	Gl1_PotentialParticle.sizeY=18
#	Gl1_PotentialParticle.sizeZ=18

#	from yade import qt
#	qt.Controller()
#	v=qt.View()

#	v.ortho=True
#	#v.sceneRadius=200
#	v.eyePosition=Vector3(-471.3751859505503,49.99999999987351,171.92744093686932)
#	v.upVector=Vector3(0,0,1)
#	v.viewDir=Vector3(1,0,0)
#	v.showEntireScene()

#	rndr=yade.qt.Renderer()
#	rndr.shape=False
#	#rndr.ghosts=False
##	rndr.bound=True # visualise Aabb
#	rndr.intrAllWire=True # visualise contact forces network

#O.saveTmp()

import time
startTime=time.time()
startIter=O.iter
#print('startTime: ', startTime)

O.run()
waitIfBatch()

#O.run(100)
#writeResults()

## --------- mpi? --------- 
#from yade import mpy as mp

#class Fibre : 
#    def __init__(self): 
#        self.numseg = 0 
#        self.cntrId = -1 
#        self.nodes = []
#        self.segs = []

#fibreList = [Fibre() for i in range(len(O.bodies))]
#numThreads = 4
##mp.initialize(3)
#mp.DOMAIN_DECOMPOSITION = True 
#mp.fibreList = fibreList
#NSTEPS = 500
#mp.mpirun(NSTEPS, numThreads)
#mp.mergeScene() 



