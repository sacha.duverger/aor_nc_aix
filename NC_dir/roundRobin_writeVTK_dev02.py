#!/usr/local/bin/yade-trunk -x
# -*- encoding=utf-8 -*-
# 2020 (c) Vasileios Angelidakis, Newcastle University, UK

# ------------------------------------------------------------------------------------------------------------------------------------------
# Round robin test of angle of repose (AOR)
# 1st benchmark problem: plain-strain device
# ------------------------------------------------------------------------------------------------------------------------------------------

# This is a script to read the output file of the plain-strain device and write a VTK output
# The positions of the spheres are loaded in YADE, where their shape is written in VTK format.
# The analyses may have been ran in any DEM code apart from YADE.

# YADE can be downloaded in Ubuntu using: "sudo apt-get install yade" or "sudo apt-get install yadedaily", as described in: https://yade-dem.org/doc/installation.html

# TODO: The only thing that needs to change is the name of the output file, e.g. here it is assumed to be: "dev01_runXXX.dat"
filename='dev02_run020.dat'

# ------------- Create VTK directory, if it doesn't exist ----------------------------------------------------------------------------------
import os
import errno
try:
   os.mkdir('./vtk/')
except OSError as exc:
   if exc.errno != errno.EEXIST:
      raise
   pass


# ------------- Engines --------------------------------------------------------------------------------------------------------------------
O.engines=[
	ForceResetter(),
	InsertionSortCollider([Bo1_Sphere_Aabb(),Bo1_Box_Aabb()]),
	InteractionLoop(
		[Ig2_Sphere_Sphere_ScGeom(),Ig2_Box_Sphere_ScGeom()],
		[Ip2_FrictMat_FrictMat_MindlinPhys(label='IPhysFunctor')], # Hertz-Mindlin
		[Law2_ScGeom_MindlinPhys_Mindlin(label='contactLaw')] # Hertz-Mindlin
	),
	NewtonIntegrator(damping=0.0,gravity=[0,0,0], label='newton'),
	VTKRecorder(iterPeriod=1,fileName='./vtk/'+filename,recorders=['spheres','colors','id']) # 'all'
]


# ------------- Calculate radius of each sphere --------------------------------------------------------------------------------------------
units=1000 #1000 for meters, 1 for milimeters
Ds=10/units # [cm] Circumdiameter of clump
rs=Ds/(2*(1+sqrt(6)/4)) # [cm] radius of each sphere


# ------------- Read roundRobin output and create list -------------------------------------------------------------------------------------
from numpy import loadtxt
lines = loadtxt(filename, comments='#', delimiter=' ', unpack=False)

myList=[0]*len(lines) #initialise list
for i in range(0,len(lines)):
	myList[i]=(Vector3(lines[i][0], lines[i][1], lines[i][2]), rs, -1 ) # append in the list the radius rs and the clumpID


# ------------- Generate cloud of spheres from the list ------------------------------------------------------------------------------------
from yade import pack
sp=pack.SpherePack()
sp.fromList(myList)
sp.toSimulation() #sp.toSimulation(material='frictionless')

for i in range(0,len(O.bodies),4): # step=4: iterate every 4 bodies
	color=Vector3(random.random(),random.random(),random.random())
	for j in range(0,4):
		O.bodies[i+j].shape.color=color


# ------------- Invoke qt view -------------------------------------------------------------------------------------------------------------
from yade import qt
qt.Controller()
v=qt.View()

v.eyePosition=Vector3(-0.0009133499999951916,-0.3124691418771022,0.03690307000000109)
v.upVector=Vector3(0,0,1)
v.viewDir=Vector3(0,1,0)


#O.run(1)

