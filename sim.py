import sys, os, glob, errno
sys.path.append(os.getcwd())

import numpy as np, time as tm
from dataRecorder import dataRecorder

from types import ModuleType

# "PP" for Potential Particle
# "CLP" for Clump
model = "CLP" 

series = "param_study" # "param_study" for PP parametric study, "main" for main series

# Simulation's parameters
wall_velocity = 0.043 * 1e3 # m/s, shouldn't change the angle of repose
damping = 0
# if model == "PP": uF_threshold = .2 # Maximum value for the unbalanced force when checking stability
# elif model == "CLP": uF_threshold = 1e-5
uF_threshold = 5e-4

# Other parameters
seed = 0

# Read parameters from table if running in batch
readParamsFromTable(seed=int(seed), model=model, Ks=240, density=1111, series=series)
from yade.params.table import *
np.random.seed(seed) # Fixing random seed

# Creating simulation's directory if it doesn't already exist
if series=="main": save_dir = "sims_{:}/".format(model)
elif series=="param_study": save_dir = "sims_{:}_param_study/".format(model)
if not os.path.isdir(save_dir) : os.mkdir(save_dir)

# Creating final states' directory if it doesn't already exist
fs_dir = "final_states/"
if not os.path.isdir(fs_dir) : os.mkdir(fs_dir)

# Load sample
if series=="main": sample = glob.glob("samples/{:}_2150_parts_{:d}.yade*.xml.bz2".format(model, seed))[0] # Beware if you have several samples with the same parameters but different yade version
elif series=="param_study": sample = glob.glob("samples/{:}_2150_parts_{:d}__density{:.0f}_Ks{:.0f}.yade*.xml.bz2".format(model, seed, density, Ks))[0] # Beware if you have several samples with the same parameters but different yade version
O.load(sample)
print("Sample {:} loaded".format(sample))

# Import utility module for chosen the model
if model == "PP": import utl_pp as utl
elif model == "CLP": import utl_clp as utl

# Get walls
loadVars('walls')
from yade.params.walls import *
walls = []
if model == "PP": 
    for b in [bbb, b0, bA, bB, bC, bD]:
        b.shape.isBoundary = True # Make sure walls ar flagged as boundary
        walls.append(O.bodies[b.id])
    bbb, b0, bA, bB, bC, bD = walls
    door = bA # Door velocity is set in pyrunners
elif model == "CLP": 
    for b in [boA, boB, lA, lB, rA, rB, baA_l, baB_l, baA_d, baB_d, fA, fB]:
        walls.append(O.bodies[b.id])
    boA, boB, lA, lB, rA, rB, baA_l, baB_l, baA_d, baB_d, fA, fB = walls
    door = baA_d

# With the PP model, simulation is setup in PyRunners so this step is necessary only for CLP
if model == "CLP": 
    for b in [baA_d, baB_d]: b.state.vel = (0, 0, wall_velocity) # Set door velocity
    newton.damping = damping


# Get initial height of the door
h0 = door.state.pos[2] 

# Add simulation's PyRunners
utl.set_pyruners(utl.sim_pyrunners)

# Setup data recorder
data_to_record = ["O.iter", "O.dt", "O.time", "O.speed", "tm.time()-t0", "unbalancedForce()", "utils.kineticEnergy()", "utl.avg_coord_nmbr()", "door.state.pos[2]-h0", "utl.n_erased"] # those variables will be written in a text file every time recorder.write_data is called	
recorder = dataRecorder(var2record=data_to_record, data_directory=save_dir, var_dict=globals())	
print("Data will be saved in ", recorder.data_directory)
O.engines += [PyRunner(command="recorder.write_data(var_dict=globals())", iterPeriod=100, label="data_writer")]

# Run simulation if it is part of batch
def run():
    global t0

    ## Print all versions so saved file is easier to reopen
    printAllVersions()

    t0 = tm.time()
    O.run(wait=True)
    t_end = tm.time()

    print("Simulation took ", t_end-t0, " seconds")

    ## Save walls
        ### Create a dictionnary with all wall objects
    walls_names = [[ i for i, a in globals().items() if id(a) == id(x)][0] for x in walls]
    wall_dict = {key: val for key,val in zip(walls_names, walls)}

        ### Use YADE's saveVars
    saveVars('walls', **wall_dict)

    if series=="main": save_name = "final_state_{:}_2150_parts_{:d}.yade{:}.xml.bz2".format(model, seed, version.split("-")[-1])
    elif series=="param_study": save_name = "final_state_{:}_2150_parts_{:d}__density{:.0f}_Ks{:.0f}.yade{:}.xml.bz2".format(model, seed, density, Ks, version.split("-")[-1])

    O.save(recorder.data_directory + save_name)

    # Create a symlink of the final state into the final states directory (it takes this much lines because if the link already exists, it has to be removed)
    try: os.symlink(recorder.data_directory + save_name, fs_dir + save_name)
    except OSError as e:
        if e.errno == errno.EEXIST:
            os.remove(fs_dir + save_name)
            os.symlink(recorder.data_directory + save_name, fs_dir + save_name)
        else: raise e

if runningInBatch(): run()
else: t0 = tm.time()
